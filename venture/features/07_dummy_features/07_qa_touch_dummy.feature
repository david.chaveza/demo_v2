# language: es
# encoding: utf-8
Característica: QA TOUCH CONEXION
  - COMO AUTOMATIZADOR DEBERIA PODER CONECTAR TAF-R CON QA-TOUCH
    - USANDO EL CODIGO DE CONEXION EN HOOKS
    - USANDO TAG'S CON PREFIJO @TC_ => SEGUIDO DEL RESULT KEY DEL TEST CASE EN QA-TOUCH. EJ. @TC_VGDMWP

  @regresion @web @higth @TR0001 @TC_VGDMWP
  Escenario: TR0001 - TEST DUMMY
    Dado Abro el aplicativo sistema
    Y Step dummy passed
    Y Step tomar evidencia
    Y Step dummy passed
    Y Step tomar evidencia

  @regresion @web @low @TR0002 @TC_JGjL6L
  Escenario: TR0002 - TEST DUMMY FAIL
    Dado Abro el aplicativo sistema
    Y Step dummy passed
    Y Step tomar evidencia
    Y Step paso dummy failed
    Y Step tomar evidencia
