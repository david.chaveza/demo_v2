# language: es
# encoding: utf-8
Característica: WS DEMO - API THE CAT

  @regresion @ws @TC_WS_001
  Escenario: GET CAT RANDOM
    Dado I am a client
    Cuando I test at web service "get cat random"
    Entonces I get response code success
    Y I get response body success
    Y Valido que el response contenga el siguiente JSON PATH "$.[0].id"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != null)]"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != '')]"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.breeds.length() > 0)]"

  @regresion @ws @TC_WS_001_02
  Escenario: GET CAT RANDOM
    Dado I am a client
    Cuando Pruebo el web services "get cat random"
    Entonces Valido que el Response Code sea exitoso
    Y Valido que el response contenga el siguiente JSON PATH "$.[0].id"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != null)]"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != '')]"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.breeds.length() > 0)]"

  @regresion @ws @TC_WS_002
  Escenario: VOTE
    Dado I am a client
    Cuando I test at web service "vote"
    Entonces I get response code success

  @regresion @ws @TC_WS_003
  Escenario: VOTE FAVOURITES
    Dado I am a client
    Cuando I test at web service "vote favourites"
    Entonces I get response code success

