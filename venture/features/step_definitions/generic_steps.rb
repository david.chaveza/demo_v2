# encoding: utf-8
##########################################################################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:
##########################################################################################################
require_relative '../../../helpers/generic'
require_relative '../../../helpers/drivers/driver'
require_relative '../../../helpers/data_driven/csv_data'
include Drivers

##########################################################################################################
# GENERICOS TODAS LAS PLATAFORMAS
#   PARA PROYECTOS REALIES DE 'RECOMIENDA' QUE TODAS LAS PLATAFOMRAS TENTAN EL 1ER STEP
#     'Dado Abro la aplicacion'
##########################################################################################################

##########################################################################################################
# WEB - CAPYBARA
##########################################################################################################

When(/^Abro el aplicativo sistema$/) do
  generic_page = WebPages::GenericPage.new
  generic_page.abrir_app
  save_evidence_execution
end

When(/^Selecciono el Logo de My Store$/) do
  generic_page = WebPages::GenericPage.new
  generic_page.seleccionar_logo_my_store
end

##########################################################################################################
# ANDROID - CALABASH
##########################################################################################################
When(/^Abro la aplicacion con calabash\-android$/) do
  true
end

##########################################################################################################
# ANDROID - APPIUM
##########################################################################################################
When(/^Abro la aplicacion con appium\-android$/) do
  true
end

##########################################################################################################
# WINIUM
##########################################################################################################
When(/^Abro el aplicativo desktop 'Bony System'$/) do
  driver(:winium)
  save_evidence_execution
end


##########################################################################################################
##########################################################################################################
##########################################################################################################
#'STEP' que permite ejecutar varios 'STEPS DEFINITION' de manera iterativa,
#     correspondientes al numero de registros de un 'CSV DATA
##########################################################################################################
##########################################################################################################
##########################################################################################################
# Permite iterar varios steps de acuerdo a los registros de un csv data o los filtros aplicados al data pool
# @params
#   :csv_data String, nombre del archivo datapool csv, este se debe ubicar en '..venture/config/csv_data'
#   :dt_filters String, filtros a aplicar al datapool csv. los filtros a columnas se delimitan por ',' (coma)
#       - delimitaodor: ','
#       - operadores: '=, !=, >, >=, <, <=, contains'
#       - ejemplo: 'columna1 = valor, columna2 != valor, columna3 > valor, columna4 <= valor, columna5 contains valor'
#   :step_strings String, 'steps definitions' a ejecutar, se ingresa un salto de linea entre cada step
When(/^Ejecuto los siguientes pasos para cada Fila del CSV "([^"]*)" y filtro por "([^"]*)":$/) do |csv_data, dt_filters, step_strings|

  #bandera resultado
  result = true
  #separar el texto en cada uno de los steps
  steps = step_strings.to_s.split(/\n+/)

  #Obtener directorio "PATH_EXTERNAL_DATA/csv_data" si no esta incluido en el nombre del archivo "csv_data"
  path_external = nil
  unless Config.path_external_data.nil?
    path_external = csv_data.to_s.downcase.include?('csv_data') ?
                      Config.path_external_data.to_s.strip
                      : "#{Config.path_external_data.to_s.strip}/csv_data"

  end

  #Obtener data
  #   Evaluar PATH_EXTERNAL_DATA para la toma de archivos de datos
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip, path_external

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all'
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  #$dt_data = csv
  Config.dt_data = csv

  #ITERAR CSV DATA
  csv.each_with_index do |record, index|

    #descargar fila 0 de la data. esta contiene nombre de columnas
    if index > 0

      #enviar data row a variable global '$dt_row'. esta se puede recuperar en cualqquier step y cualquier page object
      #$dt_row = record
      Config.dt_row = record
      result_it = "PASS"
      step_name = nil

      #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
      puts "ITERATION => #{index}"
      puts "DATA => #{record}"

      begin

        steps.each {|step_string|

          step_name = nil
          step = nil

          #'STEPS DEFINITIONS' que requieren parametrizacion:
          #   - desde el feature se pasa exprecion regular => ${dt_parametro}
          #   - se remplaza la expresion ${parametro} por  record[:nombre_columna].to_s
          if step_string.to_s.include? '{dt_'
            step =  String.new(step_string)
            step_name = get_step_dt_values(step, record)
          else
            step_name = step_string
          end

          step step_name

          #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
          puts "STEP => #{step_name} => PASS"

        }

      rescue Exception => e
        result = false
        result_it = "FAIL"
        #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
        puts "STEP => #{step_name} => FAIL"
        puts "MSJ ERROR => #{e.message}"
        save_evidence_execution_fail

      end

      #IMPRIME EN REPORTE HTML CUCUMBER DATOS DE ITERACION (LOG)
      puts "STATUS ITERATION => #{result_it}"
      puts "\n"

    end

  end

  unless result
    fail(mjs="FAIL TEST")
  end

end


##########################################################################################################
##########################################################################################################
##########################################################################################################
#'STEP' que permite obtener la data correspondientes desde un 'CSV DATA'
#     se puede filtrar y estos valores se almacenan en
#       varialbe global '$data'
#   "**NOTA": LOS VALORES OBTENEIDOS CORRESPONDEN SOLO A 1 FILA
##########################################################################################################
##########################################################################################################
##########################################################################################################

When(/^Obtengo los datos de prueba del archivo "([^"]*)" data pool y filtro por "([^"]*)"$/) do |csv_data, dt_filters|

  #Obtener directorio "PATH_EXTERNAL_DATA/csv_data" si no esta incluido en el nombre del archivo "csv_data"
  path_external = nil
  unless Config.path_external_data.nil?
    path_external = csv_data.to_s.downcase.include?('csv_data') ?
                      Config.path_external_data.to_s.strip
                      : "#{Config.path_external_data.to_s.strip}/csv_data"

  end

  #Obtener data
  #   Evaluar PATH_EXTERNAL_DATA para la toma de archivos de datos
  #   - si "dt_filters = 'all' se ejecutan recuperan los registros"
  csv = get_csv_data csv_data.to_s.strip, path_external

  #   - si "dt_filters != 'all' se manda a reducir la data, solo para los filtros seleccionados por columna
  unless dt_filters.to_s.downcase.strip == 'all' or dt_filters.to_s.empty?
    csv = get_data_by_filters dt_filters, csv
  end

  #enviar data a variable global '$dt_data'. esta se puede recuperar en cualqquier step y cualquier page object
  Config.dt_data = csv
  Config.dt_row = csv[1]
  puts "DATA => #{Config.dt_row}"

end

