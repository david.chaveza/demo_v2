#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  step donde manda el visit y el login #
#####################################################
require_relative '../../../../helpers/ws_test.rb'

#####################################################
#####################################################
#####################################################
#  INGLES
#     STEPS DEFAULT PARA WEB SERVICE
#####################################################
#####################################################
#####################################################

Given(/^I am a client$/) do
  @ws = WSTest.new
end

When(/^I test at web service "([^"]*)"$/) do |ws_name|
  @ws.response ws_name
  @ws_data = @ws.write_request_response_files
  save_evidence_file(@ws_data[:request][:data], @ws_data[:request][:file_name], @ws_data[:request][:file_extencion])
  save_evidence_file(@ws_data[:response_actual][:data], @ws_data[:response_actual][:file_name], @ws_data[:response_actual][:file_extencion])
  save_evidence_file(@ws_data[:response_esperado][:data], @ws_data[:response_esperado][:file_name], @ws_data[:response_esperado][:file_extencion])
end

Then(/^I get response code success$/) do
  @ws.assert_response_code
end

Then(/^I get response body success$/) do
  val = @ws.val_response_body
  data = @ws.write_response_body_log_fail(val)
  save_evidence_file(data[:log][:data], data[:log][:file_name], data[:log][:file_extencion])
  @ws.assert_response_body(val)
end

And(/^I get "([^"]*)" value from "([^"]*)" and set as var out$/) do |key, from_headers|
  @ws.set_var_out_from_header(key)
end

And(/^I get "([^"]*)" value from vars out$/) do |key_var_out|
  pending
end

And(/^I set "([^"]*)" value as var out$/) do |var_out|
  @ws.set_new_var_out(var_out)
end

#####################################################
#####################################################
#####################################################
#  ESPAÑOL
#     STEPS DEFAULT PARA WEB SERVICE
#####################################################
#####################################################
#####################################################
#
When(/^Soy un Cliente$/) do
  @ws = WSTest.new
end

When(/^Pruebo el web services "([^"]*)"$/) do |nombre_ws|
  @ws.response(nombre_ws)
  @ws_data = @ws.write_request_response_files
  id_archivo = timestamps
  save_evidence_file(@ws_data[:request][:data], "#{@ws_data[:request][:file_name]}_#{id_archivo}", @ws_data[:request][:file_extencion])
  save_evidence_file(@ws_data[:response_actual][:data], "#{@ws_data[:response_actual][:file_name]}_#{id_archivo}", @ws_data[:response_actual][:file_extencion])
  #save_evidence_file(@ws_data[:response_esperado][:data], @ws_data[:response_esperado][:file_name], @ws_data[:response_esperado][:file_extencion])
end

When(/^Valido que el Response Code sea exitoso$/) do
  @ws.assert_response_code
end

When(/^Valido que el response contenga el siguiente JSON PATH "([^"]*)"$/) do |json_path_expression|
  val = @ws.assert_response_json_path(json_path_expression)
  puts "VALIDACION JSON PATH EXPRESION"
  puts "#{json_path_expression} => #{val}"
end

And(/^Obtengo el valor "([^"]*)" desde los HEADERS y lo guardo como VARIABLE DE SALIDA$/) do |key|
  @ws.set_var_out_from_header(key)
  puts "VAR OUT => #{Config.get_var_out(key_var_out)}"
end

And(/^Obtengo la expresión JSON PATH "([^"]*)" del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA STRING: "([^"]*)"$/) do |json_path_expression, var_out_name|
  val = @ws.assert_response_json_path(json_path_expression)
  Config.set_var_out(var_out_name.to_s.upcase.strip, val[0].to_s.strip)
  puts "VAR OUT: #{var_out_name} => #{val}"
end

And(/^Obtengo la expresión JSON PATH "([^"]*)" del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA ARRAY: "([^"]*)"$/) do |json_path_expression, var_out_name|
  val = @ws.assert_response_json_path(json_path_expression)
  Config.set_var_out(var_out_name.to_s.upcase.strip, val[0])
  puts "VAR OUT: #{var_out_name} => #{val}"
end

And(/^Obtengo la variable "([^"]*)" desde VARIABLE DE SALIDA$/) do |key_var_out|
  @varout = nil
  @varout = Config.get_var_out(key_var_out)
  puts "VAR OUT => #{key_var_out} = #{@varout}"
end

And(/^Creo "([^"]*)" como VARIABLE DE SALIDA$/) do |var_out|
  @ws.set_new_var_out(var_out)
end

When(/^Creo la variable de SALIDA "([^"]*)" con el valor "([^"]*)" para WS$/) do |var_out, var_value|
  Config.set_var_out(var_out, var_value)
  market = '**********************************************'
  puts market
  puts "VAR OUT: #{var_out}. VALUE: #{var_value}"
  puts "VAR OUT CREADA CON EXITO!!!"
  puts market
end