# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  step donde manda el visit y el login #
#####################################################
require_relative '../../../../helpers/generic'

When(/^Step dummy passed$/) do
  puts "PASSED DUMMY STEP"
  save_evidence_execution
end

When(/^Step paso dummy failed$/) do
  save_evidence_execution
  raise "FAILED DUMMY STEP"
end

When(/^Step tomar evidencia$/) do
  save_evidence_execution
end

