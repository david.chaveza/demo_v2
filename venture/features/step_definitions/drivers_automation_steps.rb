# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY
#Descripcion: Iniciar drivers para trabajar con otra platafomra
#     Ej. se esta trabajando en web y se necesita mobile o desktop, webservices etc.
#####################################################
require_relative '../../../helpers/generic'
require_relative '../../../helpers/config.rb'
require_relative '../../../helpers/drivers/driver'
include Drivers

####################################################################################################################
####################################################################################################################
#   INICIAR DRIVERS
####################################################################################################################
####################################################################################################################

#####################################################
# CAPYBARA
#####################################################

When(/^Inicio Capybara Chrome Driver$/) do
  driver(:chrome_capybara)
end

When(/^Inicio Capybara WAM Driver$/) do
  driver(:wam_capybara)
end

When(/^Inicio Capybara Chrome Headless Driver$/) do
  driver(:chrome_headless_capybara)
end

When(/^Inicio Capybara WAM Headless Driver$/) do
  driver(:wam_capybara_headless)
end

When(/^Inicio Capybara Firefox Driver$/) do
  driver(:firefox_capybara)
end

When(/^Inicio Capybara IE Driver$/) do
  driver(:ie_capybara)
end

#####################################################
# BASADOS EN APPIUM
#####################################################

When(/^Inicio Appium Driver$/) do
  Config.driver_name = 'appium'
  driver(:appium)
end

When(/^Inicio Appium WAM Driver$/) do
  driver(:appium_wam)
end

When(/^Inicio Appium Desktop Driver$/) do
  driver(:appium_desktop)
end

When(/^Inicio WinAppDriver Driver$/) do
  driver(:winappdriver)
end


#####################################################
# WINIUM
#####################################################

When(/^Inicio Winium Driver$/) do
  driver(:winium)
end


#####################################################
# SELENIUM WEBDRIVER
#####################################################





####################################################################################################################
####################################################################################################################
#   QUIT DRIVERS
####################################################################################################################
####################################################################################################################
