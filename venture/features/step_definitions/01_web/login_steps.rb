# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  step donde manda el visit y el login #
#####################################################
require_relative '../../../../helpers/generic'

When(/^Se muestra la pagina de 'Login'$/) do
  login_page = WebPages::LoginPage.new
  login_page.assert_login_page_visible?
  save_evidence_execution
end

When(/^Hago login como "([^"]*)"$/) do |tipo_usuario|
  login_page = WebPages::LoginPage.new
  login_page.llenar_formulario_data_table(tipo_usuario)
  save_evidence_execution
  login_page.click_boton_login
end

When(/^Hago login en la app web$/) do
  #obtener datos
  email = Config.dt_row[:usuario]
  password = Config.dt_row[:password]

  login_page = WebPages::LoginPage.new
  login_page.llenar_formulario(email, password)
  save_evidence_execution
  login_page.click_boton_login
end

When(/^Se muestra el mensaje 'Login incorrecto'$/) do
  login_page = WebPages::LoginPage.new
  puts login_page.assert_lbl_login_incorrecto
  save_evidence_execution
end

When(/^Se muestra el mensaje 'password incorrecto'$/) do
  login_page = WebPages::LoginPage.new
  login_page.assert_lbl_password_incorrecto
  save_evidence_execution
end
