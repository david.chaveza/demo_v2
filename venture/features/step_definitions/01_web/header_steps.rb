# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  step donde manda el visit y el login #
#####################################################
require_relative '../../../../helpers/generic'

When(/^Seleccionar la opcion 'Sign in'$/) do
  header_page = WebPages::HeaderPage.new
  header_page.assert_sign_in_exist?
  save_evidence_execution
  header_page.click_sign_in
end

When(/^Se muestra la pagina de 'Home'$/) do
  header_page = WebPages::HeaderPage.new
  header_page.assert_sign_in_exist?
  save_evidence_execution
end

When(/^Busco 1 producto aleatorio desde el datapool csv "([^"]*)"$/) do |tipo_producto|
  header_page = WebPages::HeaderPage.new
  header_page.capturar_busqueda(tipo_producto)
  save_evidence_execution
  header_page.click_boton_buscar
  save_evidence_execution
end

When(/^Busco 1 producto aleatorio desde el datapool csv$/) do
  header_page = WebPages::HeaderPage.new
  header_page.capturar_busqueda_random
  save_evidence_execution
  header_page.click_boton_buscar
  save_evidence_execution
end