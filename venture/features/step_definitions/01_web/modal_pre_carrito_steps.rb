# encoding: utf-8
############################################################
#Autor: TESTER ACADEMY                                          #
#Descripcion:  step donde para acciones y validaciones plp #
############################################################
require_relative '../../../../helpers/generic'

When(/^Se muestra el modal de productos agregados al carrito$/) do
  modal_pre_carrito_page = WebPages::ModalPreCarritoPage.new
  modal_pre_carrito_page.assert_add_prod_successfull?
  save_evidence_execution
end

When(/^Selecciono la opción 'Proceed to checkout'$/) do
  modal_pre_carrito_page = WebPages::ModalPreCarritoPage.new
  modal_pre_carrito_page.select_btn_proceed_to_checout
  save_evidence_execution
end