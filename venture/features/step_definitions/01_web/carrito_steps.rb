# encoding: utf-8
############################################################
#Autor: TESTER ACADEMY                                          #
#Descripcion:  step donde para acciones y validaciones plp #
############################################################
require_relative '../../../../helpers/generic'

When(/^Se muestra la pagina de 'Carrito'$/) do
  carrito_page = WebPages::CarritoPage.new
  carrito_page.assert_carrito_page?
  save_evidence_execution
end

When(/^Actualizo la candidad de items$/) do
  carrito_page = WebPages::CarritoPage.new
  cantidad_inicial = carrito_page.obtener_cantidad_inicial_producto_uno
  save_evidence_execution
  carrito_page.incrementar_cantidad_inicial_producto_uno(cantidad_inicial)
  save_evidence_execution
end

When(/^Elimino el producto del carrito$/) do
  carrito_page = WebPages::CarritoPage.new
  carrito_page.eliminar_producto_uno_carrito
  save_evidence_execution
end

When(/^Se muestra el mensaje 'Tu carrito de compras esta vacio'$/) do
  carrito_page = WebPages::CarritoPage.new
  carrito_page.assert_carrito_vacio?
  save_evidence_execution
end