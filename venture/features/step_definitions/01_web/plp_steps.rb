# encoding: utf-8
############################################################
#Autor: TESTER ACADEMY                                          #
#Descripcion:  step donde para acciones y validaciones plp #
############################################################
require_relative '../../../../helpers/generic'

When(/^Se muestra la pagina de 'PLP'$/) do
  plp_page = WebPages::PlpPage.new
  plp_page.assert_plp?
  save_evidence_execution
end


When(/^Selecciono un producto$/) do
  plp_page = WebPages::PlpPage.new
  plp_page.select_primer_prod_plp
  save_evidence_execution
end