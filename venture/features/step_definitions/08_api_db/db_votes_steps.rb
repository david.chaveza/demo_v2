# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  step donde manda el visit y el login #
#####################################################
require_relative '../../../../helpers/generic'

#STEP DUMMY - EMULA INSERT DE LA DB QUE DEBERIA HACER LA API
When(/^Inserto el registro en la DB para la tabla VOTES$/) do
  votes_db = DataBase::VotesDB.new
  @data_api = Config.get_var_out("API_VOTE_DATA")
  id = @data_api['id']
  image_id = @data_api['image_id']
  sub_id = @data_api['sub_id']
  value = @data_api['value']
  country_code = @data_api['country_code']
  @data_db = votes_db.insert_vote(id, image_id, sub_id, value, country_code)
  puts "DB SQL. Registro insertado: #{@data_api}"
end

When(/^Obtengo la data de la Query SQL votes para el ID_VOTES$/) do
  votes_db = DataBase::VotesDB.new
  @data_api = Config.get_var_out("API_VOTE_DATA")
  @data_db = votes_db.get_vote(@data_api["id"])
  puts "DB SQL Data: #{@data_db}"
end

When(/^Valido la información del API VS DB para la Query SQL Votes$/) do
  votes_db = DataBase::VotesDB.new
  puts "DB SQL Data: #{@data_db}"
  puts "API Data: #{@data_api}"
  raise "Error. DATA DB no se encontraron datos" if @data_db.nil?
  raise "Error. DATA DB no se encontraron datos" if @data_db.empty?
  votes_db.assert_db_vs_api(@data_db[0]['ID_VOTE'],
                            @data_db[0]['IMAGE_ID'], @data_db[0]['SUB_ID'],
                            @data_db[0]['VALUE_VOTE'], @data_db[0]['COUNTRY'],
                            @data_api["id"], @data_api["image_id"], @data_api["sub_id"],
                            @data_api["value"], @data_api["country_code"])
end