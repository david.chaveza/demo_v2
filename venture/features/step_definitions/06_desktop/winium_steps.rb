# encoding: utf-8
############################################################
#Autor: TESTER ACADEMY                                          #
#Descripcion: steps para
############################################################
require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

When(/^Se muestra la pagina de 'Home Bony System'$/) do
  true
end

When(/^Selecciono la opción 'Salir del Sistema'$/) do
  menu_inicio_config = WiniumPages::MenuInicioConfigPage.new
  menu_inicio_config.seleccionar_salir
end

When(/^Se muestra el Alert 'Confirmar Salir del Sistema'$/) do
  @alert_page = WiniumPages::AlertsPage.new
  @alert_page.get_alert
  save_evidence_execution
end

When(/^Selecciono el boton 'Si' del Alert$/) do
  @alert_page.confirmar_alerta
end

When(/^Selecciono el boton 'No' del Alert$/) do
  @alert_page.cancelar_alerta
end

When(/^Valido que se halla cerrado el sistema 'Bony System'$/) do
  sleep 2
  save_evidence_execution
end

When(/^Selecciono el tab menu "([^"]*)"$/) do |menu_tab|
  menu_headers = WiniumPages::MenusHeaderPage.new
  menu_headers.seleccionar_menu(menu_tab)
end

When(/^Se muestra la pagina de 'Conejos en Criadero'$/) do
  @conejos_criadero = WiniumPages::ConejosEnCriaderoPage.new
  @conejos_criadero.assert_page_visible
  save_evidence_execution
end

When(/^Busco el conejo agregado$/) do
  conejo = Config.get_var_out('conejo_agregado')
  @conejos_criadero.buscar_conejo(conejo)
  save_evidence_execution
end

When(/^Selecciono la opción 'Agregar conejo' del menu 'Conejos En Criadero'$/) do

end
