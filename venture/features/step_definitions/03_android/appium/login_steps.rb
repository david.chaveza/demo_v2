# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY
#Descripcion:  steps
#####################################################
require_relative '../../../../../helpers/generic'

When(/^Se visualiza la pagina de 'Login' de la 'APP DEMO'$/) do
  @login_page = AndroidAppiumPages::LoginPage.new
  @login_page.assert_login_page_visible?
  save_evidence_execution
end

When(/^Hago login con "([^"]*)" en la 'APP DEMO'$/) do |tipo_usuario|
  @login_page.llenar_formulario_data_table(tipo_usuario)
  save_evidence_execution
  @login_page.touch_boton_login
end

When(/^Hago login en la 'APP DEMO'$/) do
  #obtener datos
  usuario = Config.dt_row[:usuario]
  password = Config.dt_row[:password]

  @login_page.llenar_formulario(usuario, password)
  save_evidence_execution
  @login_page.touch_boton_login
end

When(/^Se visualiza el popup de 'Login exitoso'$/) do
  @login_page.assert_login_correcto
  save_evidence_execution
end

When(/^Se visualiza el popup de 'Login incorrecto'$/) do
  @login_page.assert_login_incorrecto
  save_evidence_execution
end

When(/^Acepto la alerta del popup login$/) do
  @login_page.touch_boton_aceptar_alert
end

