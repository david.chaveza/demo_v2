# language: es
# encoding: utf-8
Característica: LOGIN - APP DEMO - APPIUM

  @regresion @MIXPLATFORM_01
  Escenario: MIXPLATFORM_01 - LOGIN EXITOSOS
    Dado Abro el aplicativo sistema
    Cuando Seleccionar la opcion 'Sign in'
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login como "usuario valido"
    Entonces Se muestra la pagina de 'Mi Cuenta'
    Dado Inicio Appium Driver
    Dado Abro la aplicacion con appium-android
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login con "usuario valido app demo" en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login exitoso'
