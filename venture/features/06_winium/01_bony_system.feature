# language: es
# encoding: utf-8
Característica: BONY SYSTEM - DESKTOP WINIUM

  @regresion @winium @TC_WINIUM_001
  Escenario: TC_WINIUM_001 - CERRAR APP DESDE BOTON SALIR
    Dado Abro el aplicativo desktop 'Bony System'
    Entonces Se muestra la pagina de 'Home Bony System'
    Cuando Selecciono la opción 'Salir del Sistema'
    Entonces Se muestra el Alert 'Confirmar Salir del Sistema'
    Y Selecciono el boton 'Si' del Alert
    Entonces Valido que se halla cerrado el sistema 'Bony System'

  @regresion @winium @TC_WINIUM_002
  Escenario: TC_WINIUM_002 - CANCELAR CERRAR APP DESDE BOTON SALIR
    Dado Abro el aplicativo desktop 'Bony System'
    Entonces Se muestra la pagina de 'Home Bony System'
    Cuando Selecciono la opción 'Salir del Sistema'
    Entonces Se muestra el Alert 'Confirmar Salir del Sistema'
    Y Selecciono el boton 'No' del Alert
    Entonces Se muestra la pagina de 'Home Bony System'

  @regresion @winium @TC_WINIUM_003
  Escenario: TC_WINIUM_003 - AGREGAR CONEJO
    Dado Abro el aplicativo desktop 'Bony System'
    Entonces Se muestra la pagina de 'Home Bony System'
    Cuando Selecciono el tab menu "Conejos En Criadero"
    Entonces Se muestra la pagina de 'Conejos en Criadero'
    Entonces Selecciono la opción 'Agregar conejo' del menu 'Conejos En Criadero'

  @regresion @winium @TC_WINIUM_004
  Escenario: TC_WINIUM_004 - BUSCAR CONEJO AGREGADO
    Dado Abro el aplicativo desktop 'Bony System'
    Entonces Se muestra la pagina de 'Home Bony System'
    Cuando Selecciono el tab menu "Conejos En Criadero"
    Entonces Se muestra la pagina de 'Conejos en Criadero'
    Entonces Busco el conejo agregado

