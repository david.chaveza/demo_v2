# language: es
# encoding: utf-8
Característica: LOGIN - APP DEMO - APPIUM

  @regresion @android @APP_ADR_TC01 @TC_29957
  Escenario: APP_ADR_TC01 - LOGIN EXITOSO
    Dado Abro la aplicacion con appium-android
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login con "usuario valido app demo" en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login exitoso'
    
  @regresion @android @APP_ADR_TC01 @TC_ABC
  Escenario: APP_ADR_TC01 - LOGIN EXITOSO - CSV DATA
    Dado Abro la aplicacion con appium-android
    Y Obtengo los datos de prueba del archivo "dt_login" data pool y filtro por "tipo_usuario = usuario valido app demo"
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login exitoso'
    
  @regresion @android @APP_ADR_TC02 @TC_29958
  Escenario: APP_ADR_TC02 - LOGIN USUARIO INCORRECTO
    Dado Abro la aplicacion con appium-android
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login con "usuario incorrecto app demo" en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login incorrecto'



