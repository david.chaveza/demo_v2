# language: es
# encoding: utf-8
Característica: OTROS TEST - APP DEMO - CALABASH

  @regresion @mobile @cal_android @CAL_TC03
  Escenario: CAL_TC03 - CONSULTAR REGISTROS
    Dado Abro la aplicacion con appium-android
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login con "usuario valido app demo" en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login exitoso'
    Y Acepto la alerta del popup login
    Entonces Se visualiza la pagina de opciones
    Cuando Selecciono la opción "CONSULTAR REGISTROS"
    Y Consulto los siguientes registros
      | registro                |
      | 13 - CALABASH ANDROID   |
      | 19 - CUCUMBER CON RUBY  |
      | 2 - ZOE TERESA SHAORAN  |
    Entonces Se valida que todos los registros fueron encontrados

  @regresion @mobile @cal_android @CAL_TC04
  Escenario: CAL_TC04 - VALIDAR PUSH NOTIFICATIONS
    Dado Abro la aplicacion con appium-android
    Entonces Se visualiza la pagina de 'Login' de la 'APP DEMO'
    Cuando Hago login con "usuario valido app demo" en la 'APP DEMO'
    Entonces Se visualiza el popup de 'Login exitoso'
    Y Acepto la alerta del popup login
    Entonces Se visualiza la pagina de opciones
    Cuando Selecciono la opción "CREAR PUSH NOTIFICATIONS"
    Y Abro la barra de notificaciones del dispositivo
    Entonces Se visualiza la notificacion de 'app demo'



