# language: es
# encoding: utf-8
Característica: API DB

  @regresion @ws @TC_WS_001
  Escenario: GET CAT RANDOM
    Dado I am a client
    Cuando I test at web service "get cat random"
    Entonces I get response code success
    Y I get response body success
    Y Valido que el response contenga el siguiente JSON PATH "$.[0].id"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != null)]"
    Y Valido que el response contenga el siguiente JSON PATH "$.[?(@.id != '')]"

  @regresion @ws @TC_WS_002
  Escenario: VOTE - FAIL DUMMY
    Dado I am a client
    Cuando Pruebo el web services "vote"
    Entonces Valido que el Response Code sea exitoso

  @regresion @ws @TC_WS_003
  Escenario: VOTE - DB
    Dado I am a client
    Cuando Pruebo el web services "get cat random"
    Entonces Valido que el Response Code sea exitoso
    Y Obtengo la expresión JSON PATH "$.[0].id" del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA STRING: "IMAGE_ID"
    Cuando Pruebo el web services "vote"
    Y Obtengo la expresión JSON PATH "$." del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA ARRAY: "API_VOTE_DATA"
    Cuando Inserto el registro en la DB para la tabla VOTES
    Y Obtengo la data de la Query SQL votes para el ID_VOTES
    Y Valido la información del API VS DB para la Query SQL Votes

  @regresion @ws @TC_WS_004
  Escenario: VOTE - DB - FAIL
    Dado I am a client
    Cuando Pruebo el web services "get cat random"
    Entonces Valido que el Response Code sea exitoso
    Y Obtengo la expresión JSON PATH "$.[0].id" del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA STRING: "IMAGE_ID"
    Cuando Pruebo el web services "vote"
    Y Obtengo la expresión JSON PATH "$." del RESPONSE BODY y lo guardo como VARIABLE DE SALIDA ARRAY: "API_VOTE_DATA"
    Y Obtengo la data de la Query SQL votes para el ID_VOTES
    Y Valido la información del API VS DB para la Query SQL Votes

  @regresion @ws @TC_WS_005
  Escenario: VOTE FAVOURITES
    Dado I am a client
    Cuando I test at web service "vote favourites"
    Entonces I get response code success

