require 'yaml'
require_relative '../../../helpers/config.rb'
require_relative '../../../helpers/generic.rb'
# HELPER DRIVERS
require_relative '../../../helpers/drivers/driver_appium'
require_relative '../../../helpers/drivers/driver_winium'
require_relative '../../../helpers/drivers/driver_capybara'
# HELPER PARA COMUNICAR CON LAMBDATEST
require_relative '../../../helpers/drivers/lambdatest/driver_lambdatest'
# HELPER PARA COMUNICAR CON SPIRA
require_relative '../../../helpers/qa_management_tools/spira/SpiraController'
# HELPER PARA COMUNICAR CON TELEGRAM
require_relative '../../../helpers/reporter/telegram/send_message/messages_utils.rb'
require_relative '../../../helpers/reporter/telegram/send_document/document_utils.rb'
# HELPER PARA COMUNICAR CON QA-TOUCH
require_relative '../../../helpers/qa_management_tools/qa_touch/add_results/AddResultsController'

#-------------------------------------------------------------------
# NOMBRE DEL PROYECTO
#-------------------------------------------------------------------
@@nombre_proyecto = "DEMO V2 - TAF"

#-------------------------------------------------------------------
# ANTES DE CADA ESCENARIO
#-------------------------------------------------------------------
Before do |scenario|
  #Recupera el nombre del feautre, para tomar evidencia dentro de los step
  Config.current_feature = scenario.feature.name
  #Recupera el nombre del scenario, para tomar evidencia dentro de los step
  Config.current_scenario = scenario.name
end

#-------------------------------------------------------------------
# DESPUES DE CADA ESCENARIO
#-------------------------------------------------------------------
After do |scenario|
  unless Config.driver_name.to_s.to_sym == :ws
    if scenario.failed? and Config.driver_name.to_s.downcase.include?('capybara')
      #Reset capybara en exception net read timeout
      Drivers::capybara_reset_in_net_readtimeout_ex(scenario)
      #EVIDENCIA EN ESTATUS FAIL
      save_evidence_execution_fail
    end

    #QUIT CAPYBARA DRIVER
    Capybara.current_session.driver.quit if Config.driver_name.to_s.downcase.include?('capybara')
    #LIMPIAR CACHE
    #page.execute_script('if (localStorage && localStorage.clear) localStorage.clear();')
    Capybara.reset_sessions! if Config.driver_name.to_s.downcase.include?('capybara')
    #RESET APPIUM
    Config.driver_appium.reset if Config.driver_name.to_s.downcase.include?('appium')

    #-------------------------------------------------------------------
    #CONEXION LAMBDATEST - ENVIA EL ESTATUS DEL TEST CASE
    #-------------------------------------------------------------------
    Drivers::set_result_lambdatest(scenario.name,
                                   scenario.failed? ? :failed : :passed,
                                   scenario.failed? ? scenario.exception.message : nil
    ) if Config.driver_name.to_s.downcase.include?('lambdatest_capybara')
  end

  #-------------------------------------------------------------------
  #CONEXION SPIRA - TODOS LOS 'AUTOMATION_DRIVER' Y 'DRIVERS_NAME'
  #-------------------------------------------------------------------
  #spira_controller = QAManagementTools::Spira::SpiraController.new
  #spira_controller.get_data_scenario_exec(scenario)
  #puts spira_controller.send_result_to_spira
  #-------------------------------------------------------------------

  #-------------------------------------------------------------------
  # TELEGRAM MENSAJE TEST CASE STATUS
  #-------------------------------------------------------------------
  #crear request para enviar mensaje del estatus dle test
  send_telegram_message_test_status(scenario.name,
                                    scenario.failed? ? 'FAILED' : 'PASSED',
                                    scenario.failed? ? scenario.exception.message : nil
  )
  #crear request para enviar foto del fail
  #   "Config.current_evidence_file" OBTIENE EL ULTIMA PATH + FILE NAME DE IMAGEN DE EVIDENCIA
  #send_telegram_document(Config.current_evidence_file,
  #                       scenario.failed? ? "EVIDENCIA FAILED - #{scenario.name}" : "EVIDENCIA PASSED - #{scenario.name}"
  #)

  #-------------------------------------------------------------------
  # CONEXION QA-TOUCH
  #-------------------------------------------------------------------
  qa_touch_controller = QAManagementTools::QATouch::AddResultsController.new
  qa_touch_controller.add_results_to_qa_touch(scenario,
                                              Config.current_screenshot_path,
                                              true
  )

end

#-------------------------------------------------------------------
# DESPUES DE CADA PASO (STEP)
#-------------------------------------------------------------------
AfterStep do |scenario|

end

#-------------------------------------------------------------------
# ANTES DE EJECUTAR LA SUITE (FEATURES)
#-------------------------------------------------------------------
AfterConfiguration do |config|
  puts "AFTER CONFIG #{config.feature_dirs}"
  #init_path
  #-------------------------------------------------------------------
  # TELEGRAM MENSAJE INICIAL
  #-------------------------------------------------------------------
  send_telegram_message_inicial(@@nombre_proyecto)
end

#-------------------------------------------------------------------
# DESPUES DE EJECUTAR LA SUITE (FEATURES)
#-------------------------------------------------------------------
at_exit do
  #-------------------------------------------------------------------
  # TELEGRAM MENSAJE FINAL
  #-------------------------------------------------------------------
  send_telegram_message_final(@@nombre_proyecto)

  #-------------------------------------------------------------------
  #CONEXION QMETRY - ENVIAR RESULTADOS
  #-------------------------------------------------------------------
  # url_qmetry_upload_file = nil
  # url_qmetry_upload_file = qmetry_import_results('FV20-TR-488') if Config.driver_name.to_s[-8..-1] == 'capybara'
  # url_qmetry_upload_file = qmetry_import_results('FV20-TR-492') if Config.driver_name.to_s.downcase.include?('wam')
  # url_qmetry_upload_file = qmetry_import_results('FV20-TR-495') if Config.driver_name.to_s.downcase == ('ws')
  # if Config.driver_name.to_s.downcase == 'appium'
  #   url_qmetry_upload_file = qmetry_import_results('FV20-TR-493') if Config.get_data_device[:platform_name].to_s.downcase == 'android'
  #   url_qmetry_upload_file = qmetry_import_results('FV20-TR-494') if Config.get_data_device[:platform_name].to_s.downcase == 'ios'
  # end
  # puts "QMETRY => URL VACIA" if url_qmetry_upload_file.nil?
  # puts "QMETRY => URL UPLOAD FILE => '#{url_qmetry_upload_file}'" unless url_qmetry_upload_file.nil?
  # puts "QMETRY => FILE CUCUBMER RESULTS: #{Config.path_results}/results.json" unless url_qmetry_upload_file.nil?
  # qmetry_upload_file(url_qmetry_upload_file, "#{Config.path_results}/results.json")

  #-------------------------------------------------------------------
  # DRIVERS - QUIT, STOP SERVERS, KILL CMD/SHELL SERVER
  #-------------------------------------------------------------------
  include Drivers
  appium_driver_quit(Config.driver)
  winium_driver_quit(Config.driver)
  appium_driver_quit(Config.driver_appium)
  appium_driver_quit(Config.driver_winappdriver)
  winium_driver_quit(Config.driver_winium)
  kill_all_appium_server
  kill_cmd_shell_appium_server
  kill_all_winium_server
  kill_cmd_shell_winium_server
  #-----------------------------------------------

end