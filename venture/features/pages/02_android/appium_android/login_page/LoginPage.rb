# encoding: utf-8

require_relative 'LoginUIElements'
require_relative '../../../../../../helpers/generic'
require_relative '../../../../../../helpers/uielement_actions/or_actions_ui'

module AndroidAppiumPages
  class LoginPage < AndroidAppiumPages::UIElements::LoginUIElements

    #############################################################################
    # ACCIONES
    #############################################################################

    def llenar_formulario(usuario, password)
      or_exec_uielement(@txt_usuario, usuario)
      or_exec_uielement(@txt_password, password)

    rescue Exception => e
      raise("Error => no se pudo llenar el formulario 'login'.
           \nException =>#{e.message}"
      )
    end

    def llenar_formulario_data_table(tipo_usuario)
      #OBTENER DATOS DE CSV
      data = get_data_by_filters("tipo_usuario = #{tipo_usuario.to_s.strip}", @dt_login)[1]
      puts "CSV DATA => dt_row => #{data}"

      or_exec_uielement(@txt_usuario, data[:usuario])
      or_exec_uielement(@txt_password, data[:password])

    rescue Exception => e
      raise("Error => no se pudo llenar el formulario 'login'.
           \nException =>#{e.message}"
      )
    end

    def touch_boton_login
      or_exec_uielement(@btn_login)

    rescue Exception => e
      raise("Error => al presionar en el boton 'SIGN IN'.
           \nException =>#{e.message}"
      )
    end

    def touch_boton_aceptar_alert
      or_exec_uielement(@btn_login)

    rescue Exception => e
      raise("Error => al presionar en el boton 'SIGN IN'.
           \nException =>#{e.message}"
      )
    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_login_page_visible?
      el = or_get_uielement(@txt_usuario)
      puts "TXT USUARIO = #{el.to_s}"
    end

    def assert_login_correcto
      titulo_alert_esperado = @dt_yml['lbl_titulo_alert_login_exitoso'].to_s
      titulo_alert = or_exec_uielement(@lbl_titulo_alert).uielement.attribute('text')
      gen_assert_text(titulo_alert, titulo_alert_esperado,
                  "La leyenda del popup 'Login Exitoso' no es la esperada"
      )
    end

    def assert_login_incorrecto
      titulo_alert_esperado = @dt_yml['lbl_titulo_alert_login_incorrecto'].to_s
      titulo_alert = or_exec_uielement(@lbl_titulo_alert).uielement.attribute('text')
      gen_assert_text_contains(titulo_alert, titulo_alert_esperado,
                  "La leyenda del popup 'Login Incorrecto' no es la esperada"
      )
    end


  end

end

