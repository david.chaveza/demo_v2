# encoding: utf-8
require_relative '../../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../../helpers/data_driven/yml_data'

module AndroidAppiumPages
  module UIElements
    class LoginUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @driver = Config.driver_appium
        #@asserts = ORActions::ORCalabash::UIElementsAsserts.new(world)
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_login'
        #DATA CSV
        @dt_login = get_csv_data('dt_login')
        #DATA YML
        @dt_yml = get_yml_data('dt_yml_login', @path_page)

        #UIELEMENTS
        @txt_usuario = get_or_data('txt_usuario', @or_file, @path_page, @driver)
        @txt_password = get_or_data('txt_password', @or_file, @path_page, @driver)
        @btn_login = get_or_data('btn_login', @or_file, @path_page, @driver)
        @lbl_titulo_alert = get_or_data('lbl_titulo_alert', @or_file, @path_page, @driver)
        @btn_aceptar_alert = get_or_data('btn_aceptar_alert', @or_file, @path_page, @driver)
      end


    end
  end
end

