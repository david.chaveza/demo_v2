# encoding: utf-8
require 'selenium-webdriver'
require 'rspec'
require 'rspec/expectations'
require_relative 'AlertsPageUIElements'


module WiniumPages
  class AlertsPage < WiniumPages::UIElements::AlertsPageUIElements

    #############################################################################
    # ACCIONES
    #############################################################################

    def get_alert
      wnd_main = or_get_uielement(@wnd_main)
      alert = wnd_main.uielement.find_element(@wnd_alert[:selector].to_s.strip.to_sym,
                                              @wnd_alert[:locator].to_s.strip
      )
      return alert

    rescue Exception => e
      raise "Error. No se encontro la Alerta. Exception => #{e.message}"
    end

    def confirmar_alerta
      alert = get_alert
      btn_si = get_alert.find_element(@btn_alert_si[:selector].to_s.strip.to_sym,
                                      @btn_alert_si[:locator].to_s.strip
      )
      @ui_elements_action.click_uielement(btn_si)

    rescue Exception => e
      raise("Error => Al confirmar la Alerta.
           \nException =>#{e.message}"
      )
    end

    def cancelar_alerta
      alert = get_alert
      btn_no = get_alert.find_element(@btn_alert_no[:selector].to_s.strip.to_sym,
                                      @btn_alert_no[:locator].to_s.strip
      )
      @ui_elements_action.click_uielement(btn_no)

    rescue Exception => e
      raise("Error => Al cancelar la Alerta.
           \nException =>#{e.message}"
      )
    end



    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################


  end

end

