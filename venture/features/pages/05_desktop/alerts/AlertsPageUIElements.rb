# encoding: utf-8
require_relative '../../../../../helpers/uielement_actions/or_winium/UIElementsActions'
require_relative '../../../../../helpers/uielement_actions/or_winium/UIElementsAsserts'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WiniumPages
  module UIElements
    class AlertsPageUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @driver = Config.driver_winium
        @asserts = ORActions::ORWinium::UIElementsAsserts.new
        @ui_elements_action = ORActions::ORWinium::UIElementsActions.new(@driver)
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_alerts'

        #UIELEMENTS
        @wnd_main = get_or_data('wnd_main', @or_file, @path_page, @driver)
        @wnd_alert = get_or_data('wnd_alert', @or_file, @path_page, @driver)
        @btn_alert_si = get_or_data('btn_alert_si', @or_file, @path_page, @driver)
        @btn_alert_no = get_or_data('btn_alert_no', @or_file, @path_page, @driver)
      end


    end
  end
end

