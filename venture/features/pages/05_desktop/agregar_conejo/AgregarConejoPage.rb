# encoding: utf-8
require 'selenium-webdriver'
require 'rspec'
require 'rspec/expectations'
require_relative 'AgregarConejoUIElements'


module WiniumPages
  class AgregarConejoPage < WiniumPages::UIElements::AgregarConejoUIElements

    #############################################################################
    # ACCIONES
    #############################################################################

    def buscar_conejo(nombre_conejo)
      wnd_main = or_get_uielement(@wnd_main)
      txt_buscar = wnd_main.uielement.find_element(@txt_buscar_conejo[:selector].to_s.strip.to_sym,
                                                   @txt_buscar_conejo[:locator].to_s.strip
      )
      @ui_elements_action.send_keys_to_uielement(ui_menu, nombre_conejo)

    rescue Exception => e
      raise("Error => Al Buscar Conejo: '#{nombre_conejo}'.
           \nException =>#{e.message}"
      )
    end



    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_page_visible
      wnd_main = or_get_uielement(@wnd_main)
      lbl_buscar_conejos = wnd_main.uielement.find_element(@lbl_buscar_conejo[:selector].to_s.strip.to_sym,
                                                           @lbl_buscar_conejo[:locator].to_s.strip
      )
      lbl_buscar_conejos.attribute('Name')
    rescue Exception => e
      raise "Error. No se visualiza la pagina: 'Conejos en Criadero'.
            \nException => #{e.message}"
    end


  end

end

