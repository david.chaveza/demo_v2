# encoding: utf-8
require 'selenium-webdriver'
require 'rspec'
require 'rspec/expectations'
require_relative 'MenusHeaderUIElements'


module WiniumPages
  class MenusHeaderPage < WiniumPages::UIElements::MenusHeaderUIElements

    #############################################################################
    # ACCIONES
    #############################################################################

    def seleccionar_menu(menu)
      wnd_main = or_get_uielement(@wnd_main)
      selector = @menu_header[:selector].to_s.strip.to_sym
      locator = String.new(@menu_header[:locator])
      locator = locator_override(locator, [menu])
      ui_menu = wnd_main.uielement.find_element(selector, locator)
      @ui_elements_action.click_uielement(ui_menu)
      unless ui_menu.attribute('IsSelected')
        raise "El menu tab: '#{menu}' no esta seleccionado"
      end
    rescue Exception => e
      raise("Error => Al seleccionar el menu: '#{menu}'.
           \nException =>#{e.message}"
      )
    end



    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################



  end

end

