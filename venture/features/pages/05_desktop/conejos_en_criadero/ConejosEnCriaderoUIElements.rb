# encoding: utf-8
require_relative '../../../../../helpers/uielement_actions/or_winium/UIElementsActions'
require_relative '../../../../../helpers/uielement_actions/or_winium/UIElementsAsserts'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WiniumPages
  module UIElements
    class ConejosEnCriaderoUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @driver = Config.driver_winium
        @asserts = ORActions::ORWinium::UIElementsAsserts.new
        @ui_elements_action = ORActions::ORWinium::UIElementsActions.new(@driver)
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_conejos_criadero'

        #UIELEMENTS
        @wnd_main = get_or_data('wnd_main', @or_file, @path_page, @driver)
        @lbl_buscar_conejo = get_or_data('lbl_buscar_conejo', @or_file, @path_page, @driver)
        @txt_buscar_conejo = get_or_data('txt_buscar_conejo', @or_file, @path_page, @driver)


      end


    end
  end
end

