# encoding: utf-8
require 'selenium-webdriver'
require 'rspec'
require 'rspec/expectations'
require_relative 'MenuInicioConfigUIElements'


module WiniumPages
  class MenuInicioConfigPage < WiniumPages::UIElements::MenuInicioConfigUIElements

    #############################################################################
    # ACCIONES
    #############################################################################

    def seleccionar_salir
      wnd_main = or_get_uielement(@wnd_main)
      btn_salir = wnd_main.uielement.find_element(@btn_salir[:selector].to_s.strip.to_sym,
                                                  @btn_salir[:locator].to_s.strip
      )
      @ui_elements_action.click_uielement(btn_salir)

    rescue Exception => e
      raise("Error => Al seleccionar el menu: '#{menu}'.
           \nException =>#{e.message}"
      )
    end



    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################



  end

end

