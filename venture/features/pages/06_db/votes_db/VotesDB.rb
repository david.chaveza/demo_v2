# encoding: utf-8
require_relative 'VotesQuerys'
require_relative '../../../../../helpers/generic.rb'

module DataBase
  class VotesDB < DataBase::Querys::VotesQuerys

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################

    def get_vote(vote_id)
      @my.open_connection
      query = q_get_vote(vote_id)
      return @my.get_sql_data(query)

    rescue Exception => e
      raise("Error => Al obtener query 'get vote'.
           \nException =>#{e.message}"
      )
    end

    def insert_vote(id_vote, image_id, sub_id, value_vote, country)
      @my.open_connection
      query = q_insert_vote(id_vote, image_id, sub_id, value_vote, country)
      @my.insert_data(query)

    rescue Exception => e
      raise("Error => Al ejecutar insert  'vote'.
           \nException =>#{e.message}"
      )
    end

    def assert_db_vs_api(db_id_vote, db_image_id, db_sub_id, db_value_vote, db_country,
                         api_id_vote, api_image_id, api_sub_id, api_value_vote, api_country)
      res = Array.new
      res.push( gen_verify_text(db_id_vote, api_id_vote, "Validacion API VS DB Fail") )
      res.push( gen_verify_text(db_image_id, api_image_id, "Validacion API VS DB Fail") )
      res.push( gen_verify_text(db_sub_id, api_sub_id, "Validacion API VS DB Fail") )
      res.push( gen_verify_text(db_value_vote, api_value_vote, "Validacion API VS DB Fail") )
      res.push( gen_verify_text(db_country, api_country, "Validacion API VS DB Fail") )

      raise "Error. Validacion API VS DB Fail." if res.to_s.include?('false')

    rescue Exception => e
      raise("Error => Al validar los datos de la DB VS API.
           \nException =>#{e.message}"
      )
    end


  end
end

