# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/data_driven/mysql/MySQLData.rb'

module DataBase
  module Querys
    class VotesQuerys

      def initialize
        @my = DataDriven::MySql::MySQLData.new
      end

      def q_get_vote(id_vote)
        return "select * from apicat.votes as v where v.ID_VOTE = #{id_vote};"
      end

      def q_insert_vote(id_vote, image_id, sub_id, value_vote, country)
        return "insert into apicat.votes (ID_VOTE, IMAGE_ID, SUB_ID, VALUE_VOTE, COUNTRY)
                values (#{id_vote}, '#{image_id}', '#{sub_id}', #{value_vote}, '#{country}')"
      end


    end
  end
end

