#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  clase metodos genericos adicionales  #
# para el negocio proyecto                          #
#####################################################
require_relative '../../../../helpers/generic.rb'
require_relative '../../../../helpers/ws_test'
require_relative '../../../../helpers/config'
#require 'unirest'


class WSTest


  #Método para enviar el request y recibir el response
  def response(ws_name = nil)
    @ws = ws_name
    @data_ws_filters = nil
    @data_ws_filters = "env_name = #{Config.environment.to_s.downcase.strip}"

    #ENVIAR REQUEST Y OBTENER EL RESPONSE
    @r = get_reponse @ws, nil, @data_ws_filters
    #puts "RESPONSE CODE => #{@r.code}"
    #puts "REPONSE BODY => #{@r.raw_body}"
  end

  #Assert para validar que el response code, sea igual al response code esperado
  def assert_response_code
    assert_ws_response_code @ws, @r
  end

  #Assert para validar que el response body, sea igual al response body esperado
  def val_response_body
    val = assert_ws_response_body @ws, @r
    return val
  end

  #Assert para validar que el response body, sea igual al response body esperado
  # @params
  # * :val Array HASH MAP con los datos de validación del response
  def assert_response_body(val)
    ws = get_ws(@ws)

    if val.size > 0 and @r.code.to_s.strip == ws[:expected_response_code].to_s.strip
      puts("assert_response_body => Warning. Algunos elementos del response no hacen match.
            \nVer archivo de logs en evidencias")
    end
    if val.size > 0 and  @r.code.to_s.strip != ws[:expected_response_code].to_s.strip
      require 'nokogiri'
      fail("assert_response_body => Response Body no esperado.\nVer archivo de logs en evidencias
              \n***************************************************************************************
              \nREPONSE BODY ESPERADO
              \n#{Nokogiri::XML( ws[:expected_response_body] ).to_xml.to_s[0..300]}...
              \n***************************************************************************************
              \nREPONSE BODY ACTUAL
              \n#{Nokogiri::XML( @r.body ).to_xml.to_s[0..300]}...
              \n***************************************************************************************")
    end

  end

  #Assert para validar que el response body, sea igual al response body esperado
  def response_headers
    val = get_response_headers @ws, @r
    return val
  end

  def assert_response_json_path(json_path_expression)
    val = assert_json_path_value(@r.body, json_path_expression)
    return val
  end

  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################
  # GET & SET VARS OUT
  #     se obtienen y crear las variables de salida usadas en los template de request y response
  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################

  #Metodo que permite setear un VAR OUT desde un valor de los RESPONSE HEADERS
  # @params
  #   :header_name Nombre del header al cual se va recuperar el valor
  def set_var_out_from_header(header_name)
    ws = get_ws(@ws)
    #obtener response body
    header_value = @r[header_name.to_s.to_sym]
    Config.set_var_out("#{header_name.to_s.upcase.strip}", header_value.to_s.strip)
  end

  #Metodo que permite setear un VAR OUT desde un valor de los RESPONSE HEADERS
  # @params
  #   :header_name Nombre del header al cual se va recuperar el valor
  def set_new_var_out(var_out)
    ws = get_ws(@ws)
    driver_http = ws[:driver].to_s.downcase.strip

    #obtener response body, dependiendo el driver http
    case var_out.to_s.downcase.strip
      when 'email_random'
        email = email_random
        Config.set_var_out("#{var_out.to_s.upcase.strip}", email.to_s.strip)
      when 'account_random'
        account = "TEST_#{timestamps}"
        Config.set_var_out("#{var_out.to_s.upcase.strip}", account.to_s.strip)
      else
        puts("set_new_var_out => opción  no valida => #{var_out}")
    end

  end


  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################
  # ESCRIBIR ARCHIVOS
  #   - REQUEST
  #   - RESPONSE
  #   - RESPONSE ESPERADO
  #   - LOGS
  #   - LOGS FAILS
  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################

  #Método que escribe en archivo de evidencia los log de warning o fail del request body
  # @params
  # * :data HASH MAP con los datos de la comparacion entre response obtenido y response esperado
  #         EJEMPLO: { :nodo_padre => @el_padre, :nodo_hijo => el_hijo,
  #                       :res_experado => values[0], :res_obtenido => values[1],
  #                        :mensaje => 'Valores no hacen match. Valor NULL detectado', :result => false
  #                   }
  def write_response_body_log_fail(data)
    log = StringIO.new
    marker = '*******************************************************************************************'

    for i in 0..(data.size - 1)
      log << "#{marker}\n"
      log << "Elemento Padre: #{data[i][:nodo_padre]}\n"
      log << "Elemento: #{data[i][:nodo_hijo]}\n"
      log << "Valor esperado: #{data[i][:res_experado]}\n"
      log << "Valor obtenido: #{data[i][:res_obtenido].nil? ? 'NULL' : ( data[i][:res_obtenido].to_s.empty? ? '""' : data[i][:res_obtenido] )}\n"
      log << "Mensaje: #{data[i][:mensaje]}\n"
      log << "Estatus: #{data[i][:result]}\n"
      log << "#{marker}\n"
    end

    #save_evidence_file(log.string, "#{@ws}_log_fail_response_body", 'log')

    return {
        :log => { :data => log.string, :file_name => "#{@ws}_log_fail_response_body", :file_extencion => 'log' }
    }

  end

  #Escribir response a directorio de evidencias
  def write_request_response_files
    ws = get_ws(@ws)
    ws_tipo = ws[:ws_type].to_s.downcase.strip
    file_extencion = ws_tipo == 'soap' ? 'xml' : 'json'
    req_body = get_ws_parameters(ws, @data_ws_filters)
    req_headers = get_ws_headers(ws[:headers])
    response_is_file = ws[:expected_response_body_is_file].to_s.strip
    response_expected = ws[:expected_response_body].to_s.strip
    driver_http = ws[:driver].to_s.downcase.strip
    resp_body = nil
    resp_headers = nil

    #obtener response body, dependiendo el driver http
    resp_body = @r.read_body
    resp_headers = @r.each_header.to_h

    #se obtiene el response esperado del archivo csv
    if response_is_file.to_s.downcase.strip == '1'
      file_r =  File.join(File.dirname(__FILE__), "../../../../venture/config/csv_data/#{response_expected.to_s.downcase.strip}")
      file_data_r = File.read(file_r.to_s)
      response_expected = file_data_r
    end

    if ws_tipo.to_s.include? 'soap'
      request_params = req_body.to_s.empty? ? nil : Nokogiri::XML( req_body ).to_xml
      r_esperado = "#{Nokogiri::XML( response_expected ).to_xml}"
      r_actual = "Response Headers: \n#{resp_headers} \n\nResponse Body: \n#{Nokogiri::XML( resp_body ).to_xml}"
    end
    if ws_tipo.to_s.include? 'rest'
      request_params = req_body.to_s.empty? ? nil : JSON.parse( req_body )
      r_esperado = response_expected.to_s.empty? ? '' : JSON.parse( response_expected )
      #r_actual = JSON.parse( @r.raw_body )
      r_actual = "Response Headers: \n#{resp_headers} \n\nResponse Body: \n#{resp_body}"
    end

    #construir request
    request = "Web Service: #{@ws} \nMethod: #{ws[:method]} \nURL: #{Config.url_visit}/#{ws[:url]} \nRequest Headers: \n#{req_headers.to_s} \nRequest Body: \n#{req_body}"

    return {
        :request => { :data => request, :file_name => "#{@ws}_request", :file_extencion => 'txt' },
        :response_esperado => { :data => r_esperado, :file_name => "#{@ws}_response_esperado", :file_extencion => file_extencion },
        :response_actual => { :data => r_actual, :file_name => "#{@ws}_response", :file_extencion => file_extencion }
    }

  end


  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################
  # METODOS ASOCIADOS A OBTENRE O.R. DATA
  ##########################################################################################################
  ##########################################################################################################
  ##########################################################################################################

  #METODO QUE OBTIENE TODA LA DATA DEL ARCHIVO O.R. (OBJECT REPOSITORY)
  def get_or_dt
    return get_ws @ws
  end


  #METODO QUE OBTIENE EL NOMBRE DEL ARCHIVO DATA POOL ASOCIADO EN EL O.R. (OBJECT REPOSITORY)
  def get_ws_dt_file

    #variable que oontiene el nombre del data pool que alimenta los webelement del O.R.
    dt = get_ws @ws
    dt_file = nil

    dt_file = dt[:data_file].to_s.downcase.strip

    return dt_file

  end


end
