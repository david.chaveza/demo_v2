# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WebPages
  module UIElements
    class ModalPreCarritoUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @or_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_modal_pre_carrito'

        #UIELEMENTS
        @lbl_add_prod_successfull = get_or_data('lbl_add_prod_successfull', @or_file, @path_page)
        @btn_proceed_to_ckeckout = get_or_data('btn_proceed_to_ckeckout', @or_file, @path_page)

      end


    end
  end
end

