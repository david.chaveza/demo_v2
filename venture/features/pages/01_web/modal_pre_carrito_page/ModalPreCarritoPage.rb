# encoding: utf-8
require 'capybara/dsl'
require 'capybara/driver/base'
require 'rspec'
require 'rspec/expectations'
require_relative 'ModalPreCarritoUIElements'
require_relative '../../../../../helpers/uielement_actions/or_actions_ui'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIElementsAsserts'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIBrowserActions'


module WebPages
  class ModalPreCarritoPage < WebPages::UIElements::ModalPreCarritoUIElements

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################
    def select_btn_proceed_to_checout
      or_exec_uielement(@btn_proceed_to_ckeckout)
    rescue Exception => e
      raise("Error => Al presionar el boton: 'Proceed to checkout'.
           \nException =>#{e.message}"
      )
    end

    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################


    def assert_add_prod_successfull?
      #ASSERT USANDO EL METODO 'assert_for_uielement_exist'
      @asserts.assert_for_uielement_exist(@lbl_add_prod_successfull[:selector].to_s.strip.to_sym,
                                          @lbl_add_prod_successfull[:locator].to_s.strip,
                                          10,
                                          "No se encontro la etiqueta de exito en el modal pre carrito"

      )
    end



  end

end

