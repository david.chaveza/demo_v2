# encoding: utf-8
require 'capybara/dsl'
require 'capybara/driver/base'
require 'rspec'
require 'rspec/expectations'
require_relative 'CarritoUIElements'
require_relative '../../../../../helpers/uielement_actions/or_actions_ui'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIElementsAsserts'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIBrowserActions'


module WebPages
  class CarritoPage < WebPages::UIElements::CarritoUIElements

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################
    def obtener_cantidad_inicial_producto_uno
      el = or_get_uielement(@txt_numero_productos_uno)
      #js_focus_to_element
      execute_script(js_scroll_to_element, el.uielement.native)
      cantidad_inicial = el.uielement['value']
      puts ("el numero de productos es #{cantidad_inicial}")
      return cantidad_inicial
    end

    def incrementar_cantidad_inicial_producto_uno(cantidad_inicial)
      el = or_exec_uielement(@txt_numero_productos_uno, '2')
      cantidad_final = el.uielement['value']
      unless cantidad_inicial.to_i < cantidad_final.to_i
        raise("La cantidad inicial es mayor o igual que la final 'No se incremento la cantidad de producto'")
      end
    end

    def eliminar_producto_uno_carrito
      or_exec_uielement(@btn_eliminar_prod_uno)
    end

    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################


    def assert_carrito_page?
      #ASSERT USANDO EL METODO 'assert_for_uielement_exist'
      @asserts.assert_for_uielement_exist(@lbl_shopping_cart_summary[:selector].to_s.strip.to_sym,
                                          @lbl_shopping_cart_summary[:locator].to_s.strip,
                                          10,
                                          "No se encontro la etiqueta de shopping cart summary"

      )
    end

    def assert_carrito_vacio?
      #ASSERT USANDO EL METODO 'assert_for_uielement_exist'
      @asserts.assert_for_uielement_exist(@lbl_carrito_vacio[:selector].to_s.strip.to_sym,
                                          @lbl_carrito_vacio[:locator].to_s.strip,
                                          10,
                                          "No se encontro la alerta de carrito vacio"

      )
    end



  end

end

