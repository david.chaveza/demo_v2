# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WebPages
  module UIElements
    class CarritoUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @or_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_carrito'

        #UIELEMENTS
        @lbl_shopping_cart_summary = get_or_data('lbl_shopping_cart_summary', @or_file, @path_page)
        @txt_numero_productos_uno = get_or_data('txt_numero_productos_uno', @or_file, @path_page)
        @btn_eliminar_prod_uno = get_or_data('btn_eliminar_prod_uno', @or_file, @path_page)
        @lbl_carrito_vacio = get_or_data('lbl_carrito_vacio', @or_file, @path_page)

      end


    end
  end
end

