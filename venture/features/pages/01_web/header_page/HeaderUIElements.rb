# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WebPages
  module UIElements
    class HeaderUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @or_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_header'
        @dt_productos = get_csv_data('dt_producto')

        #UIELEMENTS
        @txt_buscar = get_or_data('txt_buscar', @or_file, @path_page)
        @btn_buscar = get_or_data('btn_buscar', @or_file, @path_page)
        @lk_sign_in = get_or_data('lk_sign_in', @or_file, @path_page)
        @lk_todos_links = get_or_data('lk_todos_links', @or_file, @path_page)

      end


    end
  end
end

