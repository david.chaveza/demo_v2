# encoding: utf-8
require 'capybara/dsl'
require 'capybara/driver/base'
require 'rspec'
require 'rspec/expectations'
require_relative 'HeaderUIElements'
require_relative '../../../../../helpers/uielement_actions/or_actions_ui'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIElementsAsserts'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIBrowserActions'


module WebPages
  class HeaderPage < WebPages::UIElements::HeaderUIElements

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################

    def capturar_busqueda(tipo_producto)
      data = get_data_by_filters("tipo_producto = #{tipo_producto.to_s.strip}", @dt_productos)[1]
      puts "CSV DATA => dt_row => #{data}"
      or_exec_uielement(@txt_buscar, data[:producto])

    rescue Exception => e
      raise("Error => Al realizar la busqueda de producto: '#{data}'.
           \nException =>#{e.message}"
      )
    end

    def capturar_busqueda_random
      data = @dt_productos[rand(1..(@dt_productos.size - 1))]
      puts "CSV DATA => dt_row => #{data}"
      or_exec_uielement(@txt_buscar, data[:producto])

    rescue Exception => e
      raise("Error => Al realizar la busqueda de producto: '#{data}'.
           \nException =>#{e.message}"
      )
    end


    def click_boton_buscar
      or_exec_uielement(@btn_buscar)
    rescue Exception => e
      raise("Error => Al presionar el boton: 'Buscar (Lupita)'.
           \nException =>#{e.message}"
      )
    end

    def click_sign_in
      #DUMMY LOCATOR OVERRIDE - REMPLAZANDO 2 REGEX A LA VEZ
      dt_override_loc = ['a', 'Sign in']
      or_exec_uielement(@lk_sign_in, nil, dt_override_loc)
    rescue Exception => e
      raise("Error => Al seleccionar: 'Sign In'.
           \nException =>#{e.message}"
      )
    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_sign_in_exist?
      #DUMMY LOCATOR OVERRIDE - REMPLAZANDO 2 REGEX A LA VEZ
      dt_override_loc = ['a', 'Sign in']
      el = or_get_uielement(@lk_sign_in, nil, dt_override_loc)

      #LA ASSERTION ES VALIDANDO el 'RESULTADO' DE SI SE ENCONTRO EL OBJETO Y RETORNA SU ATRIBUTO 'OUTERHTML'
      if el.result
        return el.uielement['outerHTML']
      end
    end

    def assert_text_busqueda_exist?
      #ASSERT USANDO EL METODO 'assert_for_uielement_exist'
      @asserts.assert_for_uielement_exist(@txt_buscar[:selector].to_s.strip.to_sym,
                                          @txt_buscar[:locator].to_s.strip,
                                          10,

      )
    end



  end

end

