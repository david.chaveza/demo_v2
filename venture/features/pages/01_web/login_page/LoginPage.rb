# encoding: utf-8
require 'capybara/dsl'
require 'capybara/driver/base'
require 'rspec'
require 'rspec/expectations'
require_relative 'LoginUIElements'
require_relative '../../../../../helpers/uielement_actions/or_actions_ui'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIElementsAsserts'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIBrowserActions'

module WebPages
  class LoginPage < WebPages::UIElements::LoginUIElements

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################

    def llenar_formulario(email, password)
      or_exec_uielement(@txt_email, email)
      or_exec_uielement(@txt_password, password)

    rescue Exception => e
      raise("Error => no se pudo llenar el formulario 'login'.
           \nException =>#{e.message}"
      )
    end

    def llenar_formulario_data_table(tipo_usuario)
      #OBTENER DATOS DE CSV
      data = get_data_by_filters("tipo_usuario = #{tipo_usuario.to_s.strip}", @dt_login)[1]
      puts "CSV DATA => dt_row => #{data}"

      or_exec_uielement(@txt_email, data[:usuario])
      or_exec_uielement(@txt_password, data[:password])

    rescue Exception => e
      raise("Error => no se pudo llenar el formulario 'login'.
           \nException =>#{e.message}"
      )
    end

    def click_boton_login
      or_exec_uielement(@btn_sign_in)

    rescue Exception => e
      raise("Error => al presionar en el boton 'SIGN IN'.
           \nException =>#{e.message}"
      )
    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_login_page_visible?
      #ASSERT EVALUANDO QUE EXISTA 1 'XPATH', USANDO 'RSPEC'
      #page.should have_xpath(@lbl_titulo_pagina[:locator])
      or_get_uielement(@lbl_titulo_pagina)
    end

    def assert_lbl_login_incorrecto
      #ASSERT SOLO INVOCANDO 'or_get_uielement',
      #Esto por default evaluara 'SI EXISTE' el 'UIELEMENT' en el 'TIMEOUT' definido en 'OR CSV'
      dt_override_locator = [@dt_yml['lbl_msj_login_incorrecto']]
      lbl_msj_login_incorrecto = or_get_uielement(@lbl_msj_login_incorrecto, nil, dt_override_locator)
      lbl_msj_login_incorrecto.to_s
    end

    def assert_lbl_password_incorrecto
      #ASSERT SOLO INVOCANDO 'or_get_uielement',
      #Y LUEGO SI EL 'RESULT' DEL OBJETO ES 'FALSE' SE MANDA UN 'RAISE'
      dt_override_locator = [@dt_yml['lbl_msj_password_incorrecto']]
      lbl_password_incorrecto = or_get_uielement(@lbl_msj_login_incorrecto, nil, dt_override_locator)
      unless lbl_password_incorrecto.result
        raise "ERROR NO SE ENCUENTRA EL MENSAJE 'PASSWORD INCORRECTO'"
      end
    end


  end

end

