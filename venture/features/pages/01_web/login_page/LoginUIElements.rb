# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WebPages
  module UIElements
    class LoginUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @or_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_login'
        #DATA CSV
        @dt_login = get_csv_data('dt_login')
        #DATA YML
        @dt_yml = get_yml_data('dt_yml_login', @path_page)

        #UIELEMENTS
        @url_pagina = get_or_data('url_pagina', @or_file, @path_page)
        @lbl_titulo_pagina = get_or_data('lbl_titulo_pagina', @or_file, @path_page)
        @txt_email = get_or_data('txt_email', @or_file, @path_page)
        @txt_password = get_or_data('txt_password', @or_file, @path_page)
        @btn_sign_in = get_or_data('btn_sign_in', @or_file, @path_page)
        @lbl_msj_login_incorrecto = get_or_data('lbl_msj_login_incorrecto', @or_file, @path_page)
        @lbl_msj_password_incorrecto = get_or_data('lbl_msj_password_incorrecto', @or_file, @path_page)
      end


    end
  end
end

