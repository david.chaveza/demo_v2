# encoding: utf-8
#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  clase metodos genericos adicionales  #
# para el negocio proyecto                          #
#####################################################
require 'capybara/dsl'
require 'capybara/driver/base'

require 'fileutils'

module WebPages
  class GenericPage

    include Capybara::DSL

    #############################################################################
    # WEB ELEMENTS
    #############################################################################
    def initialize
      @url_base = Config.url_visit
    end

    #############################################################################
    # ACCIONES
    #############################################################################

    def abrir_app
      visit @url_base
    end



    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

  end
end
