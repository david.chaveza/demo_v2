# encoding: utf-8
#require 'capybara/dsl'
#require 'capybara/driver/base'
require_relative '../../../../../helpers/uielement_actions/or_data'
require_relative '../../../../../helpers/data_driven/csv_data'
require_relative '../../../../../helpers/data_driven/yml_data'

module WebPages
  module UIElements
    class PlpUIElements

      #############################################################################
      # UI ELEMENTS
      #############################################################################
      def initialize
        @asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @or_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @path_page = File.expand_path(File.dirname(__FILE__))
        @or_file = 'or_plp'

        #UIELEMENTS
        @lbl_search = get_or_data('lbl_search', @or_file, @path_page)
        @img_prod_uno_plp = get_or_data('img_prod_uno_plp', @or_file, @path_page)
        @btn_add_cart_prod_uno = get_or_data('btn_add_cart_prod_uno', @or_file, @path_page)
        #@btn_buscar = get_or_data('btn_buscar', @or_file, @path_page)
        #@lk_sign_in = get_or_data('lk_sign_in', @or_file, @path_page)
        #@lk_todos_links = get_or_data('lk_todos_links', @or_file, @path_page)

      end


    end
  end
end

