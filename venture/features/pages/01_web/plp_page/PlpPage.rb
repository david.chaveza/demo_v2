# encoding: utf-8
require 'capybara/dsl'
require 'capybara/driver/base'
require 'rspec'
require 'rspec/expectations'
require_relative 'PlpUIElements'
require_relative '../../../../../helpers/uielement_actions/or_actions_ui'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIElementsAsserts'
require_relative '../../../../../helpers/uielement_actions/or_capybara/UIBrowserActions'


module WebPages
  class PlpPage < WebPages::UIElements::PlpUIElements

    include Capybara::DSL
    include RSpec::Matchers

    #############################################################################
    # ACCIONES
    #############################################################################

    def capturar_busqueda_plp(tipo_producto)
      busqueda = get_data_by_filters("tipo_producto = #{tipo_producto.to_s.strip}", @dt_productos)[0]
      puts "CSV DATA => dt_row => #{busqueda}"

      or_exec_uielement(@txt_buscar, data[:producto])
    rescue Exception => e
      raise("Error => Al realizar la busqueda de producto.
           \nException =>#{e.message}"
      )
    end

    def select_primer_prod_plp
      or_exec_uielement(@img_prod_uno_plp)
      or_exec_uielement(@btn_add_cart_prod_uno)
    rescue Exception => e
      raise("Error => Seleccionar el boton agregar a carrito en el producto uno del plp.
           \nException =>#{e.message}"
      )


    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_plp?
      #ASSERT USANDO EL METODO 'assert_for_uielement_exist'
      @asserts.assert_for_uielement_exist(@lbl_search[:selector].to_s.strip.to_sym,
                                          @lbl_search[:locator].to_s.strip,
                                          10,

      )
    end



  end

end

