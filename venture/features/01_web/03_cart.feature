# language: es
# encoding: utf-8
Característica: CARRITO
  - Se debe poder agregar un producto
  - Se debe poder actualizar la cantidad un producto
  - Se debe poder eliminar un producto

  @regresion @web @critical @TC_14426
  Escenario: CARRITO_01 - AGREGAR PRODUCTO
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Home'
    Y Busco 1 producto aleatorio desde el datapool csv
    Entonces Se muestra la pagina de 'PLP'
    Y Selecciono un producto
    #Entonces Se muestra la pagina de 'Detalle del Producto'
    #Y Agrego el producto al carrito desde la pagina 'Detalle del Producto'
    Entonces Se muestra el modal de productos agregados al carrito
    Cuando Selecciono la opción 'Proceed to checkout'
    Entonces Se muestra la pagina de 'Carrito'

  @regresion @web @CARRITO_02
  Escenario: CARRITO_02 - ACTUALIZAR CANTIDAD DE ITEMS EN CARRITO
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Home'
    Y Busco 1 producto aleatorio desde el datapool csv
    Entonces Se muestra la pagina de 'PLP'
    Y Selecciono un producto
    #Entonces Se muestra la pagina de 'Detalle del Producto'
    #Y Agrego el producto al carrito desde la pagina 'Detalle del Producto'
    Entonces Se muestra el modal de productos agregados al carrito
    Cuando Selecciono la opción 'Proceed to checkout'
    Entonces Se muestra la pagina de 'Carrito'
    Pero Actualizo la candidad de items

  @regresion @web @CARRITO_03
  Escenario: CARRITO_03 - ACTUALIZAR CANTIDAD DE ITEMS EN CARRITO
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Home'
    Y Busco 1 producto aleatorio desde el datapool csv
    Entonces Se muestra la pagina de 'PLP'
    Y Selecciono un producto
    #Entonces Se muestra la pagina de 'Detalle del Producto'
    #Y Agrego el producto al carrito desde la pagina 'Detalle del Producto'
    Entonces Se muestra el modal de productos agregados al carrito
    Cuando Selecciono la opción 'Proceed to checkout'
    Entonces Se muestra la pagina de 'Carrito'
    Pero Elimino el producto del carrito
    Entonces Se muestra el mensaje 'Tu carrito de compras esta vacio'
