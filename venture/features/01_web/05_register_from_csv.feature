# language: es
# encoding: utf-8
Característica: RESGISTRO
  - Se debe poder registrar 1 nueva cuenta

  @regresion @REGISTRO_01
  Escenario: REGISTRO_01 - REGISTRAR
    Dado Abro el aplicativo sistema
    Y Obtengo los datos de prueba del archivo "dt_registro" data pool y filtro por "all"
    Entonces Se muestra la pagina de 'Home'
    Y Seleccionar la opcion 'Sign in'
    Entonces Creo las cuentas
    Y valido el resultado de crear las cuentas
