Feature: REST API
  - Validar web services

  @regression @ws
  Scenario: WS CIUDAD (TC-001)
    Given I am a client
    When  I test at web service "ciudad"
    Then I get response code success
    Then  I get response body success
    Then test end

  @regression @ws
  Scenario: WS REGISTRO (TC-001)
    Given I am a client
    When  I test at web service "registro"
    Then I get response code success
    Then  I get response body success
    Then test end

  @regression @ws
  Scenario: WS GMAPS (TC-001)
    Given I am a client
    When  I test at web service "google_maps"
    Then I get response code success
    Then test end

