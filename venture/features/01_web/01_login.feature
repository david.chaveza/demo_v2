# language: es
# encoding: utf-8
Característica: LOGIN

  @regresion @web @low @TC_33959
  Escenario: LOGIN_TC00 - TEST DUMMY FAIL
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login como "usuario valido"

  @regresion @web @critical @TC_33960
  Escenario: LOGIN_TC01 - LOGIN EXITOSO
    Dado Abro el aplicativo sistema
    Cuando Seleccionar la opcion 'Sign in'
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login como "usuario valido"
    Entonces Se muestra la pagina de 'Mi Cuenta'
    
  @regresion @web @critical @TC_ABC
  Escenario: LOGIN_TC01 - LOGIN EXITOSO - CSV DATA
    Dado Abro el aplicativo sistema
    #Y Obtengo los datos de prueba del archivo "csv_data/dt_login" data pool y filtro por "tipo_usuario = usuario valido"
    Y Obtengo los datos de prueba del archivo "dt_login" data pool y filtro por "tipo_usuario = usuario valido"
    Cuando Seleccionar la opcion 'Sign in'
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login en la app web
    Entonces Se muestra la pagina de 'Mi Cuenta'

  @regresion @web @medium @TC_33961
  Escenario: LOGIN_TC02 - LOGIN USUARIO INCORRECTO
    Dado Abro el aplicativo sistema
    Cuando Seleccionar la opcion 'Sign in'
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login como "usuario incorrecto"
    Entonces Se muestra el mensaje 'Login incorrecto'

  @regresion @web @medium @TC_33962
  Escenario: LOGIN_TC03 - LOGIN PASSWORD MENOR A 5 CARACTERES
    Dado Abro el aplicativo sistema
    Cuando Seleccionar la opcion 'Sign in'
    Entonces Se muestra la pagina de 'Login'
    Cuando Hago login como "password incorrecto"
    Entonces Se muestra el mensaje 'password incorrecto'
