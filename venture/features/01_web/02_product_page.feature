# language: es
# encoding: utf-8
Característica: PLP - CATALOGO DE PRODUCTO
  - Se debe poder realizar una busqueda
  - Se debe poder visualizar la pagina Catalogo del Producto

  @regresion @web @PLP_01
  Escenario: PLP_02 - BUSCAR PRODUCTOS
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Home'
    Y Busco 1 producto aleatorio desde el datapool csv "ropa"
    Entonces Se muestra la pagina de 'PLP'
