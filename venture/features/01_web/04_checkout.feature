# language: es
# encoding: utf-8
Característica: CHECKOUT
  - Se debe poder realizar una compra

  @regresion @CHECKOUT_01
  Escenario: CHECKOUT_01 - AGREGAR PRODUCTO
    Dado Abro el aplicativo sistema
    Entonces Se muestra la pagina de 'Home'
    Y Busco 1 producto aleatorio desde el datapool csv
    Entonces Se muestra la pagina de 'PLP'
    Y Selecciono un producto
    Entonces Se muestra la pagina de 'Detalle del Producto'
    Y Agrego el producto al carrito desde la pagina 'Detalle del Producto'
    Entonces Se muestra el modal de productos agregados al carrito
    Cuando Selecciono la opción 'Proceed to checkout'
    Entonces Se muestra la pagina de 'Carrito'
    Y Selecciono la opción 'Proceed to checkout'
    Y Confirmo la direccion de envio
    Y Selecciono la opción 'Proceed to checkout'
    Y Acepto terminos y condiciones
    Y Selecciono la opción 'Proceed to checkout'
    Y Selecciono el metodo de pago "wire"
    Y Confirmo la orden
    Entonces Se muestra la confirmacion de la orden
