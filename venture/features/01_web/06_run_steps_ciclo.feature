# language: es
# encoding: utf-8
Característica: CICLO => STEPS
  - Se ejecutan los steps que pasan como parametro string N veces, Donde N=definido en el step definition

  @regresion @STEPS_CICLO_01
  Escenario: STEPS_CICLO_01
    Dado Abro el aplicativo sistema
    Cuando Ejecuto los siguientes pasos para cada Fila del CSV "dt_registro" y filtro por "all":
      """
      Abro el aplicativo sistema
      Se muestra la pagina de 'Home'
      Seleccionar la opcion 'Sign in'
      Lleno la informacion de la cuenta
      """
