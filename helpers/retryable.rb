# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper para realizar REINTENTOS de codigo 'RETRY'
#####################################################

module Retryable

	# Options:
	# * :tries - Number of tries to perform. Defaults to 1. If you want to retry once you must set tries to 2.
	# * :on - The Exception on which a retry will be performed. Defaults to Exception, which retries on any Exception.
	# * :log - The log level to log the exception. Defaults to nil.
	#
	# If you work with something like ActiveRecord#find_or_create_by_foo, remember to put that call in a uncached { } block. That
	# forces subsequent finds to hit the database again.
	#
	# Example
	# =======
	#   retryable(:tries => 2, :on => OpenURI::HTTPError) do
	#     # your code here
	#   end
	#
	def retryable(options = {}, &block)
		opts = { :tries => 1, :on => Exception }.merge(options)
		retry_exception, retries = opts[:on], opts[:tries]
		begin
			return yield
		rescue retry_exception => e
			sleep 1
			puts "RETRYABLE => #{retries} \n#{e.message}"
			#logger.send(opts[:log], e.message) if opts[:log]
			retry if (retries -= 1) > 0
		end
		yield
	end

end
