# encoding: utf-8
require_relative 'GetAppsWS'
require_relative '../eval_data'
require 'jsonpath'

def wait_for_last_app_by_name(app_name, os, timeout = nil)
  ws = Kobiton::GetAppsWS.new
  result = false
  app_id = String.new
  json_expr = "$.apps[?(@.name == '#{app_name.to_s.strip}' && @.os == '#{os.to_s.upcase.strip}')].id"

  #si no se manda timeout por default se envia 5
  to = timeout.nil? ? 300 : timeout.to_s.to_i
  start = Time.new

  while (start.to_i + to.to_i) > Time.new.to_i
    begin
      response = ws.request
      ws.assert_response_code
      app_id = get_json_path_value(response.read_body, json_expr)
      result = true unless app_id.to_s.empty?
      break unless app_id.to_s.empty?
      puts "Esperando por APP con NOMBRE: '#{app_name}'"
    rescue Exception => e
      puts "Error. Al esperar por APP con NOMBRE: '#{app_name}'. Exception => #{e.message}"
    end
  end

  raise "El APP: '#{app_name}' 'NO EXISTE'. TimeOut: '#{timeout}'" unless  result
  appid = "kobiton-store:#{app_id.sort[app_id.size - 1].to_s}"
  puts "El APP: '#{app_name}' 'EXISTE' CON ID: '#{appid}'" if result

  return appid
end

