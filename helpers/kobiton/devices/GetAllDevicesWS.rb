# encoding: utf-8
require "uri"
require 'net/http'
require 'openssl'
require 'json'
require_relative '../../data_driven/yml_data'

module Kobiton

  class GetAllDevicesWS

    #############################################################################
    # WEBSERVICE DATA
    #############################################################################
    def initialize
      @current_path = File.expand_path(File.dirname(__FILE__))
      @kb_config = get_yml_data('../config.yml', @current_path)
      @url_servicio = "#{@kb_config['url_kobiton']}/devices"
      @auth_user = @kb_config['auth_user']
      @auth_password = @kb_config['auth_password']
      @request_ssl = true
      @request_timeout = 120
      @request_headers = [
          {:header => 'User-Agent', :valor => 'testing/webservices'}
      ]
      @response = nil
      @response_codigo_esperado = 200
    end


    #############################################################################
    # ACCIONES
    #############################################################################

    #Metodo que ejecuta el request
    # :parametrizar_body_request true o false (true si se desea parametrizar false si no se desea parametrizar)
    def request
      # 1 - URI, SSL, TimeOut
      url = URI(@url_servicio)
      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = @request_ssl
      https.read_timeout = @request_timeout

      # 2 - Métodos HTTP
      request = nil
      request = Net::HTTP::Get.new(url)
      request.basic_auth(@auth_user, @auth_password)

      # 3 - Agregar Headers
      for i in 0..(@request_headers.size - 1)
        request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
      end

      # 5 - Obtener HTTP Response
      @response = https.request(request)

      return @response

    rescue Exception => e
      raise("Error al ejecutar el Request a la URL => #{@url_servicio}. \nException => #{e.message}")

    end

    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_response_code
      unless @response.code.to_s.to_i == @response_codigo_esperado
        raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
      end
    end



  end

end


