# encoding: utf-8
require_relative 'GetAllDevicesWS'
require_relative '../eval_data'
require 'jsonpath'

def wait_for_activated_state_by_udid(device_udid, timeout = nil)
  get_all_devices = Kobiton::GetAllDevicesWS.new
  result = false
  json_expr = "$..[?(@.udid == '#{device_udid.to_s.strip}')].state"

  #si no se manda timeout por default se envia 5
  to = timeout.nil? ? 300 : timeout.to_s.to_i
  start = Time.new

  while (start.to_i + to.to_i) > Time.new.to_i
    response = get_all_devices.request
    get_all_devices.assert_response_code
    device_state = get_json_path_value(response.read_body, json_expr)
    result = true if device_state.to_s.downcase.strip.include?('activated')
    break if device_state.to_s.downcase.strip.include?('activated')
    puts "Esperando por dispositivo 'DISPONIBLE' con UDID: '#{device_udid}'"
  end

  raise "El dispositivo: '#{device_udid}' no esta 'DISPONIBLE'. TimeOut: '#{timeout}'" unless  result
  puts "El dispositivo: '#{device_udid}' esta 'DISPONIBLE'." if result
end

