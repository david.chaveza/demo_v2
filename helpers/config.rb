# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper de configuracion general
#   y recuperar variables de entorno que definen la ejecución
#####################################################

require 'yaml'
require 'tmpdir'
require_relative 'data_driven/csv_data'


class Config

  #********************************************************************************************************
  # VARIABLES ENTORNO
  #********************************************************************************************************
  #Driver de la ejecucion
  #   - CAPYBARA WEB/WAM(EMULADO): chrome_capybara, chrome_headless_capybara, firefox_capybara, ie_capybara, etc
  #   - WEBDRIVER WEB/WAM(EMULADO): chrome_webdriver, chrome_headless_webdriver, firefox_webdriver, ie_webdriver, etc
  #   - MOBILE: appium,calabash_android
  #   - APPS DESKTOP: appium, winium
  #   - WEB SERVICES: ws
  @driver_name = ENV['DRIVER'].nil? ? 'chrome_capybara' : ENV['DRIVER'].to_s.strip.downcase

  #PATH DONDE SE VAN A LOCALIZAR LOS DRIVE DE SELENIUM
  @selenium_drivers = ENV['SELENIUM_DRIVERS'].nil? ? Dir.tmpdir().to_s : ENV['SELENIUM_DRIVERS'].to_s.strip

  #ENVIRONMENT LIVE / STAGE / OTRO. EL valor debe hacer match con
  #   la columna 'env_name' del archivo '../venture/config/csv_data/00_config/dt_environment.csv'
  @environment = ENV['ENVIRONMENT'].nil? ?  'stage' : ENV['ENVIRONMENT'].to_s.strip.downcase

  #Nombre dispositivo: debe hacer match con
  #   la columna 'device_name' del archivo '../venture/config/csv_data/00_config/dt_devices.csv'
  @device = ENV['DEVICE_NAME'].nil? ? 'web_desktop' : ENV['DEVICE_NAME'].to_s.strip

  #Fecha de ejecucion este valor podria ser usado para crear la carpeta que contenga reporte y evidencia.
  @exec_date = ENV['EXEC_DATE'].nil? ? Time.now.strftime("%Y%m%d") : ENV['EXEC_DATE'].to_s.strip
  @exec_time = ENV['EXEC_TIME'].nil? ? Time.now.strftime("%H%M") : ENV['EXEC_TIME'].to_s.strip
  #Tipo de ejecucion: 'regular'/'rerun'
  @exec_type = ENV['EXEC_TYPE'].nil? ? 'single' : ENV['EXEC_TYPE'].to_s.strip

  #Obtiene el directorio de resultados de la ejecucion en curso
  @path_results = ENV['PATH_RESULTS'].nil? ? "#{@exec_type}" : "#{ENV['PATH_RESULTS']}/#{@exec_type}"

  #Directorio donde se va tomar datos externos al proyecto GIT DE AUTOMATION.
  #   Ej: data-pools, imagenes, salida para reportes y logs, apps (.apk, .ipa, accesos directos desktop), etc
  @path_external_data = ENV['PATH_EXTERNAL_DATA'].nil? ? nil
                          : ( ENV['PATH_EXTERNAL_DATA'].to_s.strip[ENV['PATH_EXTERNAL_DATA'].to_s.strip.size - 1] == '/' ?
                                ENV['PATH_EXTERNAL_DATA'].to_s.strip[0..(ENV['PATH_EXTERNAL_DATA'].to_s.strip.size - 2)]
                                : ENV['PATH_EXTERNAL_DATA'].to_s.strip
                          )


  #********************************************************************************************************
  # VARIABLES CUCUMBER YML
  #********************************************************************************************************
  #Obtener YML
  @config = YAML.load(File.read("../venture/config/cucumber.yml"))
  #Timeout por default para Capybara.default_max_wait_time = $to
  @to = @config["to"]
  #nombre del proyecto, para reporte
  @report_proyect_name = @config["report_proyect_name"]
  #logo del proyecto, para reporte
  @report_logo = @config["report_logo"]
  @report_logo_cliente = @config["report_logo_cliente"]


  #********************************************************************************************************
  # VARIABLES TEST
  #********************************************************************************************************
  @driver = nil
  @driver_winium = nil
  @driver_appium = nil
  @driver_winappdriver = nil
  @current_feature = nil
  @current_scenario = nil
  @dt_data = Array.new
  @dt_row = nil
  @vars_out = Array.new
  @current_evidence_file = nil
  @current_screenshot_path = nil



  #********************************************************************************************************
  # CLASS SELF
  #********************************************************************************************************
  class << self
    attr_accessor :report_proyect_name, :driver, :exec_time, :report_logo, :report_logo_cliente,
                  :current_scenario, :dt_row, :exec_date,
                  :path_results, :config, :device, :dt_data, :current_feature, :exec_type, :driver_name, :environment, :to, :app,
                  :driver_winium, :driver_appium, :driver_winappdriver, :vars_out, :current_evidence_file,
                  :selenium_drivers, :path_external_data, :current_screenshot_path


    #********************************************************************************************************
    # METODOS - AMBIENTE / DEVICE
    #********************************************************************************************************

    # => RETURN URL Sistema
    def url_visit
      puts "url => #{get_environment_data[:url].to_s.strip}"
      return get_environment_data[:url].to_s.strip
    end

    # Método para obtener datos del ambiente
    #Obtiene los datos del ENVIRONMENT desde el CSV 'venture/config/csv_data/environment.csv'
    # @return
    #   Hash con datos del ambiente
    def get_environment_data
      get_env = Hash.new
      environment = get_csv_data('/00_config/dt_environment')
      get_env = get_data_by_filters("env_name = #{@environment.to_s.downcase.strip}", environment )[1]

      if get_env.nil? or get_env.to_s.empty?
        raise "ENVIRONMENT INVALIDO => NO SE ENCONTRO EL AMBIENTE: '#{@environment}'"
      end

      return get_env
    end

    # Método para obtener datos del dispositivo
    #Obtiene los datos del ENVIRONMENT desde el CSV 'venture/config/csv_data/environment.csv'
    # @return
    #   Hash con datos del dispocitivo
    def get_data_device
      get_device = Hash.new
      devices = get_csv_data('/00_config/dt_devices')
      get_device = get_data_by_filters("device_name = #{@device.to_s.downcase.strip}", devices )[1]

      if get_device.nil? or get_device.to_s.empty?
        raise "DISPOSITIVO INVALIDO => NO SE ENCONTRO EL DISPOSITIVO: '#{@device}'"
      end

      return get_device
    end

    #********************************************************************************************************
    #METODOS - VARIABLES OUT
    # 		Permite obtener y crear variables que se podran usar a futuro en otro test
    #********************************************************************************************************

    #Método para obtener el valor de una variable
    def get_vars_out
      return @vars_out
    end

    #Método para obtener el valor de una variable
    # @params
    # * :var_name String nombre de la variable
    def get_var_out(var_name)
      value = nil
      for i in 0..(@vars_out.size - 1)
        if @vars_out[i][:var_name].to_s.downcase.strip == var_name.to_s.downcase.strip
          value = @vars_out[i][:value]
          break
        end
      end
      return value
    rescue Exception => e
      raise "get_var_out => No se pudo obtener la variable '#{var_name}' \nException => #{e.message}"
    end

    #Método para crear una variable
    # @params
    # * :var_name String nombre de la variable
    # * :value String valor de la variable
    def set_var_out(var_name, value=nil)
      var_exist = get_var_out(var_name)
      if var_exist
        override_var_out(var_name, value)
      else
        var = { :var_name => var_name.to_s.strip, :value => value}
        @vars_out.push(var)
      end
    rescue Exception => e
      raise "set_var_out => No se pudo crear la variable '#{var_name}' con el valor '#{value}' \nException => #{e.message}"
    end

    #Método para sobreescribir una variable ya existente
    # @params
    # * :var_name String nombre de la variable
    def override_var_out(var_name, value)
      for i in 0..(@vars_out.size - 1)
        if @vars_out[i][:var_name].to_s.downcase.strip == var_name.to_s.downcase.strip
          @vars_out[i][:value] = value
          break
        end
      end
    rescue Exception => e
      raise "get_var_out => No se pudo sobreescribir la variable '#{var_name}' \nException => #{e.message}"
    end


  end

end














