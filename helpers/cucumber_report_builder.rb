require 'report_builder'
require_relative 'config'

@path_results = File.join(File.dirname(__FILE__), "../venture/#{ARGV[0]}")
@file_single = File.join(File.dirname(__FILE__), "../venture/#{ARGV[0]}/single/results.json")
@file_rerun = File.join(File.dirname(__FILE__), "../venture/#{ARGV[0]}/rerun/results_rerun.json")
#@path_results  = File.join(File.dirname(__FILE__), "../venture/evidence/200526/1736/live_desktop_chrome")

def report_builder
	puts "PATH FILES"
	puts "#{@file_single}"
	puts "#{@file_rerun}"
	ReportBuilder.configure do |config|
		config.input_path = {
				'Ejecucion Normal' => "#{@file_single}",
				'Ejecucion Rerun' => "#{@file_rerun}",
				'Ejecucion Merge: "Normal VS Rerun"' => ["#{@file_single}", "#{@file_rerun}"]
		}
		config.report_path = "#{@path_results}/results"
		config.report_types = [:retry, :html, :json]
		config.report_title = Config.report_proyect_name
		config.additional_info = {
				#NAVEGADOR: Config.driver_name.to_s.upcase,
				NAVEGADOR: 'WEB MOBILE',
				AMBIENTE: Config.environment.to_s.upcase,
				DISPOSITIVO: Config.get_data_device
		}
		config.color = 'indigo'
	end

	ReportBuilder.build_report
	puts "REPORTE 'HTML' REPORT BUILDER TERMINADO.... "

rescue Exception => e
	puts "ERROR. AL CREAR REPORT BUILDER 'HTML'... \nException: #{e.message}"
end

#EJECUTAR
report_builder

