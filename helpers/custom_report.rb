require 'fileutils'
require_relative 'config'

#Path relativo y nombre del archivo de ejecucion single | ARGV[0] => nombre archivo json 'ejecucion single'
@file_report  = ARGV[0]
#@file_report  = File.join(File.dirname(__FILE__), "../venture/evidence/200526/0206/live_desktop_chrome/single/results.html")

def override_html
	puts "Archivo de reporte => #{@file_report}"
	report = File.read(@file_report.to_s.strip)

	#sobreescribimos el fondo de color del HEADER del html cuando es 'PASS'
	report['background: #65c400;'] = 'background: black;' unless report['background: #65c400;'].nil?
	#sobreescribimos el fondo de color del HEADER del html cuando es 'FAIL'
	report["makeRed('cucumber-header');"] = '' unless report["makeRed('cucumber-header');"].nil?
	#sobreescribimos el alto 'height' del HEADER del html
	report['height: 6em;'] = 'height: 16em;' unless report['height: 6em;'].nil?

	#copiamos el logo al directorio de ejecucion en curso
	begin
		FileUtils.cp("../venture/config/#{Config.report_logo}", "../venture/#{Config.path_results}/#{Config.report_logo}")
		FileUtils.cp("../venture/config/#{Config.report_logo_cliente}", "../venture/#{Config.path_results}/#{Config.report_logo_cliente}")
	rescue Exception => e
		puts "override_html => NO SE PUDO COPIAR LOGO O LOGO2 A PATH DE RESULTADOS. EXCEPTION: #{e.message}"
	end

	#sobreescribimos el titulo 'Cucumber Features' por la variable que contiene
	#	tambien agregamos la imagen de logo
	header = StringIO.new
	header << "<img src=\"#{Config.report_logo.to_s}\" width=\"250\" height=\"100\" style=\"margin:0px 20px\">"
	header << "<img src=\"#{Config.report_logo_cliente.to_s}\" width=\"300\" height=\"100\" style=\"float:right; margin: 0px 20px 0px 0px\">"
	header << "<h1>#{Config.report_proyect_name}</h1>"
	report['<h1>Cucumber Features</h1>'] = header.string.to_s unless report['<h1>Cucumber Features</h1>'].nil?

	#Sobreescribimos los saltos de linea entre el 'STEP' y 'IMAGEN DE EVIDENCIA'
	#report['<br>&nbsp;'] = ''
	r = String.new(report)
	report_new = []
	report_new = r.scan(/<br>&nbsp;/)
	for i in 0..(report_new.size - 1)
		r[report_new[i]] = ''
	end
	report = r

	#Agregamos un salto linea el final del 'STEP'
	#report['</span></div></li>'] = '<br>'
	r = String.new(report)
	report_new = []
	report_new = r.scan(/<\/span><\/div><\/li>/)
	for j in 0..(report_new.size - 1)
		r[report_new[j]] = '</span> </div> </li> <br>'
	end
	report = r

	#SOBREESCRIBIMOS EL ARCHIVO HTML
	File.open("#{@file_report.to_s.strip}", 'w') do |f|
		f.write(report.to_s)
		f.close
	end

	puts "REPORTE CUSTOMED TERMINADO.... "

end


#EJECUTAR EL OVERRIDE AL REPORTE
override_html

