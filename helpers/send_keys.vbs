'*************************************************************************************************
' GET PARAMETERS
'*************************************************************************************************
Dim args, keys, app, filtro_app

Set args = WScript.Arguments
keys = args.Item(0)  'the parameter from batch comes in here to arg1
app = args.Item(1)
filtro_app = args.Item(2)

If CStr(filtro_app) = "nombre_proceso" Then
	sentKeysByWNDProcessName keys, app
Else
	sentKeys keys, app
End If

'*************************************************************************************************
' CODIGO SEND KEYS
'*************************************************************************************************

Public Sub sentKeysByWNDProcessName(keys, process_name)
	Dim objShell, WMI, wql, process
	Set objShell = WScript.CreateObject("WScript.Shell")

	WScript.Sleep 1000

	Set WMI = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
	wql = "SELECT ProcessId FROM Win32_Process WHERE Name = '" & process_name & "'"

	For Each process In WMI.ExecQuery(wql)
		objShell.AppActivate(process.ProcessID)
		objShell.SendKeys keys
	Next

	WScript.Sleep 1000

	Set objShell = Nothing
End Sub


Public Sub sentKeys(keys, app_ventana_titulo)
	Dim objShell
	Set objShell = WScript.CreateObject("WScript.Shell")

	WScript.Sleep 1000
	objShell.AppActivate(app_ventana_titulo)
	objShell.SendKeys keys
	WScript.Sleep 1000

	Set objShell = Nothing

End Sub


