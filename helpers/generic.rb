# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper Metodos gnericos aqui
# 		encontraras cualquier cosa
#####################################################

require 'fileutils'
require 'yaml'
require 'cucumber'
require 'cucumber/formatter/console'
require 'cucumber/formatter/html'
require_relative 'config'
require_relative 'data_driven/csv_data.rb'


def string_market
	return "****************************************************************************"
end

#Método que obtiene un numero unico apartir del timestamps
def timestamps
	(Time.now.to_f * 1000).to_i
end

#Método que obtiene la fecha en formato HORA,MINUTO,SEGUNDO
def datetime
	Time.now.strftime('%H%M%S').to_s
end

#Método que genera un email de forma random, apartir de la fecha
def email_random
	return "test.#{timestamps}@test.com"
end


##################################################################################
##################################################################################
##################################################################################
### UTILERIAS DEL NAVEGADOR
##################################################################################
##################################################################################
##################################################################################

#Renderiza el browser para las dimenciones de 'WAM (WEB AUTOMATION MOBILE) - CAPYBARA'
def capybara_resize_browser_to_wam
	device = Config.get_data_device[:device_name]
	unless device.to_s.downcase.include?('desktop')
		page.driver.browser.manage.window.resize_to(Config.get_data_device[:resolution_width], Config.get_data_device[:resolution_height])
	end
end

#Método para Limpiar 'LOCALSTORAGE - CAPYBARA'
def capybara_clear_cache_localstorage
	unless Config.driver_name == 'ws'
		if Config.driver_name.to_s.to_sym == :firefox || Config.driver_name.to_s.to_sym == :safari
			#page.execute_script('if (localStorage && localStorage.clear) localStorage.clear()')
		else
			#page.execute_script('window.localStorage.clear;')
		end
	end
	puts "Clear Cache LocalStorage"
rescue Exception => e
	puts "clear_cache => no fue posible borrar cache y limpiar cookies #{e}"
end

#borrar cookies 'SELENIUM Y CAPYBARA'
def clear_cookies_browser
	browser = Capybara.current_session.driver.browser
	if browser.respond_to?(:clear_cookies)
		browser.clear_cookies
	elsif browser.respond_to?(:manage) and browser.manage.respond_to?(:delete_all_cookies)
		# Selenium::WebDriver
		browser.manage.delete_all_cookies
	else
		raise "NO SE PUEDEN BORRAR COOKIES, NO SE DETECTA EL TIPO DE DRIVER"
	end
end

#Método que permite codificar un texto a utf8, necesario al usar acentos, eñe, caracteres especiales
def text_encoding_utf8(text)
	return text.to_s.force_encoding(Encoding::UTF_8)
end

##################################################################################
##################################################################################
##################################################################################
### EVIDENCIA
##################################################################################
##################################################################################
##################################################################################

def init_path
	#path = File.join(File.dirname(__FILE__), "../venture/reports/#{$exec_date}/#{$exec_time}/#{$exec_type.to_s.downcase}")
	path = File.join(File.dirname(__FILE__), "../venture/evidence/#{$exec_date}/#{$exec_time}/#{$exec_type.to_s.downcase}/#{$env.to_s.downcase}_#{$device.to_s.downcase}_#{$browser.to_s.downcase}")
	FileUtils::mkdir_p path unless Dir.exist? path
end

#Metodo para setear las imagenes al reporte de cucumber html, EN EL HOOKS AFTER ESCENARIO
def set_scenario_screens_to_cucumber_report(evidence_dir)
	files = Dir["#{evidence_dir}/*"]
	for i in 0..(files.size - 1)
		file_name = File.basename files[i]
		#puts "DATA  => #{x }"
		embed(File.expand_path("#{evidence_dir}/#{file_name.to_s}"), 'image/png', "#{file_name.to_s}")
	end
end

#Método que obtiene el directorio donde se alamacenara el 'screenshot'
# Se recuperan las variables 'Config.current_feature' y 'Config.current_scenario' desde 'hooks scenario => before'
def save_evidence(evidence_status)
	feature_name = special_chars_replace(Config.current_feature.to_s).to_s.downcase.strip
	scenario_name = scenario_name_remove_tc(Config.current_scenario.to_s).to_s.downcase.strip
	scenario_name = special_chars_replace(scenario_name).to_s.downcase.strip
	evidence_dir = "#{Config.path_results}/screenshots/#{feature_name}/#{scenario_name}"
	evidence_name = "#{datetime}_#{evidence_status}.png"
	FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir

	if Config.driver_name.to_s.downcase.include?('capybara') or
			Config.driver_name.to_s.downcase.include?('lambdatest_capybara')
		page.save_screenshot("#{evidence_dir}/#{evidence_name}", :full => true)
		#embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png', "#{evidence_name.to_s}")
		base64_img = Base64.encode64(File.open("#{evidence_dir}/#{evidence_name.to_s}", 'r:UTF-8', &:read))
		#embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png;base64', "<li class=\"step message\">SCREENSHOT: #{evidence_name.to_s}</li>")
		embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png', "<li class=\"step message\">SCREENSHOT: #{evidence_name.to_s}</li> ")
	end
	if Config.driver_name.to_s.downcase.include?('webdriver')
		page.save_screenshot("#{evidence_dir}/#{evidence_name}", :full => true)
		embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png', "<li class=\"step message\">SCREENSHOT: #{evidence_name.to_s}</li> ")
	end
	if Config.driver_name.to_s.downcase.include?('appium')
		Config.driver_appium.save_screenshot("#{evidence_dir}/#{evidence_name}")
		embed(File.expand_path("#{evidence_dir}/#{evidence_name}"), 'image/png', "<li class=\"step message\">SCREENSHOT: #{evidence_name.to_s}</li> ")
	end
	if Config.driver_name.to_s.downcase.include?('calabash')
		base64_img = Base64.encode64(File.open("#{evidence_dir}/#{evidence_name.to_s}", 'r:UTF-8', &:read))
		screenshot_embed({:prefix=>"#{evidence_dir}/", :name=>"#{evidence_name}"})
	end

	#Agregar a config el valor de la evidencia actual
	#Config.current_evidence_file = "#{File.expand_path(File.dirname(__FILE__))}/../venture/#{evidence_dir}/#{evidence_name}"
	Config.current_evidence_file = "#{File.expand_path("#{evidence_dir}/#{evidence_name}")}"
	#Agregar a config el valor del path actual de las screenshots
	Config.current_screenshot_path = "#{File.expand_path("#{evidence_dir}")}"

rescue Exception => e
	puts "Error. No se pudo tomar screenshot. \nException => #{e}"
end

#Método para obtener evidencia.
# Se usa manualmente en donde se invoque este método se tomara un screenshot.
def save_evidence_execution
	save_evidence 'pass'
end

#Método para obtner evidencia y forzarla a ser fail, esto para ocuparce dentro de steps, bucles.
# Se usa manualmente en donde se invoque este método se tomara un screenshot.
def save_evidence_execution_fail
	save_evidence 'fail'
end

##################################################################################
### EVIDENCIA - FILE
##################################################################################

#Método para guardar 1 archivo de evidencia
def save_evidence_file(text_file, file_name = nil, file_extencion = nil)
	feature_name = special_chars_replace(Config.current_feature.to_s.downcase.strip)
	scenario_name = special_chars_replace(Config.current_scenario.to_s.downcase.strip)
	evidence_dir = "#{Config.path_results}/screenshots/#{feature_name}/#{scenario_name}"

	#crear nombre y extencion de archivo
	file_name = file_name.nil? ? "evidence_file" : file_name
	file_extencion = file_extencion.nil? ? "txt" : file_extencion
	evidence_file_name = "#{file_name}.#{file_extencion}"

	#crear directorio de evidencia
	FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir

	#escribir archivo
	File.open("#{evidence_dir}/#{evidence_file_name}", 'w') { |file| file.write(text_file.to_s.strip) }
	#agregar archivo a reporte cucumber
	embed(File.expand_path("#{evidence_dir}/#{evidence_file_name.to_s}"), 'text/plain', "<p>#{evidence_file_name.to_s}</p>")

rescue Exception => e
	puts "Error => save_evidence_file => No se pudo crear archivo de evidencia #{e.message}"

end


##################################################################################
##################################################################################
##################################################################################
### CUCUMBER - UTILERIAS ESCENARIOS
##################################################################################
##################################################################################
##################################################################################

#Método para obtener el nombre del scenario, esto permite extraerlo y pasarlo al nombre de la evidencia
def scenario_name_remove_tc(scenario)
	a = scenario.to_s.split('(TC')
	return a[0].to_s.downcase.strip
end

#Método que quita caracteres especiales a un string
def special_chars_replace(string_value)
	string_value = string_value.to_s.tr(':', '')
	string_value = string_value.to_s.tr('-', '')
	string_value = string_value.to_s.tr(',', '') # ,
	string_value = string_value.to_s.tr('.', '')
	string_value = string_value.to_s.tr('"', '')
	string_value = string_value.to_s.tr( ';', '' )
	string_value = string_value.to_s.tr( '(', '' )
	string_value = string_value.to_s.tr( ')', '' )
	string_value = string_value.to_s.tr( '{', '' )
	string_value = string_value.to_s.tr( '}', '' )
	string_value = string_value.to_s.tr( '[', '' )
	string_value = string_value.to_s.tr( ']', '' )
	string_value = string_value.to_s.tr( '!', '' )
	string_value = string_value.to_s.tr( '@', '' )
	string_value = string_value.to_s.tr( '#', '' )
	string_value = string_value.to_s.tr( '$', '' )
	string_value = string_value.to_s.tr( '%', '' )
	string_value = string_value.to_s.tr( '&', '' )
	string_value = string_value.to_s.tr( '/', '' )
	string_value = string_value.to_s.tr( '?', '' )
	string_value = string_value.to_s.tr( '¿', '' )
	string_value = string_value.to_s.tr( '=', '' )
	string_value = string_value.to_s.tr( '>', '' )
	string_value = string_value.to_s.tr( 'á', 'a' )
	string_value = string_value.to_s.tr( 'é', 'e' )
	string_value = string_value.to_s.tr( 'í', 'i' )
	string_value = string_value.to_s.tr( 'ó', 'o' )
	string_value = string_value.to_s.tr( 'ú', 'u' )
	string_value = string_value.to_s.tr( ' ', '_' )
	return string_value.to_s.downcase.strip
end



##################################################################################
##################################################################################
##################################################################################
### VBS - SEND KEYS
##################################################################################
##################################################################################
##################################################################################

#Metodo que permite enviar secuencia de teclas, desde teclado y no a un webelemento como lo hace capibara
# @params
#   :keys String teclas a enviar. Estandar de VBS
#   :process_filter String proceso por nombre o por titulo de ventana.
# 			Ej. "Process.Name = Calculator.exe" / "titulo ventana = Calculadora"
#   :filtro String filtro. Opciones validas "nombre_proceso/titulo_ventana"
# @examples:
# 		send_keys_vbs "{ESC}", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "100-20", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "{ENTER}", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "^(C)", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "{ESC}", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "^(V)", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "%H", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "{DOWN}", "Calculadora", "titulo_ventana"
# 		send_keys_vbs "{ENTER}", "Calculadora", "titulo_ventana"
def send_keys_vbs(keys, process_filter, filtro)
	begin
		require 'os'
		if OS.windows?
			require 'open3'
			cmd = "cscript ../helpers/send_keys.vbs \"#{keys.to_s}\" \"#{process_filter.to_s}\" \"#{filtro.to_s}\""
			stdin, stdout, waiter = Open3.popen2(cmd)
			status = waiter.value

			unless status.success?
				raise "send_keys_vbs => Error al ejecutar VBS: 'send_keys.vbs': #{cmd} Returned #{stdin}  #{stdout} :: #{status}"
			end

		end
	rescue Exception => e
		puts "send_keys_vbs => ERROR DE EJECUCION. \nException => #{e.message}"
	end
end



##################################################################################
##################################################################################
##################################################################################
### ASSERTS GENERICOS
##################################################################################
##################################################################################
##################################################################################

def gen_assert_text(actual_text, expected_text, msj_error = nil)
	unless actual_text.to_s.strip == expected_text.to_s.strip
		msj = StringIO.new
		msj << "\n"
		msj << "#{string_market}\n"
		msj << "ERROR. ASSERT TEXT.\n"
		msj << "Texto Actual: '#{actual_text.to_s}'.\n"
		msj << "Texto Esperado: '#{expected_text.to_s}'.\n"
		msj << "Error Especifico: '#{msj_error.to_s}'.\n"
		msj << "#{string_market}\n"
		raise msj.string.to_s
	end
	return true
end
def gen_verify_text(actual_text, expected_text, msj_error = nil)
	result = true
	unless actual_text.to_s.strip == expected_text.to_s.strip
		msj = StringIO.new
		msj << "\n"
		msj << "#{string_market}\n"
		msj << "ERROR. ASSERT TEXT.\n"
		msj << "Texto Actual: '#{actual_text.to_s}'.\n"
		msj << "Texto Esperado: '#{expected_text.to_s}'.\n"
		msj << "Error Especifico: '#{msj_error.to_s}'.\n"
		msj << "#{string_market}\n"
		puts msj.string.to_s
		result = false
	end
	return result
end

def gen_assert_text_contains(actual_text, expected_text, msj_error = nil)
	unless actual_text.to_s.downcase.strip.include?(expected_text.to_s.downcase.strip)
		msj = StringIO.new
		msj << "\n"
		msj << "#{string_market}\n"
		msj << "ERROR. ASSERT TEXT CONTAINS.\n"
		msj << "Texto Actual: '#{actual_text.to_s}'.\n"
		msj << "Texto Esperado: '#{expected_text.to_s}'.\n"
		msj << "Error Especifico: '#{msj_error.to_s}'.\n"
		msj << "#{string_market}\n"
		raise msj.string.to_s
	end
	return true
end



##################################################################################
### NET HTTP - UTILIERIAS
##################################################################################
def get_method_request_net_http(url, method)
	require 'net/http'
	request = nil

	case method.to_s.to_sym
	when :get then
		request = Net::HTTP::Get.new(url)
	when :post then
		request = Net::HTTP::Post.new(url)
	when :put then
		request = Net::HTTP::Put.new(url)
	when :delete then
		request = Net::HTTP::Delete.new(url)
	when :patch then
		request = Net::HTTP::Patch.new(url)
	else
		raise("method request => opción http method invalida => #{method}")
	end

	return request
end