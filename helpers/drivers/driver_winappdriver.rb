# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def winappdriver(device_name, platform_name, appium_server, app=nil)
		# Locator Strategy	Matched Attribute
		# accessibility id	AutomationId
		# class name	ClassName
		# name	Name
		# xpath	xpath

		require 'selenium-webdriver'
		require 'rubygems'
		require 'appium_lib'

		opts =
				{
						caps:
								{
										#platformName: "WINDOWS",
										#platform: "WINDOWS",
										#deviceName: "LAPTOP-ANOPMD21",
										#app: 'D:\ambiente\selenium_drivers\winium_app.bat'
										#app: 'Microsoft.WindowsCalculator_8wekyb3d8bbwe!App'
										platformName: platform_name,
										platform: platform_name,
										deviceName: device_name.to_s,
										app: app.to_s
								},
						appium_lib:
								{
										#server_url: "http://127.0.0.1:4723/",
										server_url: appium_server,
										wait_timeout: 5,
										wait_interval: 0.5
								}
				}

		#$appium_session = Selenium::WebDriver.for :remote, url: "http://127.0.0.1:4723/", desired_capabilities: opts
		driver = Appium::Driver.new(opts, false).start_driver
		puts "APPIUM SESSION = #{driver}"
		return driver

		#esperar hasta que exista
		#wait = Selenium::WebDriver::Wait.new :timeout => 20
		#wait.until { $CalculatorSession.find_elements(:name, "Clear")[0] != nil }

		# clear if already running
		#$appium_session.find_element(:name, "Clear").click()
	end


end
