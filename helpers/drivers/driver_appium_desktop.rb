# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def appium_desktop(device_name, platform_name, appium_server, app)
		require 'selenium-webdriver'
		puts "SELENIUM APPIUM DRIVER"

		caps = Selenium::WebDriver::Remote::Capabilities.new
		#caps['app'] = 'Microsoft.WindowsCalculator_8wekyb3d8bbwe!App'
		caps['app'] = app.to_s
		caps['platformName'] = platform_name.to_s
		caps['deviceName'] = device_name.to_s
		caps["requireWindowFocus"] = true
		caps.javascript_enabled = true

		driver = Selenium::WebDriver.for :remote, url: appium_server.to_s,
																						desired_capabilities: caps
		return driver
	end

end
