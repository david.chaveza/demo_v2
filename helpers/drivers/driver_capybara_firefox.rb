# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def capybara_firefox(user_agent, timeout)
		require 'selenium/webdriver'

		Capybara.default_driver = :selenium
		Capybara.reset_sessions!
		Capybara.reset!
		Capybara.default_max_wait_time = timeout.to_i
		Capybara.ignore_hidden_elements = false

		profile = Selenium::WebDriver::Firefox::Profile.new
		#Clear chache
		profile['browser.cache.disk.enable'] = false
		profile['browser.cache.memory.enable'] = false
		#Change the line below to change the user agent
		#profile['general.useragent.override'] = user_agent
		Capybara.register_driver :selenium do |app|
			Capybara::Selenium::Driver.new(app, :profile => profile)
		end
	end

end
