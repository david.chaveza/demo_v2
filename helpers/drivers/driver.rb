require 'em/pure_ruby'
require 'capybara/cucumber'
require_relative '../../helpers/config.rb'
require_relative '../../helpers/drivers/driver_appium'
require_relative '../../helpers/drivers/driver_appium_desktop'
require_relative '../../helpers/drivers/driver_appium_wam'
require_relative '../../helpers/drivers/driver_capybara_chrome'
require_relative '../../helpers/drivers/driver_capybara_chrome_headless'
require_relative '../../helpers/drivers/driver_capybara_firefox'
require_relative '../../helpers/drivers/driver_capybara_ie'
require_relative '../../helpers/drivers/driver_winappdriver'
require_relative '../../helpers/drivers/driver_winium'
require_relative '../../helpers/drivers/lambdatest/driver_lambdatest'
require_relative '../../helpers/kobiton/devices/devices_utils'
require_relative '../../helpers/kobiton/apps/apps_utils'

module Drivers

	def driver(driver_name = nil)
		#DOCUMENTACION PARA CONFIGURAR DRIVERES EN VARIOS BROWSERS USANDO CAPABILITIES
		#https://testingbot.com/support/getting-started/cucumber.html
		env = Config.get_environment_data
		device = Config.get_data_device
		driver = driver_name.nil? ? Config.driver_name.to_s.to_sym : driver_name.to_s.to_sym
		puts "DRIVER => #{driver}"


		case driver

		when :firefox_capybara
			capybara_firefox(device[:user_agent], Config.to)

		when :chrome_capybara
			capybara_chrome(device[:user_agent], Config.to)

		when :chrome_headless_capybara
			capybara_chrome(device[:user_agent], Config.to, true)

		when :ie_capybara
			capybara_ie(Config.to, device[:app_server])

		when :wam_capybara
			#You can use Chrome's predefined list of devices that comes with settings for resolution, DPI, and touch capabilities.
			capybara_chrome(device[:user_agent], Config.to, nil,
											true, device[:cap_device_name], device[:resolution_width], device[:resolution_height])

		when :wam_capybara_headless
			#You can use Chrome's predefined list of devices that comes with settings for resolution, DPI, and touch capabilities.
			capybara_chrome(device[:user_agent], Config.to, true,
											true, device[:cap_device_name], device[:resolution_width], device[:resolution_height])

		when :lambdatest_capybara
			#You can use Chrome's predefined list of devices that comes with settings for resolution, DPI, and touch capabilities.
			lambdatest_capybara(Config.to)

		when :winium
			launch_winium_server(device[:cmd_start_server]) unless device[:cmd_start_server].to_s.downcase.strip == 'na'
			#OBTENER APP DE "PATH_EXTERNAL_DATA" si esta no es NIL
			app = Config.path_external_data.nil? ? env[:desktop_app] : "#{Config.path_external_data}/apps/#{app}" if device[:instancia_appium].to_s.to_sym == :local
			Config.driver_winium = winium(app, device[:app_server])

		when :appium
			#SET DEVICE ENVIRONMENT PARA USAR MATE
			ENV['DEVICE'] = device[:device_uid].to_s.strip
			#OBTENER APP ID DE KOBITON
			app = device[:instancia_appium].to_s.to_sym == :local ? env[:mobile_app] : kb_get_app_id(env[:mobile_app], device[:platform_name])
			#OBTENER APP DE "PATH_EXTERNAL_DATA" si esta no es NIL
			app = Config.path_external_data.nil? ? app : "#{Config.path_external_data}/apps/#{app}" if device[:instancia_appium].to_s.to_sym == :local
			#VALIDAR SI EL DISPOSITIVO ESTA DISPONIBLE EN KOBITON
			kb_wait_for_device_ativated(device[:instancia_appium], device[:device_uid])
			#START APPIUM SERVER
			#launch_appium_server(device[:cmd_start_server]) unless device[:cmd_start_server].to_s.downcase.strip == 'na'
			Config.driver_appium = appium( device[:device_uid], device[:cap_device_name], device[:platform_name], device[:platform_version],
																		 device[:app_server], device[:device_orientation],
																		 env[:mobile_app_package], env[:mobile_app_activity], app,
																		 "TAF - #{device[:device_name]}", 2186,
																		 device[:instancia_appium]
			)

		when :appium_wam
			#SET DEVICE ENVIRONMENT PARA USAR MATE
			ENV['DEVICE'] = device[:device_uid].to_s.strip
			#VALIDAR SI EL DISPOSITIVO ESTA DISPONIBLE EN KOBITON
			kb_wait_for_device_ativated(device[:instancia_appium], device[:device_uid])
			#START APPIUM SERVER
			#launch_appium_server(device[:cmd_start_server]) unless device[:cmd_start_server].to_s.downcase.strip == 'na'
			browser = device[:platform_name].to_s.strip.downcase.include?('android') ? 'chrome' : 'safari'
			Config.driver_appium = appium_wam( device[:device_uid], device[:cap_device_name], device[:platform_name], device[:platform_version],
																				 device[:app_server], device[:device_orientation], browser,
																				 2186, "TAF - #{device[:device_name]}",
																				 device[:instancia_appium]
			)

		when :appium_desktop
			#START APPIUM SERVER
			launch_appium_server(device[:cmd_start_server]) unless device[:cmd_start_server].to_s.downcase.strip == 'na'
			#OBTENER APP DE "PATH_EXTERNAL_DATA" si esta no es NIL
			app = Config.path_external_data.nil? ? env[:desktop_app] : "#{Config.path_external_data}/apps/#{app}" if device[:instancia_appium].to_s.to_sym == :local
			Config.driver_appium = appium_desktop(device[:cap_device_name], device[:platform_name], device[:app_server], app)

		when :winappdriver
			launch_appium_server(device[:cmd_start_server]) unless device[:cmd_start_server].to_s.downcase.strip == 'na'
			#OBTENER APP DE "PATH_EXTERNAL_DATA" si esta no es NIL
			app = Config.path_external_data.nil? ? env[:desktop_app] : "#{Config.path_external_data}/apps/#{app}" if device[:instancia_appium].to_s.to_sym == :local
			Config.winappdriver = winappdriver(device[:cap_device_name], device[:platform_name], device[:app_server], app)

		when :ws
			puts "WS DRIVER"

		else
			raise "ERROR AL INICIALIZAR EL DRIVER. Driver invalido => #{Config.driver}"

		end

	end

end
