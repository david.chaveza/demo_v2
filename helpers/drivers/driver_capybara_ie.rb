# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def capybara_ie(timeout, url_server)
		require 'selenium/webdriver'

		Capybara.app_host = "http://localhost:3000"
		Capybara.default_driver = :selenium
		Capybara.reset!
		Capybara.default_max_wait_time = timeout.to_i
		Capybara.ignore_hidden_elements = false

		Capybara.register_driver :selenium do |app|
			#caps = Selenium::WebDriver::Remote::Capabilities.internet_explorer
			#{#caps.version = 10
			#caps.javascript_enabled = true # Javascript not enabled by default, see "Gotchas" below.
			##caps["requireWindowFocus"] = true
			#Capybara::Selenium::Driver.new(app, :browser => :internet_explorer, :desired_capabilities => caps)
			#Capybara::Selenium::Driver.new(app, :browser => :internet_explorer)
			Capybara::Selenium::Driver.new(app,
																		 :browser => :ie,
																		 :url => url_server.to_s
			)
			#:url => "http://localhost:4444/wd/hub"

		end

	end

end
