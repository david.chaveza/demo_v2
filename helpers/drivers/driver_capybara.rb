# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	#Metodo permite resetear capybara por errores Net::ReadTimeout
	def capybara_reset_in_net_readtimeout_ex(scenario)
		if scenario.failed?
			#EXCEPTION HTTP READ TIMEOUT
			if scenario.exception.message.to_s.include?("Net::ReadTimeout")
				puts "********* Net::ReadTimeout Error!! Reiniciar Capybara !*************"
				Capybara.current_session.driver.quit
				#driver
				#Drivers::driver
			end
		end
	end

end
