# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def capybara_chrome(user_agent, timeout, headless=nil,
											wam=nil, wam_device=nil, wam_width=nil, wam_height=nil)
		#require "chromedriver-screenshot"
		require 'webdrivers'
		require 'selenium-webdriver'

		#ACTUALIZAR DRIVERS
		Webdrivers.install_dir = Config.selenium_drivers
		Webdrivers::Chromedriver.remove
		Webdrivers::Chromedriver.update
		driver_path = Webdrivers::Chromedriver.driver_path

		# This env var comes from chromedriver_linux64, used on the TravisCI
		chrome_bin = ENV.fetch('CHROME_BIN', nil)
		chrome_options = {}
		chrome_options[:binary] = chrome_bin if chrome_bin
		#logging_preferences = { browser: 'SEVERE' }
		#logging_preferences = { browser: 'DEBUG' }
		logging_preferences = { browser: 'ALL' }

		Capybara.register_driver :chrome do |app|
			client = Selenium::WebDriver::Remote::Http::Default.new
			client.read_timeout = 180
			args = []
			#args << "--window-size=#{$device_data[:resolution_width]},#{$device_data[:resolution_height]}"
			#args << "--user-agent='#{$device_data[:user_agent].to_s}'"
			capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
					chromeOptions: chrome_options,
					loggingPrefs: logging_preferences,
					w3c: false
			)
			options = Selenium::WebDriver::Chrome::Options.new
			#options.add_argument("--window-size=#{$device_data[:resolution_width]}, #{$device_data[:resolution_height]}")
			options.add_argument("--start-maximized")
			if user_agent
				options.add_argument("--user-agent='#{user_agent.to_s.strip}'")
			end
			options.add_argument("--disable-infobars")
			options.add_argument("--logger=error")
			options.add_argument("--test-type")
			options.add_argument("--enable-features=NetworkService,NetworkServiceInProcess")
			options.add_argument('--disable-features=VizDisplayCompositor')
			options.add_argument('--disable-dev-shm-usage')
			if wam
				#options.add_emulation(device_metrics: { width: 640, height: 360, touch: false })
				raise "ERROR. DEVICE NAME ES VACIO O NULO. WAM_DEVICE: '#{wam_device}' \nVALIDAR '../venture/config/00_config/dt_devices.csv'" if wam_device.nil?

				#SET DEVICE O CUSTOM DEVICE
				if wam_device.to_s.downcase.strip == 'wam_custom'
					mobile_emulation = {
						width: wam_width.to_s.to_i, height: wam_height.to_s.to_i,
						pixelRatio: 1, touch: true
					}
					options.add_emulation(device_metrics: mobile_emulation)
					puts "WAM: DEVICE CUSTOM"
				else
					options.add_emulation(device_name: wam_device)
					puts "WAM: DEVICE: #{wam_device}"
				end
				#SET USER AGENT WAM
				if user_agent
					options.add_emulation(user_agent: user_agent.to_s.strip)
				end
			end
			if headless
				options.add_argument("headless disable-gpu")
			end

			#selenium_services = Selenium::WebDriver::Chrome::Service.new
			#selenium_services.driver_path = driver_path

			Capybara::Selenium::Driver.new(app,
																		 #service: selenium_services,
																		 driver_path: driver_path,
																		 browser: :chrome,
																		 desired_capabilities: capabilities, options: options
			)
		end

		Capybara.default_driver = :chrome
		Capybara.reset_sessions!
		Capybara.reset!
		Capybara.default_max_wait_time = timeout.to_i
		Capybara.ignore_hidden_elements = false
		Capybara.javascript_driver = :chrome # :chrome simulates behavior in browser
		#ChromedriverScreenshot.take_full_screenshots = true

	end

end
