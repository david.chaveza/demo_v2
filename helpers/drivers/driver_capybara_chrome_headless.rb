# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

	def capybara_chrome_headless(user_agent, timeout)
		#require "chromedriver-screenshot"
		#require 'webdrivers'
		require 'selenium-webdriver'

		# This env var comes from chromedriver_linux64, used on the TravisCI
		chrome_bin = ENV.fetch('CHROME_BIN', nil)
		chrome_options = {}
		chrome_options[:binary] = chrome_bin if chrome_bin
		logging_preferences = { browser: 'ALL' }

		Capybara.register_driver :chrome do |app|
			client = Selenium::WebDriver::Remote::Http::Default.new
			client.read_timeout = 180
			args = []
			capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
					chromeOptions: chrome_options,
					loggingPrefs: logging_preferences,
					w3c: false
			)
			options = Selenium::WebDriver::Chrome::Options.new
			#options.add_argument("--window-size=#{$device_data[:resolution_width]}, #{$device_data[:resolution_height]}")
			options.add_argument("--start-maximized")
			options.add_argument("--user-agent='#{user_agent.to_s.strip}'")
			options.add_argument("--disable-infobars")
			options.add_argument("--logger=error")
			options.add_argument("--test-type")
			options.add_argument("--enable-features=NetworkService,NetworkServiceInProcess")
			options.add_argument('--disable-features=VizDisplayCompositor')
			options.add_argument('--disable-dev-shm-usage')
			options.add_argument("headless disable-gpu")
			Capybara::Selenium::Driver.new(app, browser: :chrome,
																		 desired_capabilities: capabilities, options: options)
		end

		Capybara.default_driver = :chrome
		Capybara.reset_sessions!
		Capybara.reset!
		Capybara.default_max_wait_time = timeout.to_i
		Capybara.ignore_hidden_elements = false
		Capybara.javascript_driver = :chrome # :chrome simulates behavior in browser
		#ChromedriverScreenshot.take_full_screenshots = true

	end

end
