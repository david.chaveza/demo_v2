# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#Info:  http://appium.io/docs/en/writing-running-appium/caps/
# 			http://automate-apps.com/start-appium-server-from-command-line/
# 			INSTALAR NODE JS: https://nodejs.org/en/download/
# 					npm uninstall -g appium
# 					npm install -g appium@beta
#####################################################

module Drivers

	#' APPIUM WAM (WEB AUTOMATION MOBILE)'
	def appium_wam(device_uid, device_name, platform_name, platform_version, appium_server,
								 device_orientation, browser=nil,
								 kobiton_groupid=nil, kobiton_sessionName=nil, instancia_appium=nil)
		require 'selenium-webdriver'
		#require 'rubygems'
		require 'appium_lib'

		@chrome_driver_executable = "D:/ambiente/selenium_drivers/appium_chrome/chromedriver.exe"

		desired_caps = {
				:caps => {
						#****************************************************************************
						# KOBITON SESSION DATA
						#****************************************************************************
						#:sessionName => 'TESTING IT - AUTOMATION REGRESION',
						:sessionName => kobiton_sessionName.to_s,
						:sessionDescription => '',
						:deviceOrientation => device_orientation.to_s.strip,
						:captureScreenshots => true,
						:deviceGroup => 'ORGANIZATION',
						#:groupId => 1019, # Group: Admin
						#:groupId => kobiton_groupid, # Group: Automatizacion - Group ID: 1012
						:groupId => kobiton_groupid, # Group: Automatizacion - Group ID: 1012

						#****************************************************************************
						# DEVICE DATA
						#****************************************************************************
						:platformName => platform_name.to_s.strip,
						:platformVersion => platform_version.to_s.strip,
						:deviceName => device_name.to_s.strip,
						:udid => device_uid.to_s.strip,
						# APP DATA (WEB/APK/IPA)
						:browserName => browser.to_s.strip,
						#:chromedriverChromeMappingFile => "#{ENV['SELENIUM_DRIVERS'].to_s}/appium_chrome/chromedriverChromeMappingFile.json",

						# CONFIGS
						:noReset =>  true,
						:connectHardwareKeyboard => true,
						:clearSystemFiles => true,
						:adbExecTimeout => 60000,
						:newCommandTimeout => 180000
				},
				:appium_lib => {
						#:server_url => 'https://david.chavez.avila:a8b8d1ae-a394-444c-9ea2-3f615c390d80@api.kobiton.com/wd/hub'
						:server_url => appium_server.to_s.strip
				}
		}

		if instancia_appium.to_s.to_sym == :local
			desired_caps[:caps][:chromedriverExecutable] = @chrome_driver_executable
		end

		#driver = Selenium::WebDriver.for :remote, url: appium_server.to_s.strip, desired_capabilities: desired_caps[:caps]
		driver = Appium::Driver.new(desired_caps, true).start_driver
		puts "APPIUM SESSION = #{driver}"
		return driver
	end

end
