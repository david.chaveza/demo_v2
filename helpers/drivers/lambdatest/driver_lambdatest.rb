# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

#****************************************************************************************
#****************************************************************************************
# OBTENER CAPABILITIES DE: CSV Y LT_VARS => VARIABLES ENT0RONO
#****************************************************************************************
#****************************************************************************************
#

module Drivers

	def lambdatest_capybara(timeout)
		url_server = "https://git.client.user:GmJuW2iJs5xFUWEVZe3Yu22ffGgi4gV6AOpXxDS6sII2wiInDP@hub.lambdatest.com/wd/hub"
		caps = capabilities_lambdatest_capybara

		require 'webdrivers'
		require 'selenium-webdriver'

		Capybara.register_driver :lambdatest  do |app|
			Capybara::Selenium::Driver.new(app,
													 browser: :remote,
																		 url: url_server,
																		 desired_capabilities: caps
			)
		end

		Capybara.default_driver = :lambdatest
		Capybara.run_server = false
		Capybara.reset_sessions!
		Capybara.reset!
		Capybara.default_max_wait_time = timeout.to_i
		Capybara.ignore_hidden_elements = false
		Capybara.javascript_driver = :chrome # :chrome simulates behavior in browser

	end

	def capabilities_lambdatest_capybara

		#****************************************************************************************
		#****************************************************************************************
		# OBTENER CAPABILITIES DE: CSV Y LT_VARS => VARIABLES ENT0RONO
		#****************************************************************************************
		#****************************************************************************************

		return {
			"browserName"=>"Chrome",
			"version"=> '92.0',
			"platform"=> 'Windows 10',
			"resolution"=> '1024x768',
			"name"=> "TAF - Testing IT",
			"build"=> "capybara-lambdatest",
			"video"=>true,
			"network"=>true,
			"console"=>true,
			"visual"=>true
		}
	end

	def set_result_lambdatest(test_name, status_test, mjs_error=nil)
		test_name = test_name.to_s.strip
		status_test = status_test == :failed ? 'failed' : 'passed'
		mjs_error = mjs_error.to_s.strip

		#RENOMBRAR TEST
		Capybara.current_session.driver.execute_script("lambda-name=#{test_name}")

		#ESTATUS TEST
		Capybara.current_session.driver.execute_script("lambda-exceptions", [mjs_error]) if status_test.to_s.to_sym == :failed
		Capybara.current_session.driver.execute_script("lambda-status=#{status_test}")
	end

end
