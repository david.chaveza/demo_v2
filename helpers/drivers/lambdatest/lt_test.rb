require 'rubygems'
require 'selenium-webdriver'
# Input capabilities

USERNAME = 'git.client.user'
ACCESS_KEY = 'GmJuW2iJs5xFUWEVZe3Yu22ffGgi4gV6AOpXxDS6sII2wiInDP'

caps = Selenium::WebDriver::Remote::Capabilities.new
caps[:browserName] = 'Chrome'
caps[:platform] = 'Windows 10'
caps[:version] = '92.0'
caps[:resolution] = '1024x768'
caps[:name] = 'TEST < RUBY + UNIT TEST' # test name
caps[:build] = 'TEST < RUBY + UNIT TEST' # CI/CD job or build name

driver = Selenium::WebDriver.for(:remote,
                                 :url => "https://#{USERNAME}:#{ACCESS_KEY}@hub.lambdatest.com/wd/hub",
                                 :desired_capabilities => caps)

driver.navigate.to "https://lambdatest.github.io/sample-todo-app/"
driver.find_element(:name, 'li1')
driver.find_element(:name, 'li2')
driver.find_element(:id, 'sampletodotext').send_keys("Yey, Let's add it to list")
driver.find_element(:id, 'addbutton').click
enteredText = driver.find_element(:xpath, '/html/body/div/div/div/ul/li[6]/span').text
status = true if enteredText == "Yey, Let's add it to list"

#*****************************************************************************************
#*****************************************************************************************
#*****************************************************************************************
# MANEJO DE TEST (FAILS, PASS, EXCEPTION MESSAJES)
#*****************************************************************************************
#*****************************************************************************************
#*****************************************************************************************
test_name = "MI TEST DE PRUEBAS RUBY"
status = 'failed'
mjs_error = "ERROR. TEST FAILED CONTROLADO"

#RENOMBRAR TEST
driver.execute_script("lambda-name=#{test_name}")

#To mark test status to pass.
#driver.executeScript('lambda-status=passed')
driver.execute_script("lambda-status=#{status}")

#Escribir error de mensaje
driver.execute_script("lambda-exceptions", [mjs_error]);
#To mark test status to fail.
#driver.executeScript('lambda-status=failed')
driver.execute_script("lambda-status=#{status}")

driver.quit
