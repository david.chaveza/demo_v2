# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#Info: http://appium.io/docs/en/writing-running-appium/caps/
# 			http://automate-apps.com/start-appium-server-from-command-line/
# 			INSTALAR NODE JS: https://nodejs.org/en/download/
# 					npm uninstall -g appium
# 					npm install -g appium@beta
#####################################################
require_relative '../retryable'
require_relative '../kobiton/devices/devices_utils'
require_relative '../kobiton/devices/devices_utils'
require_relative '../kobiton/apps/apps_utils'

module Drivers
	include Retryable

	def appium(device_uid, device_name, platform_name, platform_version, appium_server,
						 device_orientation, app_package=nil, app_activity=nil, app=nil,
						 kobiton_session_name=nil, kobiton_group_id=nil, instancia_appium=nil)
		require 'selenium-webdriver'
		#require 'rubygems'
		require 'appium_lib'

		desired_caps = {
				:caps => {
						#****************************************************************************
						# KOBITON SESSION DATA
						#****************************************************************************
						#:sessionName => 'TESTING IT - AUTOMATION REGRESION',
						:sessionName => kobiton_session_name.to_s,
						:sessionDescription => '',
						:deviceOrientation => device_orientation.to_s.strip,
						:captureScreenshots => true,
						#:deviceGroup => 'KOBITON',
						:deviceGroup => 'ORGANIZATION',
						#:groupId => 1019, # Group: Admin
						:groupId => kobiton_group_id, # Group: Automatizacion - Group ID: 1012

						#****************************************************************************
						# DEVICE DATA
						#****************************************************************************
						# drivers disponibles: UiAutomator2, Espresso, UiAutomator
						#:automationName => 'UiAutomator2',
						:platformName => platform_name.to_s.strip,
						:platformVersion => platform_version.to_s.strip,
						:deviceName => device_name.to_s.strip,
						:udid => device_uid.to_s.strip,
						#:appPackage =>  app_package,
						#:app_wait_activity => "com.mobilityado.ado/.views.activitys.onboarding.OnBoardingActivity",
						#:appActivity => app_activity,
						#:app => 'Microsoft.WindowsCalculator_8wekyb3d8bbwe!App',
						:app => app.to_s.strip,

						# CONFIGS
						:noReset =>  true,
						:fullReset =>  false,
						:connectHardwareKeyboard => true,
						:clearSystemFiles => true,
						:adbExecTimeout => 60000,
						:newCommandTimeout => 180000
				},
				:appium_lib => {
						#:server_url => 'https://david.chavez.avila:a8b8d1ae-a394-444c-9ea2-3f615c390d80@api.kobiton.com/wd/hub'
						:server_url => appium_server.to_s.strip
				}
		}

		#driver = Selenium::WebDriver.for :remote, url: "http://127.0.0.1:4723/wd/hub", desired_capabilities: desired_caps[:caps]
		driver = Appium::Driver.new(desired_caps, true).start_driver
		puts "APPIUM SESSION = #{driver}"
		return driver
	end

	def appium_driver_quit(driver=Appium::Driver.new)
		driver.quit
		puts "APPIUM DRIVER. 'DRIVER.QUIT' SUCCESS"
	rescue
		puts "ERROR AL DETENER APPIUM DRIVER. 'DRIVER.QUIT'"
	end

	#VALIDAR SI EL DISPOSITIVO ESTA DISPONIBLE EN KOBITON
	def kb_get_app_id(app_name, os)
			return wait_for_last_app_by_name(app_name.to_s, os.to_s, 60)
	end

	#VALIDAR SI EL DISPOSITIVO ESTA DISPONIBLE EN KOBITON
	def kb_wait_for_device_ativated(instancia_appium, device_uid)
		unless instancia_appium.to_s.to_sym == :local
			wait_for_activated_state_by_udid(device_uid, 300)
		end
	end

	def launch_appium_server(server)
		require 'open3'
		require 'os'
		kill_all_appium_server
		kill_cmd_shell_appium_server

		#REINTENTAR HACER LA ACCIÓN EN CASO DE ERROR
		retryable(:tries => 2, :on => Exception, :log =>:error) do
			if OS.windows?
				cmd = "cmd.exe /c start \"TFA-APPIUM\" cmd.exe /k #{server}"
				#cmd = "start \"TFA-APPIUM\" #{server}"
				puts "cmd = #{cmd}"
				stdin, stdout, waiter = Open3.popen2(cmd)
				status = waiter.value

				unless status.success?
					raise "ERROR STATUS APPIUM. AL INICIAR APPIUM SERVER: #{cmd} Returned #{stdin}  #{stdout} :: #{status}"
				end
			end
			puts 'START APPIUM SERVER'
		end
	rescue Exception => e
		raise "ERROR AL INICIAR APPIUM SERVER: \nException => #{e.message}"
	end

	def kill_all_appium_server
		require 'open3'
		require 'os'
		if OS.windows?
			cmd = "taskkill /IM node.exe /F"
			stdin, stdout, waiter = Open3.popen2(cmd)
			status = waiter.value
			unless status.success?
				raise "FAIL => KILL APPIUM DRIVER EXE. Returned #{cmd} => #{stdin} #{stdout} #{status}"
			end
		end
		puts "STOP APPIUM SERVER"

	rescue Exception => e
		puts "APPIUM SERVER EXCEPTION => NO SE PUDO DETENER APPIUM SERVER. \nException => #{e.message}"
	end

	def kill_cmd_shell_appium_server
		require 'open3'
		require 'os'
		if OS.windows?
			cmd = 'taskkill /FI "WINDOWTITLE EQ TFA-APPIUM*" /F /T'
			stdin, stdout, waiter = Open3.popen2(cmd)
			status = waiter.value
			unless status.success?
				raise "FAIL => KILL APPIUM DRIVER EXE. Returned #{cmd} => #{stdin} #{stdout} #{status}"
			end
		end
		puts "KILL CMD/SHELL APPIUM SERVER"

	rescue Exception => e
		puts "APPIUM SERVER EXCEPTION => NO SE PUDO DETENER CMD/SHELL APPIUM SERVER. \nException => #{e.message}"
	end



end
