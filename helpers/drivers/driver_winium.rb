# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: NA
#Descripcion:  Helper con la configuración del driver
#####################################################

module Drivers

  def winium(app, url_server)
    require 'selenium-webdriver'
    driver = nil

    caps = Selenium::WebDriver::Remote::Capabilities.new
    caps['app'] = app.to_s.strip
    #caps['app'] = "#{$winium_generic_dir_app}/winium_app.bat"
    caps['platform'] = 'windows'
    caps["requireWindowFocus"] = true
    caps.javascript_enabled = true # Javascript not enabled by default, see "Gotchas" below.

    client = Selenium::WebDriver::Remote::Http::Default.new
    client.timeout = 120 # instead of the default 60

    #$winium_app = Selenium::WebDriver.for :remote, url: "http://#{$winium_host}:#{$winium_port}",
    driver = Selenium::WebDriver.for :remote, url: url_server,
                                          desired_capabilities: caps, http_client: client

    return driver
  end

  def winium_driver_quit(driver)
    driver.quit
    puts "WINIUM DRIVER. 'DRIVER.QUIT' SUCCESS"
  rescue
    puts "ERROR AL DETENER WINIUM DRIVER. 'DRIVER.QUIT'"
  end

  def launch_winium_server(winium_server)
    require 'open3'
    require 'os'
    if OS.windows?
      #cmd = "start #{winium_server}"
      cmd = "cmd.exe /c start \"TFA-WINIUM\" cmd.exe /k #{winium_server}"
      stdin, stdout, waiter = Open3.popen2(cmd)
      status = waiter.value

      unless status.success?
        raise "send_keys_vbs => Error al ejecutar VBS: 'send_keys.vbs': #{cmd} Returned #{stdin}  #{stdout} :: #{status}"
      end
    end
    puts 'WINIUM SERVER START'

  rescue Exception => e
    raise "WINIUM EXCEPTION => NO SE PUDO INICIAR WINIUM SERVER. \nException => #{e.message}"
  end

  def kill_all_winium_server
    require 'open3'
    require 'os'
    if OS.windows?
      cmd = "taskkill /IM Winium.Desktop.Driver.exe /F"
      stdin, stdout, waiter = Open3.popen2(cmd)
      status = waiter.value
      unless status.success?
        raise "FAIL => KILL WINIUM DRIVER EXE. Returned #{cmd} => #{stdin} #{stdout} #{status}"
      end
    end
    puts "STOP WINIUM SERVER"

  rescue Exception => e
    puts "WINIUM SERVER EXCEPTION => NO SE PUDO DETENER WINIUM SERVER. \nException => #{e.message}"
  end

  def kill_cmd_shell_winium_server
    require 'open3'
    require 'os'
    if OS.windows?
      cmd = 'taskkill /FI "WINDOWTITLE EQ TFA-WINIUM*" /F /T'
      stdin, stdout, waiter = Open3.popen2(cmd)
      status = waiter.value
      unless status.success?
        raise "FAIL => KILL APPIUM DRIVER EXE. Returned #{cmd} => #{stdin} #{stdout} #{status}"
      end
    end
    puts "KILL CMD/SHELL WINIUM SERVER"

  rescue Exception => e
    puts "APPIUM SERVER EXCEPTION => NO SE PUDO DETENER CMD/SHELL WINIUM SERVER. \nException => #{e.message}"
  end
  
  
end
