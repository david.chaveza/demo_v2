# Helper para conexión con: **"SPIRA TEST"**

1- Tener datos de conexión de SpiraTest y agregarlos al archivo:
    `../venture/config/csv_data/00_config/dt_spira.csv`

2- La columna `conexion_eneabled` indica si se va establecer o no 
la comunicación con Spira Test

3- Agregar en el HOOKS `After Scenario` el codigo:

`spira_controller = QAManagementTools::Spira::SpiraController.new`
`spira_controller.get_data_scenario_exec(scenario)`
`spira_controller.send_result_to_spira`