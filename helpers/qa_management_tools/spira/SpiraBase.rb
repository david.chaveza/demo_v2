module QAManagementTools
  module Spira
    class SpiraBase

      attr_accessor :tester_user_id, :project_id, :test_case_id, :assert_count, :execution_status_id, :start_date,
                    :runner_test_name, :release_id, :spira_url, :spira_password, :test_set_id, :spira_user, :end_date,
                    :runner_name, :runner_stack_trace, :runner_message
      def initialize(conexion_enabled, spira_url, spira_user, spira_password, project_id, test_set_id)
        #Conexion habilitada para comunicar con 'SPIRA' 'Habilitada = 1', 'Deshabilitada = 0'
        @conexion_enabled = conexion_enabled
        @spira_url = spira_url
        @spira_user = spira_user
        @spira_password = spira_password
        @project_id = project_id
        #Pasar '-1' para null
        @release_id = -1
        #Pasar '-1' para null
        @test_set_id = test_set_id
        #default to the userName
        @tester_user_id = -1
        @test_case_id = nil
        @start_date = nil
        @end_date = nil
        #Estatus test case. 'PASS=2', 'FAIL=1'
        @execution_status_id = nil
        @runner_name = 'TESTER ACADEMY::Framework'
        @runner_test_name = nil
        #'Default=0'
        @assert_count = 0
        #Mensaje de la prueba 'mensaje exitoso / mensaje fallido'
        @runner_message = nil
        #Mensaje descriptivo 'Ej. enviar la exception'
        @runner_stack_trace = nil
      end


    end
  end
end

