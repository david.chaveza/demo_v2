require_relative 'SpiraBase'
require_relative 'SpiraTestExecute'
require_relative '../../../helpers/data_driven/csv_data'

module QAManagementTools
  module Spira
    class SpiraController < QAManagementTools::Spira::SpiraBase

      def initialize
        #Obtener data de conexión a 'SPIRA' desde CSV DATA
        #   '../venture/config/csv_data/00_config/dt_spira.csv'
        @data = get_csv_data('00_config/dt_spira')[1]
        super(
            @data[:conexion_enabled].to_s.to_i,
            @data[:url].to_s.strip,
            @data[:user].to_s.strip,
            @data[:password].to_s.strip,
            @data[:project_id].to_s.to_i,
            @data[:test_set_id].to_s.to_i
        )
      end


      #********************************************************************************************
      # CUCUMBER DATA SCENARIO
      #********************************************************************************************

      #Obtener los datos de el 'TEST (ESCENARIO)' de cucumber ejecutado
      # @params
      #   :scenario Objeto del 'HOOKS'
      def get_data_scenario_exec(scenario)
        if @conexion_enabled == 1
          tags = scenario.source_tag_names
          @test_case_id = get_test_id_from_scenario_tags(tags)
          unless @conexion_enabled.nil?
            @start_date = DateTime.now.to_s
            @execution_status_id = scenario.status == :passed ? 2 : 1
            @runner_test_name = scenario.name
            @runner_message = scenario.status == :passed ? "PASSED TEST: '#{@runner_test_name}'"
                                  : "FAILED TEST: '#{@runner_test_name}'"
            @runner_stack_trace = scenario.exception.to_s
            @end_date = DateTime.now.to_s
          end
        end

      rescue Exception => e
        puts "ERROR AL OBTENER DATOS DEL TEST (ESCENARIO CUCUMBER). \nEXCEPTION => #{e.message}"
      end

      def get_test_id_from_scenario_tags(tags)
        id_test = nil
        tags.each do |tag|
          if tag.to_s.include?('@TC_')
            id_test = tag.to_s.gsub('@TC_', '')
            break
          end
        end
        id_test
      end

      #********************************************************************************************
      # SEND RESULT TO SPIRA 'SpiraTest'
      #********************************************************************************************
      def send_result_to_spira
        res = nil
        if @conexion_enabled == 1 and !@test_case_id.nil?
          spira_test_execute = Test::Unit::SpiraTest::SpiraTestExecute.new(
              @spira_url, @spira_user, @spira_password, @project_id
          )
          res = spira_test_execute.recordTestRun(@tester_user_id, @test_case_id, @release_id, @test_set_id,
                                           @start_date, @end_date, @execution_status_id, @runner_name, @runner_test_name,
                                           @assert_count, @runner_message, @runner_stack_trace
          )
        end
        return res
      rescue Exception => e
        puts "ERROR AL COMUNICAR CON SPIRA. \nEXCEPTION => #{e.message}"
      end


    end
  end
end


#The following line runs the tests using the SpiraTest runner
url = "https://testing-it.spiraservice.net"
usuario = "automationQA"
password = "test1234"
project_id = 67
release_id = -1
test_set_id = 981
test_case_id = 7681


