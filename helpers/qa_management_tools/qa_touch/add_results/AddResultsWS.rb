# encoding: utf-8
require "uri"
require 'net/http'
require 'openssl'
require 'json'
require_relative '../config_qa_touch'

module QAManagementTools
  module QATouch
    class AddResultsWS

      #############################################################################
      # WEBSERVICE DATA
      #############################################################################
      def initialize
        @current_path = File.expand_path(File.dirname(__FILE__))

        @url_servicio = QAManagementTools::QATouch::ConfigQaTouch.url_add_result
        @api_token = QAManagementTools::QATouch::ConfigQaTouch.api_token
        @domain = QAManagementTools::QATouch::ConfigQaTouch.domain
        @project_key = QAManagementTools::QATouch::ConfigQaTouch.project_key
        @test_run_key = QAManagementTools::QATouch::ConfigQaTouch.test_run_key
        @test_name = QAManagementTools::QATouch::ConfigQaTouch.test_name
        @test_status = QAManagementTools::QATouch::ConfigQaTouch.test_status
        @run_results = QAManagementTools::QATouch::ConfigQaTouch.run_results
        @fail_message = QAManagementTools::QATouch::ConfigQaTouch.fail_message
        @file = QAManagementTools::QATouch::ConfigQaTouch.file

        @request_ssl = true
        @request_timeout = 120
        @request_headers = [
            {:header => 'api-token', :valor => @api_token},
            {:header => 'domain', :valor => @domain},
            {:header => 'User-Agent', :valor => 'TESTER ACADEMY-TAF-R'},
            {:header => 'Accept', :valor => '*/*'}
        ]
        @response = nil
        @response_codigo_esperado = 200
        @assert_response_body = '"success":true'
      end


      #############################################################################
      # ACCIONES
      #############################################################################

      #Método que ejecuta el request
      # @params
      # @return
      def request
        #obtener query params
        params = "?status=#{@test_status}&project=#{@project_key}&test_run=#{@test_run_key}&run_result[]=#{@run_results}&comments=#{@fail_message}"

        url_servicio = "#{@url_servicio}#{params}"
        #url_servicio = "#{@url_servicio}"
        #encode para caracteres
        url_servicio = URI.escape(url_servicio)
        #encode para salto de linea
        # url_servicio = url_servicio.to_s.gsub("%5Cn", '%0A')

        # 1 - URI, SSL, TimeOut
        url = URI(url_servicio)

        https = Net::HTTP.new(url.host, url.port);
        https.use_ssl = @request_ssl
        https.read_timeout = @request_timeout

        # 2 - Métodos HTTP
        request = nil
        request = Net::HTTP::Post.new(url)

        # POST < Agregar Parametros (OTRA FORMA DE MANDAR PARAMETROS)
        # request.set_form_data('status' => @test_status,
        #                       'project' => @project_key,
        #                       'test_run' => @test_run_key,
        #                       'run_result[]' => @run_results
        # )


        # 3 - Agregar Headers
        for i in 0..(@request_headers.size - 1)
          request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
        end

        # 4 REQUEST BODY > AGREGAR MULTIPART-FORM DATA
        if @file.nil? or @file.to_s.empty?
          puts "Warning!!! No se encontro Archivo de Evidencias para subir a QA-Touch... Archivo de evidencia: '#{@file}'"
        else
          form_data = [['file[]', File.open("#{@file}")]] # or File.open() in case of local file
          request.set_form form_data, 'multipart/form-data'
        end

        # 5 - Obtener HTTP Response
        @response = https.request(request)

        return @response

      rescue Exception => e
        raise("Error al ejecutar el Request a la URL => #{@url_servicio}. \nException => #{e.message}")

      end


      #############################################################################
      # ASSERTS/VERIFYS/VALIDACIONES
      #############################################################################

      def assert_response_code
        unless @response.code.to_s.to_i == @response_codigo_esperado
          raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
        end
      end

      def assert_response_body
        unless @response.body.to_s.include?(@assert_response_body)
          raise("Error. Response Body debe contener => #{@assert_response_body} Response Body obtenido #{@response.body.to_s[0..500]}")
        end
      end



    end


  end
end


