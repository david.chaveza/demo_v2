require_relative 'AddResultsController'

#1. EN AddResultsController.add_results_to_qa_touch COMENTAR LINEA:
#   #QAManagementTools::QATouch::ConfigQaTouch.get_data_scenario_exec(scenario)


#2. LLENAR DATOS DEL ESCENARIO AL OBJETO ConfigQaTouch
QAManagementTools::QATouch::ConfigQaTouch.run_results = "VGDMWP"
QAManagementTools::QATouch::ConfigQaTouch.test_name = "TEST DE PRUEBAS"
QAManagementTools::QATouch::ConfigQaTouch.test_status = "Failed"
QAManagementTools::QATouch::ConfigQaTouch.fail_message = "ERROR DUMMY TEST FROM TAF-R"


#3. PRINT DATA DEL OBJETO ConfigQaTouch
QAManagementTools::QATouch::ConfigQaTouch.to_s


#4. SEND REQUEST CON QAManagementTools::QATouch::AddResultsController
qa_touch_controller = QAManagementTools::QATouch::AddResultsController.new
qa_touch_controller.add_results_to_qa_touch("TEST", "C:/ambiente/evidencia/01_conf_metodo_pago.zip")