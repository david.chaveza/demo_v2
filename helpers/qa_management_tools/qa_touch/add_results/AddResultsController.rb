require_relative 'AddResultsWS'
require_relative '../../../ZipFileGenerator'

module QAManagementTools
  module QATouch
    class AddResultsController

      #********************************************************************************************
      # SEND RESULT TO SPIRA 'SpiraTest'
      #********************************************************************************************

      #Permite agregar resultados a QA-Touch para un test case
      # @params
      #   :scenario Objeto que viene desde Cucumber Hooks define la data de un scenario cucumber
      #   :evidence String ruta donde se encuentra la evidencia a adjuntar en QA-Touch
      #   :is_directory Boolean true si la evidencia a subir es un directorio que se va comprimir en ZIP. False si es un archivo unico
      # @return
      #   True si la URL abierta es la esperada, Raise si la URL no es la correcta
      def add_results_to_qa_touch(scenario, evidence=nil, is_directory=nil)
        res = nil
        enable = QAManagementTools::QATouch::ConfigQaTouch.enabled.to_s.strip.to_i

        if enable == 1
          #setear scenario data
          QAManagementTools::QATouch::ConfigQaTouch.get_data_scenario_exec(scenario)

          unless QAManagementTools::QATouch::ConfigQaTouch.run_results.nil? or QAManagementTools::QATouch::ConfigQaTouch.run_results.to_s.empty?
            #setear path y nombre de la evidencia a subir
            QAManagementTools::QATouch::ConfigQaTouch.file = get_file_evidence(evidence)
            puts "Warning!!! No se encontro archivo de evidencia para agregar al restultado de QA-Touch." if QAManagementTools::QATouch::ConfigQaTouch.file.nil?
            #zip evidence solo si es directorio y el file no es nil
            unless QAManagementTools::QATouch::ConfigQaTouch.file.nil?
              QAManagementTools::QATouch::ConfigQaTouch.file = zip_evidence(evidence) if is_directory
            end

            #send request to qa-touch
            add_results_ws = QAManagementTools::QATouch::AddResultsWS.new
            resp = add_results_ws.request
            add_results_ws.assert_response_code
            add_results_ws.assert_response_body
            puts "TAF-R conexion exitosa con QA-Touch. Test data: #{QAManagementTools::QATouch::ConfigQaTouch.to_s}"
          end
        end

      rescue Exception => e
        puts "ERROR AL COMUNICAR CON QA-TOUCH.\nTest data: #{QAManagementTools::QATouch::ConfigQaTouch.to_s}\nEXCEPTION => #{e.message}"
      end

      #Valida si la evidencia existe de lo contrario retorna null
      # @params
      #   :evidence String ruta donde se encuentra la evidencia a adjuntar en QA-Touch
      # @return String ruta y nombre de la evidencia si no existe el archivo o directorio retorna nil
      private
      def get_file_evidence(evidence=nil)
        evidence = nil if evidence.nil?
        evidence = nil unless File.directory?(evidence)
        evidence = nil unless File.exists?(evidence)
        return evidence
      rescue Exception => e
        puts "Warning!!! No se encontro archivo de evidencia para agregar al restultado de QA-Touch. Archivo Evidence: #{evidence}.\nException: #{e.message}"
        return nil
      end

      #Comprime un directorio de evidencia en archvo .ZIP
      # @params
      #   :evidence String ruta donde se encuentra la evidencia a adjuntar en QA-Touch. Debe ser un directorio
      # @return String ruta y nombre de la evidencia ZIP
      private
      def zip_evidence(evidence)
        zip_name = "#{evidence}/evidence.zip"
        unless QAManagementTools::QATouch::ConfigQaTouch.file.nil?
          zip = ZipFileGenerator.new(evidence, zip_name)
          zip.write
        end
        return zip_name
      end

    end
  end
end



