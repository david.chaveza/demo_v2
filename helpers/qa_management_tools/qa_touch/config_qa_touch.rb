# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper de obtener configuracion para establecer conexión con QA-Touch
#   Los valores del setup se toman:
#     1er se intenta tomar de las variables de entorno
#     2do se toman del archivo $PROJECT/helpsers/qa_management_tools/qa_touch/config.yml
#####################################################

require 'yaml'
require 'tmpdir'
require_relative '../../data_driven/yml_data.rb'


module QAManagementTools
  module QATouch

    class ConfigQaTouch

      #********************************************************************************************************
      #********************************************************************************************************
      # VARIABLES ENTORNO
      #********************************************************************************************************
      #********************************************************************************************************
      @config = get_yml_data('config.yml', File.expand_path(File.dirname(__FILE__)))

      #Determina si la conexión con QA-Touch estara habilitada
      @enabled = ENV['TAF_QATOCUH_ENABLED'].nil? ? @config['enabled'].to_s.strip.downcase : ENV['TAF_QATOCUH_ENABLED'].to_s.strip.downcase

      #URL Para agregar resultados a QA-Touch
      @url_add_result = ENV['TAF_QATOCUH_URL_ADD_RESULTS'].nil? ? @config['url_add_result'].to_s.strip : ENV['TAF_QATOCUH_URL_ADD_RESULTS'].to_s.strip

      #API TOKEN. Obtener de QA-Touch
      @api_token = ENV['TAF_QATOCUH_API_TOKEN'].nil? ? @config['api_token'].to_s.strip : ENV['TAF_QATOCUH_API_TOKEN'].to_s.strip

      #HEADER DOMAIN. Dominio de la organización
      @domain = ENV['TAF_QATOCUH_DOMAIN'].nil? ? @config['domain'].to_s.strip : ENV['TAF_QATOCUH_DOMAIN'].to_s.strip

      #PROJECT KEY. Obtener de QA-Touch
      @project_key = ENV['TAF_QATOCUH_PROJECT_KEY'].nil? ? @config['project_key'].to_s.strip : ENV['TAF_QATOCUH_PROJECT_KEY'].to_s.strip

      #TEST RUN KEY. Obtener de QA-Touch
      @test_run_key = ENV['TAF_QATOCUH_TEST_RUN_KEY'].nil? ? @config['test_run_key'].to_s.strip : ENV['TAF_QATOCUH_TEST_RUN_KEY'].to_s.strip

      #Nombre del escenario o test name. Se obtendra de los hooks del objeto scenario
      @test_name = nil

      #Nombre del escenario o test name. Se obtendra de los hooks del objeto scenario
      @test_status = nil

      #RUN RESULTS. ID Resultado personal de cada test case en QA-Touch.
      #     - Estos se obtendran de los hooks en los tag de los scenarios de cucumber
      #     - El tag siempre debera llevar el prefijo @TC_
      @run_results = nil

      @fail_message = nil

      #Path y nombre del File A SUBIR A QA-Touch. Este se obtendra desde Generics < Evidence
      @file = nil


      #********************************************************************************************************
      #********************************************************************************************************
      # CLASS SELF
      #********************************************************************************************************
      #********************************************************************************************************
      class << self
        #Accessors
        attr_accessor :config, :enabled, :url_add_result, :api_token, :domain,
                      :project_key, :test_run_key, :test_name, :test_status, :run_results,
                      :file, :fail_message


        #********************************************************************************************************
        #********************************************************************************************************
        # METODOS
        #********************************************************************************************************
        #********************************************************************************************************
        def to_s
          "
             enabled: #{@enabled},
             url_add_result: #{@url_add_result},
             api_token: #{@api_token},
             domain: #{@domain},
             project_key: #{@project_key},
             test_run_key: #{@test_run_key},
             test_name: #{@test_name},
             test_status: #{@test_status},
             run_results: #{@run_results},
             file: #{@file},
             fail_message: #{@fail_message}
          "
        end

        #********************************************************************************************
        # CUCUMBER DATA SCENARIO
        #********************************************************************************************
        #Obtener los datos de el 'TEST (ESCENARIO)' de cucumber ejecutado
        # @params
        #   :scenario Objeto del 'HOOKS'
        def get_data_scenario_exec(scenario)
          tags = scenario.source_tag_names
          @test_name = scenario.name
          @test_status = scenario.status == :passed ? "Passed" : "Failed"
          @run_results = get_run_result_from_scenario_tags(tags)
          @fail_message = scenario.status == :passed ? "TAF-R PASSED TEST: '#{@test_name}'"
                            : "TAF-R FAILED TEST: '#{@test_name}'. Exception: '#{scenario.exception.to_s}'"

        rescue Exception => e
          @test_name = ""
          @test_status = ""
          @run_results = ""
          @fail_message = ""
          puts "ERROR AL OBTENER DATOS DEL TEST (ESCENARIO CUCUMBER). \nEXCEPTION => #{e.message}"
        end


        #get run results para conexion con QA-Touch
        #   El tag siempre debera llevar el prefijo @TC_
        def get_run_result_from_scenario_tags(tags)
          id_test = nil
          tags.each do |tag|
            if tag.to_s.include?('@TC_')
              id_test = tag.to_s.gsub('@TC_', '')
              break
            end
          end
          id_test
        end


      end


    end
  end
end













