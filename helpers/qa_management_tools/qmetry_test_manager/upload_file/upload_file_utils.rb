# encoding: utf-8
require 'jsonpath'
require_relative 'UploadFileWS'
require_relative '../eval_data'
require_relative '../../../data_driven/yml_data'


def qmetry_upload_file(url, path_file_results)
  current_path = File.expand_path(File.dirname(__FILE__))
  config = get_yml_data('../config.yml', current_path)
  res = false

  #Si la configuracion esta habilitada (1), se ejecuta los mensajes a telegram
  if config['enabled'].to_s.strip == '1'
    ws = Qmetry::UploadFileWS.new

    begin
      puts "Qmetry: Subiendo Archivo de Resultados."
      response = ws.request(url, path_file_results)
      ws.assert_response_code
      res = true
      puts "Success. Se subio correctamente el archivo"
    rescue Exception => e
      puts "Error. Al ejecutar '/rest/api/automation/importresult'. \nException => #{e.message}"
    end
  end

  raise "Error. Al subir archivo de resultados a Qmetry" unless res
  return res
end

