require "uri"
require 'net/http'
require 'openssl'
require 'json'


@url_servicio = "https://qtm4j-cloud-prod-4.s3.amazonaws.com/automation-uploads/1648146213806_bd673f0e-748a-4487-adc8-6aef269c2900.json?X-Amz-Security-Token=IQoJb3JpZ2luX2VjENv%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJGMEQCIFgyeSocPsEuyGqctQ9lRfByT48QLZprnif27y1turGeAiAmqvAFCNB6IHJ9eyLy8PIHumSb0niWI3y6Hlp%2F%2FZONyir6AwhjEAQaDDQxNTU1MTIyNTgwNSIMBxKxY%2Baxwx%2FycuZyKtcDkqpH%2FTAyCqy41RyYXrhKyxHpglSP5hIzn6p5%2FJ5R4h9KWkae6Bd3Ic46S2IwnmtuCGPDnEnRlxYqdDD9m9IobIxP7blouI2jNygOX6MwSQydW1MiHS%2BG0Wx%2Fcdfx5hZMFCRgcRbj81Hpy3gm7MP2ZDiZohQmCBQ%2FHNdnRq1t0mbxiGLrCegFRA8IGDvebFjtyRCCoh58qpBmycQCeTJg410EFTekDxdvK0cnjt7jCsmmpZWkaBPQCS0ak4x5B6l%2FJyaVdDp%2BW7kJJuyOfdR9Tm51npzo3%2FyiGwAIBeaLSitK1t0tL5%2FjqvAq%2B%2B66SpwZgVVVLAFuI0Y8MFBZYYpK4SqgS92kc9BbfFOO7VLeBeNcEexun28AsmX20Q7uSRpULgZce6s6AyjUlWsUTrcSUwoEBELwJU7dtrc0ee%2B5rFSvH2yQgslWyUHRuyEcZzu2pRFu0ZkfRYccHDF9fGfvuDXxcqpIeq4NUwoPw%2Brg7M0VM4iXQXsobYpzfJXWm3iWhOw%2Frf3ZswRhEgrLY6tbO92%2FuPDa0e3KnMAK%2FXlHkRr0BUILKuW%2FDtZXnuaNdy8PNLalyvUch0%2Fzzs1UcBKUjoogehwY0zEWT8WBqmxzM2a48QJk2wV6MJPp8pEGOqYBPFzdlX7T6Z%2BLaMMggFXldBQA0Ad1MVjuvdPshiw8ZIbjXkwSzbVuP4EP3JUux6C6H9WlLEnJA0hn7mmi%2Brts0CprID%2BRHid4yHiR1xKc3L7suRK8Q%2FdXGkvW9NL0eW4Mh7wJ88KBC2FA4b9SwzDcY5wNwk1l2PpKVFqsQTGgFiwwnSMR8C4TlWEtYzsrkvTJfyD8ujFQ2hWOTlMWW7fv2%2BlSfNwbHg%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220324T182333Z&X-Amz-SignedHeaders=content-type%3Bhost&X-Amz-Expires=299&X-Amz-Credential=ASIAWBQGIP7GYNVHL5T4%2F20220324%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=25a61e83e2d41cbb2de5da57f2ff59d8be27868527986a401c231fc42b1740a1"
@resp = nil
@api_key = 'bdd63642304abcf424b509d219766c376b80c9f3ca9af006e4fe45eaff0c25a4b1138ebe98507967c5c80040b23df21eb193900e05753a77690ce2841b49773f3d1018c6e3b7ca866590140ff20e9432'


# 1 - URI, SSL, TimeOut
uri = URI(@url_servicio)

https = Net::HTTP.new(uri.host, uri.port);
https.use_ssl = true
https.read_timeout = 120


# 2 - Métodos HTTP
request = nil
request = Net::HTTP::Put.new(uri)


# 3 - Agregar Headers
request['apiKey'] = @api_key
request['Content-Type'] = 'multipart/form-data'


# 4 REQUEST BODY
#form_data = [['src', File.open("C:/ambiente/results.json")]] # or File.open() in case of local file
#request.set_form form_data, 'multipart/form-data'
#request.body = 'src: "/C:/ambiente/results.json"'
file = File.open("C:/ambiente/results.json")
request.body = file.read
file.close


# 5 - Obtener HTTP Response
# response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http| # pay attention to use_ssl if you need it
#   @resp = http.request(request)
# end
@resp = https.request(request)


puts @resp