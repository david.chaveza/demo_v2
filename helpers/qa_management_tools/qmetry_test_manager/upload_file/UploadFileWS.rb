# encoding: utf-8
require "uri"
require 'net/http'
require 'openssl'
require 'json'
require_relative '../../../data_driven/yml_data'

module Qmetry
  class UploadFileWS

    #############################################################################
    # WEBSERVICE DATA
    #############################################################################
    def initialize
      @current_path = File.expand_path(File.dirname(__FILE__))
      @config = get_yml_data('../config.yml', @current_path)
      @request_ssl = true
      @request_timeout = 120
      @api_key = "#{@config['api_key']}"
      @request_headers = [
        {:header => 'apiKey', :valor => @api_key},
        {:header => 'Content-Type', :valor => 'multipart/form-data'}
      ]
      @response = nil
      @response_codigo_esperado = 200
    end


    #############################################################################
    # ACCIONES
    #############################################################################

    #Método que ejecuta el request
    # @params
    #   :url String URL del servicio. Esta se genera dinamicamente desde el WS ImportResults
    #   :path_results_file Directorio + archivo de resultados CUCUMBER JSON
    # @return
    #   :Response Response
    def request(url, path_results_file)
      # 1 - URI, SSL, TimeOut
      url = URI(url)

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = @request_ssl
      https.read_timeout = @request_timeout

      # 2 - Métodos HTTP
      request = nil
      request = Net::HTTP::Put.new(url)

      # 3 - Agregar Headers
      for i in 0..(@request_headers.size - 1)
        request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
      end

      # 4 REQUEST BODY
      file = File.open(path_results_file)
      request.body = file.read
      file.close

      # 5 - Obtener HTTP Response
      @response = https.request(request)

      return @response

    rescue Exception => e
      raise("Error al ejecutar el Request a la URL => #{url}. \nException => #{e.message}")

    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_response_code
      unless @response.code.to_s.to_i == @response_codigo_esperado
        raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
      end
    end


  end

end


