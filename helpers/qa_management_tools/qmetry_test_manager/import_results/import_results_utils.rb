# encoding: utf-8
require 'jsonpath'
require_relative 'ImportResultsWS'
require_relative '../eval_data'

def qmetry_import_results(test_cycle)
  current_path = File.expand_path(File.dirname(__FILE__))
  config = get_yml_data('../config.yml', current_path)
  url_upload_file = nil

  #Si la configuracion esta habilitada (1), se ejecuta los mensajes a telegram
  if config['enabled'].to_s.strip == '1'
    ws = Qmetry::ImportResultsWS.new
    json_expr = "$.url"

    begin
      puts "Qmetry: Importando Resultados."
      response = ws.request
      ws.assert_response_code
      result = get_json_path_value(response.read_body, json_expr)
      raise "Error. Al ejecutar '/rest/api/automation/importresult'" if result.size == 0
      url_upload_file = result[0].to_s
    rescue Exception => e
      puts "Error. Al ejecutar '/rest/api/automation/importresult'. \nException => #{e.message}"
    end
  end

  return url_upload_file
end
