# encoding: utf-8
require "uri"
require 'net/http'
require 'openssl'
require 'json'
require_relative '../../../data_driven/yml_data'

module Qmetry
  class ImportResultsWS

    #############################################################################
    # WEBSERVICE DATA
    #############################################################################
    def initialize(test_cycle)
      @current_path = File.expand_path(File.dirname(__FILE__))
      @config = get_yml_data('../config.yml', @current_path)
      @url_servicio = "#{@config['url_import']}"
      @request_ssl = true
      @request_timeout = 120
      @api_key = "#{@config['api_key']}"
      #@test_cycle = "#{@config['test_cycle']}"
      @test_cycle = test_cycle
      @user_account_id = "#{@config['user_account_id']}"
      @request_headers = [
          {:header => 'apiKey', :valor => @api_key},
          {:header => 'Content-Type', :valor => 'application/json'}
      ]
      @req_body_file = File.read("#{@current_path}/import_results.json")
      @response = nil
      @response_codigo_esperado = 200
    end


    #############################################################################
    # ACCIONES
    #############################################################################

    #Método que ejecuta el request
    # @params
    # @return
    #   :String Response
    def request
      url_servicio = "#{@url_servicio}"
      #encode para caracteres
      url_servicio = URI.escape(url_servicio)
      #encode para salto de linea
      url_servicio = url_servicio.to_s.gsub("%5Cn", '%0A')

      # 1 - URI, SSL, TimeOut
      url = URI(url_servicio)

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = @request_ssl
      https.read_timeout = @request_timeout

      # 2 - Métodos HTTP
      request = nil
      request = Net::HTTP::Post.new(url)
      #request.basic_auth(@auth_user, @auth_password)

      # 3 - Agregar Headers
      for i in 0..(@request_headers.size - 1)
        request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
      end

      # 4 REQUEST BODY
      @req_body_file["${TEST_CYCLE}"] = @test_cycle
      for i in 0..(@req_body_file.to_s.scan("${USER_ACCOUNT_ID}").size - 1)
        @req_body_file["${USER_ACCOUNT_ID}"] = @user_account_id
      end
      request.body = @req_body_file

      # 5 - Obtener HTTP Response
      @response = https.request(request)

      return @response

    rescue Exception => e
      raise("Error al ejecutar el Request a la URL => #{url}. \nException => #{e.message}")

    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_response_code
      unless @response.code.to_s.to_i == @response_codigo_esperado
        raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
      end
    end



  end

end


