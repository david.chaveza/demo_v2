# Helper para conexión con: **"QMETRY TEST MANAGER"**

1- Crear un API KEY en Qmetry y agregarla a '${PROYECT}/helpers/qa_management _tools/qmetry_test_manager/config.yml'. Ejemplo: 
    `bdd63642304abcf424b509d219766c376b80c9f3ca9af006e4fe45eaff0c25a4b1138ebe98507967c5c80040b23df21eb193900e05753a77690ce2841b49773f3d1018c6e3b7ca866590140ff20e9432`

2- En '${PROYECT}/helpers/qa_management _tools/qmetry_test_manager/config.yml' `conexion_eneabled` indica si se va establecer o no 
la comunicación con Qmetry "1 => habilitado", "0 => deshabilitado"

3- Agregar en el HOOKS `at_exit do` (Despues de terminar la suite de escenarios) el codigo:

`url_qmetry_upload_file = qmetry_import_results`

`puts "QMETRY => URL UPLOAD FILE => #{url_qmetry_upload_file}"`

`puts "QMETRY => FILE CUCUBMER RESULTS: #{Config.path_results}/results.json"`

`qmetry_upload_file(url_qmetry_upload_file, "#{Config.path_results}/results.json")`


**NOTAS: Se incluye coleccion de Postman de ejemplo QMETRY.postman_collection

