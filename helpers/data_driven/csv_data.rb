# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper para obtener los datos del csv
#####################################################
require 'fileutils'
require 'csv'


#####################################################
# READ CSV DATA
#####################################################

#Metodo obtener la data de un csv en hash map
# @params:
#   :csv_data String nombre del archivo csv que se va cargar
#   :path_file String directorio del archivo,
#       si este no se especifica, se toma por defautl "$PROYECT/venture/config/csv_data"
def get_csv_data(csv_data, path_file=nil)
  csv_file_name = nil
  csv_file = nil

  #valida si el archivo contiene extención, si no la tiene agrega .csv
  csv_file_name = csv_data.to_s.include?('.csv') ? csv_data : "#{csv_data}.csv"
  csv_file = path_file.nil? ? File.join(File.dirname(__FILE__), "../../venture/config/csv_data/#{csv_file_name}")
                            : "#{path_file}/#{csv_file_name}"
  #puts "CSV FILE => #{csv_file}"

  csv_arr = CSV.read( csv_file,  {:headers => true, :header_converters => :symbol, :encoding => 'UTF-8'} )
  keys = csv_arr.headers.to_a
  # read attribute example => csv[index][:column1]
  return csv_arr.to_a.map {|row| Hash[ keys.zip(row) ] }
end


#####################################################
# WRITE CSV DATA
#####################################################

#Metodo obtener la data de un csv en hash map
# @params:
#   :data Array de Hash con los registros a inseter del CSV. La 1er fila seran los encabezados (NOMBRES DE COLUMNAS)
#   :csv_file_name String nombre del archivo csv que se va crear o sobreescribir
#   :path_file String directorio del archivo,
#       si este no se especifica, se toma por defautl "$PROYECT/venture/config/csv_data"
def export_hash_to_csv(data, csv_file_name, path_file=nil)
  csv_file = nil

  #valida si el archivo contiene extención, si no la tiene agrega .csv
  csv_file_name = csv_file_name.to_s.include?('.csv') ? csv_file_name : "#{csv_file_name}.csv"
  csv_file = path_file.nil? ? File.join(File.dirname(__FILE__), "../../venture/config/csv_data/#{csv_file_name}")
                 : "#{path_file}/#{csv_file_name}"
  puts "CSV FILE => #{csv_file}"


  CSV.open(csv_file, "w", headers: data.first.keys) do |csv|
    data.each do |h|
      csv << h.values
    end
  end

end




#####################################################
# FILTER CSV DATA (HASH MAP FILTER)
#####################################################

# Permite filtrar un hash map proveniente de csv data
# @params
#   :dt_filters String, filtros a aplicar al datapool csv. los filtros a columnas se delimitan por ',' (coma)
#       - operadores: '=, !=, >, >=, <, <=, contains'
#       - ejemplo: 'columna1 = valor, columna2 != valor, columna3 > valor, columna4 <= valor, columna5 contains valor'
#   :csv ARRAY DE HASH, cada hash es 1 registro del CSV
def get_data_by_filters(filters, csv)

  filters_a = filters.to_s.split(',')
  csv_tmp  = Array.new
  csv_tmp = csv

  for i in 0..(filters_a.size - 1)

    filter = filters_a[i].to_s.downcase.strip
    filter_data = get_filter_data filter
    #se limipia el array
    data_filtered = Array.new

    csv_tmp.each_with_index do |record, index|

      #agregar headeres del csv
      if index == 0
        data_filtered.push(record)
      end

      case filter_data[:operador].to_s.strip
        when '='
          if record[filter_data[:key].to_s.to_sym].to_s.downcase.strip == filter_data[:value].to_s.downcase.strip
            data_filtered.push(record)
          end
        when '!='
          if record[filter_data[:key].to_s.to_sym].to_s.downcase.strip != filter_data[:value].to_s.downcase.strip
            data_filtered.push(record)
          end
        when '>'
          if record[filter_data[:key].to_s.to_sym].to_s.to_f > filter_data[:value].to_s.to_f
            data_filtered.push(record)
          end
        when '>='
          if record[filter_data[:key].to_s.to_sym].to_s.to_f >= filter_data[:value].to_s.to_f
            data_filtered.push(record)
          end
        when '<'
          if record[filter_data[:key].to_s.to_sym].to_s.to_f < filter_data[:value].to_s.to_f
            data_filtered.push(record)
          end
        when '<='
          if record[filter_data[:key].to_s.to_sym].to_s.to_f <= filter_data[:value].to_s.to_f
            data_filtered.push(record)
          end
        when 'contains'
          if record[filter_data[:key].to_s.to_sym].to_s.downcase.include? filter_data[:value].to_s.downcase
            data_filtered.push(record)
          end
      end

    end

    #Se agregan los datos del 1er filtro a 'csv_tmp' para ir reduciendo los registros filtrados
    csv_tmp = data_filtered

  end

  return data_filtered

end

# Permite obtener la key(nombre columna), valor de columna para el filtro, y operador utilizado
# @params
#   :filter String, cada filtro en especifico, enviado al método 'dt_filters'
#   :csv ARRAY DE HASH, cada hash es 1 registro del CSV
def get_filter_data(filter)

  filter_a = Array.new
  filter_data = Hash.new

  if filter.include? '=' and !filter.include? '!='
    filter_a = filter.split('=')
    operador = '='
  elsif filter.include? '!='
    filter_a = filter.split('!=')
    operador = '!='
  elsif filter.include? '>'
    filter_a = filter.split('>')
    operador = '>'
  elsif filter.include? '>='
    filter_a = filter.split('>=')
    operador = '>='
  elsif filter.include? '<'
    filter_a = filter.split('<')
    operador = '<'
  elsif filter.include? '<='
    filter_a = filter.split('<=')
    operador = '<='
  elsif filter.include? 'contains'
    filter_a = filter.split('contains')
    operador = 'contains'
  end

  filter_data = {:key => filter_a[0].to_s.strip, :value => filter_a[1].to_s.strip, :operador => operador}

end




