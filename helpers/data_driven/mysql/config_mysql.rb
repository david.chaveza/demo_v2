# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper de obtener configuracion para establecer conexión con QA-Touch
#   Los valores del setup se toman:
#     1er se intenta tomar de las variables de entorno
#     2do se toman del archivo $PROJECT/helpsers/qa_management_tools/qa_touch/config.yml
#####################################################

require 'yaml'
require 'tmpdir'
require_relative '../../data_driven/yml_data.rb'


module DataDriven
  module MySql

    class ConfigMySql

      #********************************************************************************************************
      #********************************************************************************************************
      # VARIABLES ENTORNO
      #********************************************************************************************************
      #********************************************************************************************************
      @config = get_yml_data('config.yml', File.expand_path(File.dirname(__FILE__)))

      #Mysql Host
      @host = ENV['TAF_MYSQL_HOST'].nil? ? @config['host'].to_s.strip : ENV['TAF_MYSQL_HOST'].to_s.strip

      #Mysql Port
      @port = ENV['TAF_MYSQL_PORT'].nil? ? @config['port'].to_s.strip.downcase : ENV['TAF_MYSQL_PORT'].to_s.strip.downcase

      #Mysql data base name
      @db_name = ENV['TAF_MYSQL_DB_NAME'].nil? ? @config['db_name'].to_s.strip : ENV['TAF_MYSQL_DB_NAME'].to_s.strip

      #Mysql data base user
      @db_user = ENV['TAF_MYSQL_DB_USER'].nil? ? @config['db_user'].to_s.strip : ENV['TAF_MYSQL_DB_USER'].to_s.strip

      #Mysql data base password
      @db_password = ENV['TAF_MYSQL_DB_PASSWORD'].nil? ? @config['db_password'].to_s.strip : ENV['TAF_MYSQL_DB_PASSWORD'].to_s.strip

      @connection = nil



      #********************************************************************************************************
      #********************************************************************************************************
      # CLASS SELF
      #********************************************************************************************************
      #********************************************************************************************************
      class << self
        #Accessors
        attr_accessor :config, :host, :port, :db_name, :db_user, :db_password, :connection

        def to_s
          ":config = #{config}, :host = #{host}, :port = #{port}, :db_name = #{db_name}, :db_user = #{db_user}, :db_password = #{db_password}, :connection = #{connection}"
        end
      end


    end
  end
end













