# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper para realizar operaciones en DB MySQL
#####################################################
require 'mysql'
require_relative 'config_mysql'

#####################################################
# CREATE CONNECTION
#####################################################

#Connection to Mysql
# @params:
#   :host String data base host
#   :port String data base port
#   :db_name String data base name
#   :db_user String data base user
#   :db_password String data base password
#       if any params is nil then take connection values from DataDriven::MySql::ConfigMySql
# @return MySQL Connection
def open_connection(host = nil, port = nil, db_name = nil, db_user = nil, db_password = nil)
  host = host.nil? ? DataDriven::MySql::ConfigMySql.host : host
  port = port.nil? ? DataDriven::MySql::ConfigMySql.port : port
  db_name = db_name.nil? ? DataDriven::MySql::ConfigMySql.db_name : db_name
  db_user = db_user.nil? ? DataDriven::MySql::ConfigMySql.db_user : db_user
  db_password = db_password.nil? ? DataDriven::MySql::ConfigMySql.db_password : db_password

  #open connecion
  DataDriven::MySql::ConfigMySql.connection = Mysql.connect("mysql://#{db_user}:#{db_password}@#{host}:#{port}/#{db_name}?charset=utf8mb4")

  return DataDriven::MySql::ConfigMySql.connection

rescue Exception => e
  raise "Error. Mysql connection was not done. Exception => #{e.message}"
end


#####################################################
# GET SQL DATA
#####################################################

#Get data from data base when execute any mysql query
# @params:
#   :query String SQL query
def get_sql_data(query)
  myquerydata = Array.new
  keys = Array.new
  #Get data
  q = DataDriven::MySql::ConfigMySql.connection.query(query)

  #get header columns
  for i in 0..(q.fields.size - 1)
    keys.push(q.fields[i].name)
  end

  #make hash map to headers and colus
  return q.to_a.map {|row| Hash[ keys.zip(row) ] }

rescue Exception => e
  raise "Error. Mysql Get SQL Query Failed. Exception => #{e.message}"
end


#####################################################
# SQL INSERT
#####################################################

#Get data from data base when execute any mysql query
# @params:
#   :query String SQL query
def insert_data(query)
  stmt = DataDriven::MySql::ConfigMySql.connection.prepare(query)
  stmt.execute
  puts "Mysql data inserted successfully. SQL Query => #{query}"

rescue Exception => e
  raise "Error. Mysql Insert Failed. Exception => #{e.message}"
end





