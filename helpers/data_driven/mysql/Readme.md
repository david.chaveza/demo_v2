# **"QA TOUCH - Helper Conexión"**
_________________________________________________________________________

# **Agregar la funcionalidad a otros proyectos de TAF-R**

1- Agregar a helpers el siguiente folder:
`$PROJECT/helpers/qa_management_tools/qa_touch`

2- Agregar a helpers el archivo:
`$PROJECT/helpers/ZipFileGenerator.rb` Esta es una utileria que permite comprimir directorios, puede servir para multiples funcionalidades y projectos

3- Actualizar:
`$PROJECT/helpers/config.rb`

4- Actualizar:
`$PROJECT/helpers/generic.rb` Se actualizar metodo "save_evidence(evidence_status)"

5- Actualizar:
`$PROJECT/venture/features/support/hooks.rb` 

`**NOTA: Tomar en cuenta que se deben prender o apagar el resto de caracteristicas como spira conexion, qmetric, telegram, etc` 

**Codigo en Hooks (Before Scenario) - Adjuntar a QA-Touch un directorio en .zip**

`qa_touch_controller = QAManagementTools::QATouch::AddResultsController.new`
`qa_touch_controller.add_results_to_qa_touch(scenario, 
Config.current_screenshot_path,
true)`

**Codigo en Hooks (Before Scenario) - Adjuntar un archivo unico .png**

`qa_touch_controller = QAManagementTools::QATouch::AddResultsController.new`
`qa_touch_controller.add_results_to_qa_touch(scenario, 
Config.current_evidence_file,
false)`

****NOTA: Tomar en cuenta si se tienen cambios customizados en los archivos del proyecto donde se quiere integrar esta funcionalidad, se pueden sobre-escribir si se reemplaza los archivo .rb por completo; entonces se recomienda migrar solo metodos y variables necesarios**
_________________________________________________________________________

# **Setup Data Connection**

1- Variables de Entorno:
`Ej. ver $PROJECT/run/07_dummy_bash/01_qa_touch.bat`

2- QA-Touch YML config:
`Ej. ver $PROJECT/helpers/qa_management_tools/qa_touch/config.yml`


_________________________________________________________________________

# **Test Execution**

- Ejecutar desde la IDE Ruby Mine, solo si se esta debugeando, en cuyo caso puede configurar las varialbes de entorno o el archivo .YML descrito en la sección anterior
- Ejecutar desde archivos bash (.bat/.sh) como se realiza normalmente la ejecución de TAF-R, decidir el setup de la conexión con QA-Touch si sera en variables de entorno o desde YML
- Ejecutar desde Jenkins como se realiza normalmente la ejecución de TAF-R, decidir el setup de la conexión con QA-Touch si sera en variables de entorno o desde YML
- Ejecutar desde otra herramienta CI/CD (git-hub actions, etc) setup son parecidos a Jenkins en cuyo caso se requiere inveztiación, decidir el setup de la conexión con QA-Touch si sera en variables de entorno o desde YML

_________________________________________________________________________
