# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper para obtener los datos de un YML
#####################################################

require 'fileutils'
require 'yaml'


#####################################################
# READ YML DATA
#####################################################

#Metodo obtener la data de un csv en hash map
# @params:
#   :yml_file nobre del archivo yml que se va cargar
#   :path_file String directorio del archivo,
#       si este no se especifica, se toma por defautl "$PROYECT/venture/config/yml_data"
def get_yml_data(yml_file, path_file=nil)
  file_name = nil
  file = nil

  #valida si el archivo contiene extención, si no la tiene agrega .csv
  file_name = yml_file.to_s.include?('.yml') ? yml_file : "#{yml_file}.yml"
  file = path_file.nil? ? File.join(File.dirname(__FILE__), "../../venture/config/yml_data/#{file_name}")
                 : "#{path_file}/#{file_name}"
  puts "YML FILE => #{file}"

  return YAML.load(File.read(file))
end

