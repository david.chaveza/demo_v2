# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper retorna javascrip en string
#####################################################
require 'fileutils'
require 'yaml'

##################################################################################
### JAVASCRIPT - NAVEGACION
##################################################################################

#Metodo para hacer refresh a una pagina
def js_load_page
  return "location.reload();"
end

#Método para hacer back en una page
def js_back_page
  return "window.history.back();"
end

#Método para obtener la url donce se encuentra actualmente la pagina
def js_get_current_url
  return "return window.location.href;"
end


##################################################################################
### JAVASCRIPT - SCROLLS
##################################################################################

#Método para hacer scroll en un elemento de una page web
# @params
# * :element elemento al cual se hace scroll
# * :point punto al cual se hace scroll
def js_scroll_left(element, point)
  return "$('#{element}').scrollLeft(#{point});"
end

#posiciona la pantalla en el elemento
def js_scroll_to_element
  script = <<-JS
      arguments[0].scrollIntoView(true);
  JS
end

#Método que ejecuta el metodo .focus() el un elemento
def js_focus_to_element
  script = <<-JS
      arguments[0].focus();
  JS
end

#Método para hacer scroll en un elemento de una page web
# @params
# * :element elemento al cual se hace scroll
# * :point punto al cual se hace scroll
def js_scroll_to_x_y(x, y)
  return "window.scrollTo(#{x},#{y})"
end


##################################################################################
### JAVASCRIPTS - ACCIONES
##################################################################################

def js_click_on_element
  script = <<-JS
      arguments[0].click();
  JS
end

def js_remove_attribute_element
  script = <<-JS
      arguments[0].removeAttribute(arguments[1]);
  JS
end

#posiciona la pantalla en el elemento
def js_mouse_over
  script = <<-JS
      arguments[0].trigger("mouseenter");
  JS
end