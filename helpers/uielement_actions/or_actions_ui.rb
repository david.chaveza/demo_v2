# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper para ejecutar acciones del OR file
#####################################################
require_relative 'or_capybara/ORUIElementActions'
require_relative 'or_appium/ORAppiumUIElementActions'
require_relative 'or_winium/ORWiniumUIElementActions'
require_relative 'ORBase'

#Metodo que ejecuta validacion y acciones sobre un web element
# @params
#   :or_uielement HASH, fila del OR CSV
#   :send_text: String texto a ingresar en el elemento.
#   :data_override_locator: Array[String], cada expresion REGEX '${DT_VALUE}' del la columna ':locator'
#                               se sobreescribira con cada 'string' del 'ARRAY'
#                                - ${DT_VALUE} se sobreescriben, con los valores del array de este parametro
# @return
#   CLASS OBJECT 'ORAction::ORBase'
def or_exec_uielement(or_uielement, send_text = nil, data_override_locator = nil)
  #CREAR OR BASE
  @or_base = nil
  @or_base = ORActions::ORBase.new

  #OBTENER EL UIELEMENT
  or_get_uielement(or_uielement, send_text, data_override_locator)
  #puts "or_uielement => #{or_uielement}"

  #evalua si el resultado de la validacion es 'true' y ejecuta las 'acciones'
  if @or_base.result
    action_1 = @or_base.action_1.nil? ? :na : @or_base.action_1.to_s.to_sym
    action_2 = @or_base.action_2.nil? ? :na : @or_base.action_2.to_s.to_sym
    or_uielements_action( action_1 ) if action_1 != :na
    sleep(@or_base.sleep_between_actions.to_s.to_i) unless @or_base.sleep_between_actions.nil?
    or_uielements_action( action_2 ) if action_2 != :na
    sleep(@or_base.sleep_between_actions.to_s.to_i) unless @or_base.sleep_between_actions.nil?
  end

  return @or_base
end

#Metodo que valida que el elemento exista y setea la DATA al objeto clase 'ORActions::ORBase'
# @params
#   :or_uielement HASH, fila del OR CSV
#   :send_text: String texto a ingresar en el elemento.
#   :data_override_locator: Array[String], cada expresion REGEX '${DT_VALUE}' del la columna ':locator'
#                               se sobreescribira con cada 'string' del 'ARRAY'
#                                - ${DT_VALUE} se sobreescriben, con los valores del array de este parametro
# @return
#   CLASS OBJECT 'ORAction::ORBase'
def or_get_uielement(or_uielement, send_text = nil, data_override_locator = nil)
  or_base = ORActions::ORBase.new
  #SET OR_CSV - OR BASE
  or_base.set_or_csv(or_uielement, send_text, data_override_locator)
  #CALCULAR EL 'automation_driver'
  or_base.automation_driver = or_base.automation_driver.to_s.to_sym
  or_base.result = false

  #validar si el 'automation_driver' es valido
  or_error_mjs = ORActions::ORErrorMessages.new(or_base)
  raise(or_error_mjs.ex_msj_error_automation_driver) unless ORActions::ORBase.OR_AUTOMATION_DRIVERS.include?(or_base.automation_driver)

  #OVERRIDE 'LOCATOR' O 'LOCATOR_OVERRIDE'
  if or_base.locator.to_s.include?('${DT_VALUE}')
    raise("Error en el UIElement: '#{or_base.element_name}'. El parametro 'data_override_locator' debe ser 1 ARRAY. El valor actual es: #{or_base.data_override_locator}") unless or_base.data_override_locator.is_a?(Array)
    locator_override = nil
    locator_override = String.new(or_base.locator.to_s.strip)
    locator_override = locator_override(locator_override, or_base.data_override_locator)
    or_base.locator_new = locator_override
  else
   or_base.locator_new = or_base.locator
  end

  #CALCULAR TIMEOUT UIELEMENT
  or_base.time_out = or_base.time_out.nil? ? 1 : or_base.time_out

  #ENVIAR LA INFO OBTENIDA HASTA EL MOMENTO A '@or_base'
  @or_base = or_base
  #OBTENER EL UIELEMENT Y VALIDAR SI EXISTE O NO EN BASE A ESO APLICAR  'ASSERT/VERIFY'
  #   - VERIFY = 0  - ASSERT = 1
  or_base.uielement = validar_uielement

  #SETEAR RESULT A TRUE SI EL ELEMENTO EXISTE
  if or_base.uielement
    or_base.result = true
  end

  #puts "result => #{result}"
  #SE RETORNA EL VALOR EN FORMA DE HASH
  @or_base = or_base
  return or_base
end

# @params
#   CLASS OBJECT 'ORAction::ORBase'
private
def or_uielements_action(action)
  case @or_base.automation_driver
  when :capybara
    or_actions = ORActions::ORCapybara::ORUIElementActions.new(@or_base)
    or_actions.or_uielement_action(action)

  when :appium
    or_actions = ORActions::ORAppium::ORAppiumUIElementActions.new(@or_base)
    or_actions.or_uielement_action(action)

  when :appium_wam
    or_actions = ORActions::ORAppium::ORAppiumUIElementActions.new(@or_base)
    or_actions.or_uielement_action(action)

  else

  end
end

def locator_override(locator_override, data_override_locator)
  matches = locator_override.scan(/\$\{DT_VALUE\}/)
  #iteramos los match y los sustituimos con los elementos de data_override_locator
  matches.each_with_index do |match, index|
    locator_override[match] = data_override_locator[index].to_s.strip
  end
  puts "locator override => #{locator_override}"
  return locator_override
end

#Metodo que evalua ASSERT/VERITY a un elemento dependiendo el 'DRIVE_NAME' que usemos
# @params
#   :or_uielement HASH, fila del OR CSV
# @return
#   Dependiendo del driver:
#     - UIELEMENT Objeto de elemento dependiendo el driver
#     - String Locator del elemento
#     - True/False si existe o no el elmento
def validar_uielement
  element = nil

  case @or_base.automation_driver
  when :capybara
    or_uielement_actions = ORActions::ORCapybara::ORUIElementActions.new(@or_base)
    element = or_uielement_actions.validar_uielement

  when :appium
    or_uielement_actions = ORActions::ORAppium::ORAppiumUIElementActions.new(@or_base)
    element = or_uielement_actions.validar_uielement

  when :appium_wam
    or_uielement_actions = ORActions::ORAppium::ORAppiumUIElementActions.new(@or_base)
    element = or_uielement_actions.validar_uielement

  when :winium
    or_uielement_actions = ORActions::ORWinium::ORWiniumUIElementActions.new(@or_base)
    element = or_uielement_actions.validar_uielement

  else

  end
  return element
end
