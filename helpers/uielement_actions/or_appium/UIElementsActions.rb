require 'fileutils'
require 'selenium-webdriver'
require 'appium_lib'
require_relative 'mensajes_webdriver'
require_relative '../../../helpers/mate'

module ORActions
  module ORAppium
    class UIElementsActions

      def initialize(driver=Appium::Driver.new)
        @driver = driver
      end

      #**************************************************************************************
      # ACCIONES
      #**************************************************************************************

      #Permite abrir una nueva app a partir de su package y el nombre del activity
      #   Y se puede continuar testeando con los comandos de appium
      def start_activity(app_package, app_activity)
        action = 'ABRIR APP + ACTIVITY'
        begin
          @driver.start_activity({app_package: app_package, app_activity: app_activity})

        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def click_uielement(uielement, mobile_lib=nil)
        action = 'CLICK'
        begin
          if mobile_lib.to_s.downcase.include?('mate')
            tap_element(uielement)
          else
            uielement.click
          end

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def touch_uielement(uielement, mobile_lib=nil)
        action = 'TOUCH'
        begin
          if mobile_lib.to_s.downcase.include?('mate')
            tap_element(uielement)
          else
            accion = Appium::TouchAction.new(@driver).press(uielement.location.to_h).wait(5).release
            accion.perform
          end

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def send_keys_to_uielement(uielement, keys, mobile_lib=nil)
        action = 'SEND KEYS (ESCRIBIR TEXTO)'
        begin
          if mobile_lib.to_s.downcase.include?('mate')
            set_text_element(uielement, keys)
          else
            uielement.send_keys keys
          end

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def select_uielement_option_by_text(uielement, option)
        action = 'SELECT LIST POR TEXTO'
        begin
          dropdown = uielement
          select_list = Selenium::WebDriver::Support::Select.new(dropdown)
          select_list.select_by(:text, option.to_s.strip)

          uielement.send_keys keys

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def select_uielement_option_by_value(uielement, option)
        action = 'SELECT OPTION LIST POR TEXTO'
        begin
          dropdown = uielement
          select_list = Selenium::WebDriver::Support::Select.new(dropdown)
          select_list.select_by(:value, option.to_s.strip)

          uielement.send_keys keys

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def move_to_uielement(uielement_origin, uielement_target)
        action = 'MOVER A...'
        begin
          accion = Appium::TouchAction.new(@driver).press(uielement_origin.location.to_h).move_to(uielement_target.location.to_h)
          accion.release.perform

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def drag_and_drop_to_uielement(uielement_origin, uielement_target)
        action = 'DRAG AND DROP'
        begin
          accion = Appium::TouchAction.new(@driver)
          accion.long_press(uielement_origin.location.to_h).move_to(uielement_target.location.to_h)
          accion.release.perform

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end


    end
  end
end
