#INFO: http://appium.io/docs/en/commands/web/execute/index.html

require 'fileutils'
require 'selenium-webdriver'
require_relative 'mensajes_webdriver'
require_relative '../js_web'

module ORActions
  module ORAppium
    class UIElementsJSBrowserActions

      def initialize(driver)
        @driver = driver
      end


      ##################################################################################
      ### JAVASCRIPT - NAVEGADOR
      ##################################################################################

      def js_reload_page
        action = 'JS REFRESCAR PAGINA'
        begin
          @driver.execute_script(js_load_page)
        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def js_back_browser_page
        action = 'JS IR ATRAS (BACK PAGE)'
        begin
          @driver.execute_script(js_back_page)
        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def js_get_current_url_page
        action = 'JS OBTENER URL ACTUAL'
        begin
          @driver.execute_script(js_get_current_url)
        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end


      ##################################################################################
      ### JAVASCRIPT - ACCIONES UIELEMENTS
      ##################################################################################

      def js_click_uielement(uielement)
        action = 'JS CLICK (CLICK DESDE JAVASCRIPT)'
        begin
          @driver.execute_script(js_click_on_element, uielement)
        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def js_remove_attribute_uielement(uielement)
        action = 'JS REMOVER ATRIBUTO'
        begin
          @driver.execute_script(js_remove_attribute_element, uielement)
        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end


      ##################################################################################
      ### JAVASCRIPT - SCROLLS
      ##################################################################################

      #scroll into view a elemento
      #Posiciona la pantalla en el elemento
      def js_focus_to_uielement(uielement)
        action = 'JS FOCUS (FOCUS AL ELEMENTO)'
        begin
          @driver.execute_script(js_focus_to_element, uielement)
        rescue Exception => e
          puts err_msj_js_action(action, e.message)
        end
      end

      def js_scroll_to_uielement(uielement)
        action = 'JS SCROLL INTO VIEW (POSICIONAR SOBRE ELEMENTO)'
        begin
          @driver.execute_script(js_scroll_to_element, uielement)
        rescue Exception => e
          puts err_msj_js_action(action, e.message)
        end
      end

      def js_scroll_screen_by_x_y(x, y)
        action = "JS SCROLL EN LA VENTANA PARA COORDENADAS #{x}, #{y}"
        begin
          @driver.execute_script(js_scroll_to_x_y(x, y))
        rescue Exception => e
          puts err_msj_js_action(action, e.message)
        end
      end


    end
  end
end
