#INFO: http://appium.io/docs/en/commands/mobile-command/

require 'fileutils'
require 'appium_lib'
require_relative 'mensajes_webdriver'

module ORActions
  module ORAppium
    class UIElementsJSAdrAppActions

      def initialize(driver=Appium::Driver.new)
        @driver = driver
      end


      def jsapp_batery_info
        action = 'JS-APP OBTENER INFO DE LA BATERIA'
        begin
          return @driver.execute_script('mobile:batteryInfo')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_accept_alert
        action = 'JS-APP ACEPTAR ALERT'
        begin
          @driver.execute_script('mobile:acceptAlert')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_dismiss_alert
        action = 'JS-APP DESCARTAR ALERT'
        begin
          @driver.execute_script('mobile:dismissAlert')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_scroll(selector, locator)
        action = 'JS-APP SCROLL A UIELEMENT VISIBLE'
        begin
          @driver.execute_script('mobile:scroll', {strategy: selector, selector: locator})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_deep_link(url, app_package)
        action = 'JS-APP DEEP LINK'
        begin
          @driver.execute_script('mobile:deepLink', {url: url, package: app_package})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_get_device_info
        action = 'JS-APP OBTENER INFO DEL DISPOSITIVO'
        begin
          @driver.execute_script('mobile:deviceInfo')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_enter_text(text)
        action = 'JS-APP INGRESAR TEXTO'
        begin
          @driver.execute_script('mobile:type', {text: text.to_s.strip})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

    end
  end
end
