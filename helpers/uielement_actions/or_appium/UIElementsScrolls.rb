require 'fileutils'
require 'appium_lib'
require_relative 'mensajes_webdriver'
require_relative '../../../helpers/mate'
require_relative 'UIElementsAsserts'

module ORActions
  module ORAppium
    class UIElementsScrolls

      def initialize(driver=Appium::Driver.new)
        @driver = driver
        @ui_asserts = ORActions::ORAppium::UIElementsAsserts.new(@driver)
      end

      #**************************************************************************************
      # ACCIONES
      #**************************************************************************************

      def scroll_down(scroll_size =  nil)
        action = 'SCROLL DOWN'
        begin
          scroll_size = scroll_size.nil? ? 100 : scroll_size.to_i
          scroll_to_down scroll_size

        rescue Exception => ex
          raise err_msj_uielement_action(action, ex.message)
        end
      end

      def scroll_up(scroll_size =  nil)
        action = 'SCROLL UP'
        begin
          scroll_size = scroll_size.nil? ? 100 : scroll_size.to_i
          scroll_to_up scroll_size

        rescue Exception => ex
          raise err_msj_uielement_action(action, ex.message)
        end
      end

      def scroll_down_to_uielement(selector, locator, scroll_intentos = nil, scroll_size =  nil)
        action = 'SCROLL DOWN TO UIELEMENT'
        begin
          element = false
          intentos = 0
          scroll_intentos = scroll_intentos.nil? ? 5 : scroll_intentos.to_i
          scroll_size = scroll_size.nil? ? 100 : scroll_size.to_i

          while intentos < scroll_intentos
            element = @ui_asserts.verify_for_uielement_exist(selector, locator, 1)
            if element
              break
            end
            scroll_to_down scroll_size
            intentos = intentos + 1
          end

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, ex.message)
        end

        return element
      end

      def scroll_up_to_uielement(selector, locator, scroll_intentos = nil, scroll_size =  nil)
        action = 'SCROLL DOWN TO UIELEMENT'
        begin
          element = false
          intentos = 0
          scroll_intentos = scroll_intentos.nil? ? 5 : scroll_intentos.to_i
          scroll_size = scroll_size.nil? ? 100 : scroll_size.to_i

          while intentos < scroll_intentos
            element = @ui_asserts.verify_for_uielement_exist(selector, locator, 1)
            if element
              break
            end
            scroll_to_up scroll_size
            intentos = intentos + 1
          end

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, ex.message)
        end

        return element
      end

    end
  end
end
