require 'fileutils'
require 'appium_lib'
require 'selenium-webdriver'
require_relative '../../retryable'
require_relative '../ORBase'
require_relative '../ORErrorMessages'
require_relative 'UIBrowserActions'
require_relative 'UIElementsActions'
require_relative 'UIElementsAsserts'
require_relative 'UIElementsJSIOSAppActions'
require_relative 'UIElementsJSAdrAppActions'
require_relative 'UIElementsJSBrowserActions'
require_relative 'UIElementsScrolls'

module ORActions
  module ORAppium
    class ORAppiumUIElementActions
      include Retryable

      def initialize(or_base = ORActions::ORBase.new)
        @or_base = or_base
        @driver = or_base.driver
        @ui_browser_action = ORActions::ORAppium::UIBrowserActions.new(@driver)
        @ui_elements_action = ORActions::ORAppium::UIElementsActions.new(@driver)
        @ui_asserts = ORActions::ORAppium::UIElementsAsserts.new(@driver)
        @ui_js_ios_action = ORActions::ORAppium::UIElementsJSIOSAppActions.new(@driver)
        @ui_js_adr_action = ORActions::ORAppium::UIElementsJSAdrAppActions.new(@driver)
        @ui_js_browser_action = ORActions::ORAppium::UIElementsJSBrowserActions.new(@driver)
        @ui_elements_scrolls = ORActions::ORAppium::UIElementsScrolls.new(@driver)
        @or_err_msj = ORActions::ORErrorMessages.new(@or_base)
        @string_market = "****************************************************************************"
      end

      #Metodo que evalua ASSERT/VERITY a un elemento
      # @params
      # @return
      #   - Capybara::UIElement Objeto UIElement - SI '@or_base.selector.to_s.to_sym != :get_array_uielements'
      #   - ARRAY Capybara::UIElement Ojetos UIElements - SI '@or_base.selector.to_s.to_sym = :get_array_uielements'
      def validar_uielement
        element = nil
        if @or_base.automation_driver.to_s.to_sym == :appium
          element = @or_base.locator_new
        else
          validar_uielement_appium_wam
        end

          return element
      end

      def validar_uielement_appium_wam
        element = nil
        if @or_base.selector.to_s.to_sym == :url
          element = true
        elsif @or_base.selector.to_s.to_sym == :start_activity
          element = true
        elsif @or_base.selector.to_s.to_sym == :mate
          element = @or_base.locator_new
        else
          #SI 'ACCION_1' ES ':get_array_uielements', entonces se recuperar todos los elementos con find_elements
          if @or_base.action_1.to_s.to_sym == :get_array_uielements
            element = assert_verify_array_uielement
          else
            element = assert_verify_uielement
          end
        end
        return element
      end

      def assert_verify_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        end
        return element
      end

      def assert_verify_array_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        end
        return element
      end


      #Metodo que ejecuta acciones a un elemento, este metodo se debe implementar usando OR DATA POOL
      #   :action Accion a realizar
      def or_uielement_action(action)
        #OBJECT => find(CAPYBARA=>ELEMENT)
        uielement = @or_base.uielement
        #valor a ingresar en el uielement
        send_text = @or_base.send_text
        #obtener accition y datos scroll
        action_scroll = get_actions_scrolls(action)
        action = action_scroll[:action]
        scroll_intentos = action_scroll[:scroll_intentos]
        scroll_drag = action_scroll[:scroll_drag]
        #obtener accition y datos para start activity
        action_start_activity = get_actions_start_activity(action)
        action = action_start_activity[:action]
        app_package = action_start_activity[:app_package]
        app_activity = action_start_activity[:app_activity]
        ENV['APPIUM_DUMP_XML'] = nil
        ENV['APPIUM_DUMP_XML'] = @or_base.driver.page_source

        action = action.nil? ? :na : action.to_s.to_sym
        action = action.to_s.empty? ? :na : action.to_s.to_sym

        #REINTENTAR HACER LA ACCIÓN EN CASO DE ERROR
        retryable(:tries => 1, :on => Exception, :log =>:error) do
          #posibiona la pantalla en el elemento con js
          @ui_js_browser_action.js_scroll_to_uielement(uielement) if @or_base.automation_driver == :appium_wam

          case action.to_s.to_sym
          when :na
            puts "NA ACTION"
          when :get_array_uielements
            puts "NA ACTION => PARA APPIUM_WAM => SE RECUEPARAN TODOS LOS UILEMENTS - EN EL OR_GET_UIELEMENT"
            puts "ACCION APLICADA - PARA APPIUM..."
            action_assert_verify_uielements(1)
          when :get_uielement
            puts "NA ACTION => PARA APPIUM_WAM => SE RECUEPARAN TODOS LOS UILEMENTS - EN EL OR_GET_UIELEMENT"
            puts "ACCION APLICADA - PARA APPIUM..."
            action_assert_verify_uielement(1)

          ##################################################################################
          ### WAM (WEB AUTOMATION MOBILE)
          ##################################################################################
          when :start_activity
            @ui_elements_action.start_activity(app_package, app_activity)

          when :click
            action_assert_verify_uielement(1)
            @ui_elements_action.click_uielement(@or_base.uielement, @or_base.selector)

          when :touch
            action_assert_verify_uielement(1)
            @ui_elements_action.touch_uielement(@or_base.uielement, @or_base.selector)

          when :send_keys
            action_assert_verify_uielement(1)
            @ui_elements_action.send_keys_to_uielement(@or_base.uielement, send_text, @or_base.selector)

          when :select_option_by_text
            @ui_elements_action.select_uielement_option_by_text(@or_base.uielement, send_text.to_s.strip)

          when :select_option_by_value
            @ui_elements_action.select_uielement_option_by_value(@or_base.uielement, send_text.to_s.strip)

          ##################################################################################
          ### BROWSER
          ##################################################################################
          when :visit
            @ui_browser_action.visit_url(@or_base.locator_new.to_s.strip)

          when :switch_web_view
            @ui_browser_action.switch_web_view

          when :switch_out_web_view
            @ui_browser_action.switch_out_web_view

          ##################################################################################
          ### SCROLL ANDROID
          ##################################################################################
          when :scroll_down
              @ui_elements_scrolls.scroll_down(scroll_drag)

          when :scroll_up
              @ui_elements_scrolls.scroll_up(scroll_drag)

          when :scroll_down_to_uielement
            @or_base.uielement = @ui_elements_scrolls.scroll_down_to_uielement(@or_base.selector, @or_base.locator_new, scroll_intentos, scroll_drag)

          when :scroll_up_to_uielement
            @or_base.uielement = @ui_elements_scrolls.scroll_up_to_uielement(@or_base.selector, @or_base.locator_new, scroll_intentos, scroll_drag)

          ##################################################################################
          ### JS - BROWSER
          ##################################################################################
          when :js_reload_page
            @ui_js_browser_action.js_reload_page

          when :js_back_page
            @ui_js_browser_action.js_back_browser_page

          when :js_get_current_url
            @ui_js_browser_action.js_get_current_url_page

          when :js_click
            @ui_js_browser_action.js_click_uielement(uielement)

          when :js_remove_attribute
            @ui_js_browser_action.js_remove_attribute_uielement(uielement)

          when :js_focus
            @ui_js_browser_action.js_focus_to_uielement(uielement)

          when :js_scroll_into_view
            @ui_js_browser_action.js_scroll_to_uielement(uielement)

          else
            raise "ERROR => or_uielement_action => ACCION NO VALIDA. => #{action}"
          end
        end

      rescue Selenium::WebDriver::Error::NoSuchElementError => e3
        raise @or_err_msj.ex_element_not_found
      rescue Exception => ex
        raise @or_err_msj.ex_element_action(action, ex.message)

      end

      def get_actions_scrolls(action)
        res = { :action => action, :scroll_intentos => nil, :scroll_drag => nil }
        if action.to_s.include?('|') and action.to_s.downcase.include?('scroll')
          action = action.to_s.split('|')
          res[:action] = action[0]
          res[:scroll_intentos] = action[1]
          res[:scroll_drag] = action[2]
        end
        return res
      end

      def get_actions_start_activity(action)
        res = { :action => action, :app_package => nil, :app_activity => nil }
        if action.to_s.include?('|') and action.to_s.downcase.include?('start_activity')
          action = action.to_s.split('|')
          res[:action] = action[0]
          res[:app_package] = action[1]
          res[:app_activity] = action[2]
        end
        return res
      end

      def action_assert_verify_uielement(timeout)
        if @or_base.automation_driver == :appium and @or_base.selector.to_s.to_sym != :mate
          @or_base.uielement = @ui_asserts.assert_for_uielement_exist(@or_base.selector, @or_base.locator_new, timeout, @or_err_msj.ex_element_not_found)
        end
      end

      def action_assert_verify_uielements(timeout)
        if @or_base.automation_driver == :appium and @or_base.selector.to_s.to_sym != :mate
          @or_base.uielement = @ui_asserts.assert_for_uielement_exist(@or_base.selector, @or_base.locator_new, timeout, @or_err_msj.ex_element_not_found)
        end
      end



    end
  end
end
