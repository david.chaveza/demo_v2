#INFO: http://appium.io/docs/en/commands/mobile-command/

require 'fileutils'
require 'appium_lib'
require_relative 'mensajes_webdriver'

module ORActions
  module ORAppium
    class UIElementsJSIOSAppActions

      def initialize(driver=Appium::Driver.new)
        @driver = driver
      end


      def jsapp_batery_info
        action = 'JS-APP OBTENER INFO DE LA BATERIA'
        begin
          return @driver.execute_script('mobile:batteryInfo')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_alert(alert_action, button_label)
        action = "JS-APP ALERT: '#{alert_action.to_s.upcase}'"
        begin
          @driver.execute_script('mobile:alert', {action: alert_action, buttonLabel: button_label})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_scroll_down
        action = 'JS-APP SCROLL DOWN'
        begin
          @driver.execute_script('mobile:scroll', { direction: "down"})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_scroll_up
        action = 'JS-APP SCROLL UP'
        begin
          @driver.execute_script('mobile:scroll', { direction: "up"})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_scroll_right
        action = 'JS-APP SCROLL RIGHT'
        begin
          @driver.execute_script('mobile:scroll', { direction: "right"})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_scroll_left
        action = 'JS-APP SCROLL LEFT'
        begin
          @driver.execute_script('mobile:scroll', { direction: "left"})

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end

      def jsapp_get_device_info
        action = 'JS-APP OBTENER INFO DEL DISPOSITIVO'
        begin
          @driver.execute_script('mobile:deviceInfo')

        rescue Exception => e
          raise err_msj_js_action(action, e.message)
        end
      end


    end
  end
end
