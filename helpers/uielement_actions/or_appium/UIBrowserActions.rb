require 'fileutils'
require 'appium_lib'
require_relative 'mensajes_webdriver'

module ORActions
  module ORAppium
    class UIBrowserActions

      def initialize(driver=Appium::Driver.new)
        @driver = driver
      end

      def visit_url(url)
        begin
          @driver.get(url)

        rescue Exception => e
          raise err_msj_visit_url(url, e.message)
        end
      end

      #Metodo para hacer switch a una ventana por su nombre
      def switch_window_by_title(titulo_ventana)
        result = false
        ventanas = @driver.window_handles
        for i in 0..(ventanas.size - 1)
          @driver.switch_to.window(ventanas[i])
          puts "TITULO PAGINA => #{@driver.title}"
          if titulo_ventana.to_s.strip.downcase == @driver.title.to_s.strip.downcase
            result = true
            break
          end
        end
      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end

      #Metodo para hacer switch a una ventana por su nombre
      def switch_window_by_title_contains(titulo_ventana)
        result = false
        ventanas = @driver.window_handles
        for i in 0..(ventanas.size - 1)
          @driver.switch_to.window(ventanas[i])
          puts "TITULO PAGINA => #{@driver.title}"
          if titulo_ventana.to_s.strip.downcase.include?(@driver.title.to_s.strip.downcase)
            result = true
            break
          end
        end
      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end

      #Hacer switch a webview
      def switch_web_view
        webview = @driver.available_contexts.last
        #@driver.switch_to.context(webview)
        @driver.set_context(webview)

      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end

      #Hacer switch fuera del webview 'contenido principal'
      def switch_out_web_view
        #@driver.switch_to.context(@driver.contexts.first)
        @driver.set_context(@driver.available_contexts.first)
        @driver.switch_to_default_context

      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end


    end
  end
end
