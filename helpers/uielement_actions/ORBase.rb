module ORActions
  class ORBase

    #*********************************************************
    # VARIABLES - OR CSV
    #*********************************************************
    @automation_driver
    @assert_verify_action
    @element_name
    @or_element_name
    @selector
    @locator
    @array_uielements
    @locator_new
    @action_1
    @action_2
    @actions_extra
    @sleep_between_actions
    @time_out
    @element_description

    #*********************************************************
    # VARIABLES - CONSTANTES DEFAULT
    #*********************************************************
    @OR_AUTOMATION_DRIVERS = [
        :capybara,
        :webdriver,
        :selenium,
        :appium,
        :appium_wam,
        :winium,
        :winappdriver
    ].freeze

    #*********************************************************
    # VARIABLES - RESULTADOS EVALUAR EL UIELEMENT (OPTIONS DEL METODO 'OR_EXECUTE_UIELEMENT')
    #*********************************************************
    @data_override_locator = []
    @uielement
    @send_text
    @options
    @result

    #*********************************************************
    # DRIVER AUTOMATION. PARA APPIUM, WINIUM, ETC
    #*********************************************************
    @driver

    #*********************************************************
    # METODO - SET CSV OR
    #*********************************************************
    #Setea variables de esta clase desde el HASH obtenido de 1 elemento dle OR CSV
    # @params
    #   :or_uielement HASH, fila del OR CSV FILE
    #       EJ. {
    #             :automation_driver=>"capybara", :assert_verify_action=>"0", :element_name=>"Text Email", :or_element_name=>"txt_email",
  #               :selector=>"id", :locator=>"email", :array_uielements=>"0", :action_1=>"click",
    #             :action_2=>"send_keys", :actions_extra=>nil, :sleep_between_actions=>nil, :time_out=>"1",
    #             :element_description=>"campo para capturar email"
    #           }
    def set_or_csv(or_uielement, send_text = nil, data_override_locator = nil)
      @automation_driver = or_uielement[:automation_driver]
      @assert_verify_action = or_uielement[:assert_verify_action]
      @element_name = or_uielement[:element_name]
      @or_element_name = or_uielement[:or_element_name]
      @selector = or_uielement[:selector]
      @locator = or_uielement[:locator]
      @array_uielements = or_uielement[:array_uielements]
      @action_1 = or_uielement[:action_1]
      @action_2 = or_uielement[:action_2]
      @actions_extra = or_uielement[:actions_extra]
      @sleep_between_actions = or_uielement[:sleep_between_actions]
      @time_out = or_uielement[:time_out]
      @element_description = or_uielement[:element_description]
      @driver = or_uielement[:driver]
      @send_text = send_text
      @data_override_locator = data_override_locator
    end

    #*********************************************************
    # METODOS CONSULTA
    #*********************************************************
    def to_s
      s = StringIO.new
      s << "automation_driver = #{@automation_driver} \n"
      s << "assert_verify_action = #{@assert_verify_action} \n"
      s << "element_name = #{@element_name} \n"
      s << "or_element_name = #{@or_element_name} \n"
      s << "selector = #{@selector} \n"
      s << "locator = #{@locator} \n"
      s << "array_uielements = #{@array_uielements} \n"
      s << "locator_override = #{@locator_override} \n"
      s << "locator_new = #{@locator_new} \n"
      s << "action_1 = #{@action_1} \n"
      s << "action_2 = #{@action_2} \n"
      s << "actions_extra = #{@actions_extra} \n"
      s << "sleep_between_actions = #{@sleep_between_actions} \n"
      s << "time_out = #{@time_out} \n"
      s << "element_description = #{@element_description} \n"
      s << "OR_DEFAULT_OPTIONS = #{@OR_DEFAULT_OPTIONS} \n"
      s << "OR_AUTOMATION_DRIVERS = #{@OR_AUTOMATION_DRIVERS} \n"
      s << "driver = #{@driver} \n"
      s << "data_override_locator = #{@data_override_locator} \n"
      s << "uielement = #{@uielement} \n"
      s << "send_text = #{@send_text} \n"
      s << "options = #{@options} \n"
      s << "result = #{@result} \n"
      s.string.to_s
    end

    #*********************************************************
    # ATTR
    #*********************************************************
    attr_accessor :locator, :result, :locator_new,
                  :send_text, :actions_extra, :uielement, :action_1, :action_2,
                  :element_description, :options, :locator_override, :time_out, :automation_driver,
                  :assert_verify_action, :element_name, :or_element_name, :sleep_between_actions,
                  :selector, :data_override_locator, :array_uielements, :driver

    class << self
      attr_reader :OR_AUTOMATION_DRIVERS

    end

  end
end

