require 'fileutils'
require 'selenium-webdriver'
require_relative 'mensajes_webdriver'

module ORActions
  module ORWinium
    class UIElementsAsserts

      def initialize(driver=Selenium::WebDriver)
        @driver = driver
      end
      ##################################################################################
      ### ASSERTS - GENERICOS
      ##################################################################################

      #Asssert para validar si la pagina esta en una 'url' en un determindado timeout
      # @params
      #   :url_substring string que puede estar contenido en la url
      #   :timeout timeout a esperar la url
      # @return
      #   True si la URL abierta es la esperada, Raise si la URL no es la correcta
      def assert_current_url(url_substring, timeout=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          current_url = @driver.current_url
          if current_url.to_s.include? "#{url_substring}"
            result = true
            break
          end
        end

        puts "current url => #{current_url}"
        unless result
          raise("URL INCORRECTA. URL ACTUAL: #{current_url}. URL ESPERADA #{url_substring}")
        end
        return result
      end


      ##################################################################################
      ### ASSERTS - FIND UIELEMENT
      ##################################################################################

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   WebElement::UIElement si el elemento existe, Raise Exception si no se encuentra el elemento
      def assert_for_uielement_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = nil
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, to, '')
                        : mjs_error

        wait = Selenium::WebDriver::Wait.new(:timeout => to, :message => mjs_error)
        result = wait.until { @driver.find_element(selector, locator) }
        wait.until { result.displayed? }
        return result

      rescue Exception => e
        raise(e.message)
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, False si no se encuentra el elemento
      def verify_for_uielement_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = nil
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, to, '')
                        : mjs_error

        wait = Selenium::WebDriver::Wait.new(:timeout => to, :message => mjs_error)
        result = wait.until { @driver.find_element(selector, locator) }
        wait.until { result.displayed? }
        return result

      rescue Exception => e
        puts(e.message)
      end


      ##################################################################################
      ### FIND ELEMENTS (LIST UIELEMENTS)
      ##################################################################################

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, Raise Exception si no se encuentra el elemento
      def assert_for_uielements_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = nil
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, to, '')
                        : mjs_error

        wait = Selenium::WebDriver::Wait.new(:timeout => to, :message => mjs_error)
        result = wait.until { @driver.find_elements(selector, locator) }
        wait.until { result.displayed? }
        return result

      rescue Exception => e
        raise(e.message)
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, False si no se encuentra el elemento
      def verify_for_uielements_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = nil
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, to, '')
                        : mjs_error

        wait = Selenium::WebDriver::Wait.new(:timeout => to, :message => mjs_error)
        result = wait.until { @driver.find_elements(selector, locator) }
        wait.until { result.displayed? }
        return result

      rescue Exception => e
        raise(e.message)
      end


    end
  end
end
