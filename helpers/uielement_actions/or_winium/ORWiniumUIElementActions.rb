require 'fileutils'
require 'selenium-webdriver'
require_relative '../../retryable'
require_relative '../ORBase'
require_relative '../ORErrorMessages'
require_relative 'UIElementsActions'
require_relative 'UIElementsAsserts'

module ORActions
  module ORWinium
    class ORWiniumUIElementActions
      include Retryable

      def initialize(or_base = ORActions::ORBase.new)
        @or_base = or_base
        @driver = or_base.driver
        @ui_elements_action = ORActions::ORWinium::UIElementsActions.new(@driver)
        @ui_asserts = ORActions::ORWinium::UIElementsAsserts.new(@driver)
        @or_err_msj = ORActions::ORErrorMessages.new(@or_base)
        @string_market = "****************************************************************************"
      end

      #Metodo que evalua ASSERT/VERITY a un elemento
      # @params
      # @return
      #   - Capybara::UIElement Objeto UIElement - SI '@or_base.selector.to_s.to_sym != :get_array_uielements'
      #   - ARRAY Capybara::UIElement Ojetos UIElements - SI '@or_base.selector.to_s.to_sym = :get_array_uielements'
      def validar_uielement
        element = nil
        #SI 'ACCION_1' ES ':get_array_uielements', entonces se recuperar todos los elementos con page.all
        if @or_base.action_1.to_s.to_sym == :get_array_uielements
          element = assert_verify_array_uielement
        else
          element = assert_verify_uielement
        end
        return element
      end

      def assert_verify_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        end
        return element
      end

      def assert_verify_array_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        end
        return element
      end


      #Metodo que ejecuta acciones a un elemento, este metodo se debe implementar usando OR DATA POOL
      #   :action Accion a realizar
      def or_uielement_action(action)
        #OBJECT => find(CAPYBARA=>ELEMENT)
        uielement = @or_base.uielement
        #valor a ingresar en el uielement
        send_text = @or_base.send_text

        action = action.nil? ? :na : action.to_s.to_sym
        action = action.to_s.empty? ? :na : action.to_s.to_sym

        #REINTENTAR HACER LA ACCIÓN EN CASO DE ERROR
        retryable(:tries => 1, :on => Exception, :log =>:error) do
          #posibiona la pantalla en el elemento con js
          @ui_js_browser_action.js_scroll_to_uielement(uielement) if @or_base.automation_driver == :appium_wam

          case action.to_s.to_sym
          when :na
            puts "NA ACTION"
          when :get_array_uielements
            puts "NA ACTION => PARA APPIUM_WAM => SE RECUEPARAN TODOS LOS UILEMENTS - EN EL OR_GET_UIELEMENT"
            puts "ACCION APLICADA - PARA APPIUM..."
          when :get_uielement
            puts "NA ACTION => PARA APPIUM_WAM => SE RECUEPARAN TODOS LOS UILEMENTS - EN EL OR_GET_UIELEMENT"
            puts "ACCION APLICADA - PARA APPIUM..."

          ##################################################################################
          ### WAM (WEB AUTOMATION MOBILE)
          ##################################################################################
          when :click
            @ui_elements_action.click_uielement(@or_base.uielement, @or_base.selector)

          when :send_keys
            @ui_elements_action.send_keys_to_uielement(@or_base.uielement, send_text, @or_base.selector)

          when :select_option_by_text
            @ui_elements_action.select_uielement_option_by_text(@or_base.uielement, send_text.to_s.strip)

          else
            raise "ERROR => or_uielement_action => ACCION NO VALIDA. => #{action}"
          end

        end

      rescue Selenium::WebDriver::Error::NoSuchElementError => e3
        raise @or_err_msj.ex_element_not_found
      rescue Exception => ex
        raise @or_err_msj.ex_element_action(action, ex.message)

      end

      def get_actions_scrolls(action)
        res = { :action => action, :scroll_intentos => nil, :scroll_drag => nil }
        if action.to_s.include?('|') and action.to_s.downcase.include?('scroll')
          action = action.to_s.split('|')
          res[:action] = action[0]
          res[:scroll_intentos] = action[1]
          res[:scroll_drag] = action[2]
        end
        return res
      end

      def get_actions_start_activity(action)
        res = { :action => action, :app_package => nil, :app_activity => nil }
        if action.to_s.include?('|') and action.to_s.downcase.include?('start_activity')
          action = action.to_s.split('|')
          res[:action] = action[0]
          res[:app_package] = action[1]
          res[:app_activity] = action[2]
        end
        return res
      end

      def action_assert_verify_uielement(timeout)
        if @or_base.automation_driver == :appium and @or_base.selector.to_s.to_sym != :mate
          @or_base.uielement = @ui_asserts.assert_for_uielement_exist(@or_base.selector, @or_base.locator_new, timeout, @or_err_msj.ex_element_not_found)
        end
      end

      def action_assert_verify_uielements(timeout)
        if @or_base.automation_driver == :appium and @or_base.selector.to_s.to_sym != :mate
          @or_base.uielement = @ui_asserts.assert_for_uielement_exist(@or_base.selector, @or_base.locator_new, timeout, @or_err_msj.ex_element_not_found)
        end
      end



    end
  end
end
