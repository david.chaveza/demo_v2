# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  OR MENSAJES GENERICOS
#####################################################

@string_market = "****************************************************************************"


#*****************************************************************************************************
# NAVEGADOR - MENSAJES DE ERROR
#*****************************************************************************************************
#Mensaje de error al visitar url
def err_msj_visit_url(url, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "Error. Al abrir URL: '#{url}'\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end

#Mensaje de error al hacer switch a una ventana
def err_msj_switch_window(window_title, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "ERROR AL HACER SWITCH A VENTANA\n"
  msj << "Titulo de la ventana: #{window_title}\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end


#*****************************************************************************************************
# ELEMENT NOT FOUND
#*****************************************************************************************************
#Mensaje de error al ejecutar js reload page
def err_msj_uielement_notfound(selector=nil, locator=nil, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "ELEMENTO(S) NO ENCONTRADO(S)\n"
  msj << "Filtros de busqueda:\n"
  msj << "\t- Selector: '#{selector}'\n"
  msj << "\t- Locator: '#{locator}'\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end

def err_msj_timeout_uielement(selector, locator, timeout, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "TIMEOUT ERROR AL BUSCAR EL/LOS ELEMENTO(S)\n"
  msj << "TimeOut: '#{timeout}'\n"
  msj << "Filtros de busqueda:\n"
  msj << "\t- Selector: '#{selector}'\n"
  msj << "\t- Locator: '#{locator}'\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end


#*****************************************************************************************************
# ELEMENT ACCION
#*****************************************************************************************************
#Mensaje de error al ejecutar js reload page
def err_msj_uielement_action(action, selector=nil, locator=nil, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "ERROR AL EJECUTAR ACCION\n"
  msj << "Accion:\n"
  msj << "#{action}\n"
  msj << "Filtros de busqueda:\n"
  msj << "\t- Selector: '#{selector}'\n"
  msj << "\t- Locator: '#{locator}'\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end


#*****************************************************************************************************
# JS - MENSAJES DE ERROR
#*****************************************************************************************************
#Mensaje de error al ejecutar js reload page
def err_msj_js_action(action, exception=nil)
  msj = StringIO.new
  msj << "\n"
  msj << "#{@string_market}\n"
  msj << "ERROR AL EJECUTAR JAVASCRIPT\n"
  msj << "Error al intentar realizar la acción: '#{action}'\n"
  msj << "Exception:\n"
  msj << "#{exception}\n"
  msj << "#{@string_market}\n"
  return msj.string.to_s
end


