require 'fileutils'
require 'selenium-webdriver'
require_relative 'mensajes_webdriver'

module ORActions
  module ORWinium
    class UIElementsActions

      def initialize(driver=Selenium::WebDriver)
        @driver = driver
      end

      #**************************************************************************************
      # ACCIONES
      #**************************************************************************************

      def click_uielement(uielement)
        action = 'CLICK'
        begin
            uielement.click

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def send_keys_to_uielement(uielement, keys)
        action = 'SEND KEYS (ESCRIBIR TEXTO)'
        begin
            uielement.send_keys keys

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def select_uielement_option_by_text(uielement, option)
        action = 'SELECT LIST POR TEXTO'
        begin
          dropdown = uielement
          select_list = Selenium::WebDriver::Support::Select.new(dropdown)
          select_list.select_by(:text, option.to_s.strip)

          uielement.send_keys keys

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def select_uielement_option_by_value(uielement, option)
        action = 'SELECT LIST POR TEXTO'
        begin
          dropdown = uielement
          select_list = Selenium::WebDriver::Support::Select.new(dropdown)
          select_list.select_by(:value, option.to_s.strip)

          uielement.send_keys keys

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end

      def drag_and_drop_to_uielement(uielement_origin, uielement_target)
        action = 'DRAG AND DROP'
        begin
          @driver.action.drag_and_drop(uielement_origin, uielement_target).release.perform

        rescue Selenium::WebDriver::Error::NoSuchElementError => e3
          raise err_msj_uielement_notfound(nil, nil, e3.message)
        rescue Exception => ex
          raise err_msj_uielement_action(action, nil, nil, ex.message)
        end
      end


    end
  end
end
