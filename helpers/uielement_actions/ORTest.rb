# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  OR TEST -- PRUEBAS DE CLASES DEL OR ACTIONS
#####################################################
require_relative 'ORBase'

module ORActions
  class ORTest < ORActions::ORBase
    def initialize(or_base=ORActions::ORBase.new)
      @or_base=or_base
    end

    def getselector
      puts "#{@or_base.selector}"
      @or_base.selector = "//*[MY ooo]"
      puts "#{@or_base.selector}"
    end

    private
    def selectores
      puts "#{@or_base.selector}"
      @or_base.selector = "//*[MY ooo]"
      puts "#{@or_base.selector}"
    end

  end
end

class Test2
  def initialize(or_base=ORActions::ORBase.new)
    @or_base=or_base
  end
  def test
    ortest = ORActions::ORTest.new(@or_base)
    ortest.getselector
  end
end

oraction = ORActions::ORBase.new
oraction.selector = 'VALOR SELECTOR'

ortest = ORActions::ORTest.new(oraction)
ortest.getselector

oraction.selector = 'MOD SELECTOR'
ortest.getselector

t = Test2.new(oraction)
t.test
