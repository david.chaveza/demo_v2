require 'fileutils'
require 'capybara/dsl'
require_relative 'mensajes_capybara'

module ORActions
  module ORCapybara
    class UIBrowserActions
      include Capybara::DSL

      #Método para modificar el timeout, este se inicializa por primera vez en 'env.rb'
      def set_new_timeout(to)
        Capybara.default_max_wait_time = to.to_s.to_i
      end

      #Método para resetear el timeout, este se inicializa por primera vez en el drive
      def reset_default_timeout
        Capybara.default_max_wait_time = Config.to.to_s.to_i
      end

      def visit_url(url)
        begin
          select(option, from: uielement, visible: false).select_option
        rescue Exception => e
          raise err_msj_visit_url(url, e.message)
        end
      end

      #Metodo para hacer switch a una ventana por su nombre
      def switch_window_by_title(titulo_ventana)
        result = false
        ventanas = page.driver.browser.window_handles
        for i in 0..(ventanas.size - 1)
          page.driver.browser.switch_to.window(ventanas[i])
          puts "TITULO PAGINA => #{page.title}"
          if titulo_ventana.to_s.strip.downcase == page.title.to_s.strip.downcase
            result = true
            break
          end
        end
      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end

      #Metodo para hacer switch a una ventana por su nombre
      def switch_window_by_title_contains(titulo_ventana)
        result = false
        ventanas = page.driver.browser.window_handles
        for i in 0..(ventanas.size - 1)
          page.driver.browser.switch_to.window(ventanas[i])
          puts "TITULO PAGINA => #{page.title}"
          if titulo_ventana.to_s.strip.downcase.include?(page.title.to_s.strip.downcase)
            result = true
            break
          end
        end
      rescue Exception => e
        raise err_msj_switch_window(titulo_ventana, e.message)
      end


    end
  end
end
