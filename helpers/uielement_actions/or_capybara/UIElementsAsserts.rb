require 'fileutils'
require 'capybara/dsl'
require_relative 'mensajes_capybara'

module ORActions
  module ORCapybara
    class UIElementsAsserts
      include Capybara::DSL

      ##################################################################################
      ### ASSERTS - GENERICOS
      ##################################################################################

      #Asssert para validar si la pagina esta en una 'url' en un determindado timeout
      # @params
      #   :url_substring string que puede estar contenido en la url
      #   :timeout timeout a esperar la url
      # @return
      #   True si la URL abierta es la esperada, Raise si la URL no es la correcta
      def assert_current_url(url_substring, timeout=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          current_url = execute_script(js_get_current_url)
          if current_url.to_s.include? "#{url_substring}"
            result = true
            break
          end
        end

        puts "current url => #{current_url}"
        unless result
          raise("URL INCORRECTA. URL ACTUAL: #{current_url}. URL ESPERADA #{url_substring}")
        end
        return result
      end


      ##################################################################################
      ### ASSERTS - FIND UIELEMENT
      ##################################################################################

      #Metodo para validar si un elemento existe en la page (fue encontrado)
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # @return
      #   Boolean
      def uielement_exists(selector, locator)
        result = false
        begin
          element = find(selector, locator)
          result = element
        rescue Capybara::ElementNotFound => e
          puts err_msj_uielement_notfound(selector, locator, e.message)
          result = false
        rescue Exception => ex
          puts err_msj_uielement_notfound(selector, locator, e.message)
          result = false
        end

        return result
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, Raise Exception si no se encuentra el elemento
      def assert_for_uielement_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          result = uielement_exists(selector, locator)
          if result
            break
          end
        end

        unless result
          mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, (timeout.nil? ? 5 : timeout.to_s.to_i), '')
                          : mjs_error
          raise(mjs_error)
          #fail(mjs_error)
        end
        return result
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, False si no se encuentra el elemento
      def verify_for_uielement_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          result = uielement_exists(selector, locator)
          if result
            break
          end
        end

        unless result
          mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, (timeout.nil? ? 5 : timeout.to_s.to_i), '')
                          : mjs_error
          puts(mjs_error)
        end
        return result
      end


      ##################################################################################
      ### PAGE.ALL (LIST UIELEMENTS)
      ##################################################################################
      #Metodo para validar si existen varios elementos (LISTA DE ELEMENTOS)
      # @params
      # * :find_by Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # @return
      #   Boolean
      def uielements_exists(selector, locator)
        result = false
        begin
          element = page.all(selector, locator)
          result = element.empty? ? false : element
        rescue Capybara::ElementNotFound => e
          puts err_msj_uielement_notfound(selector, locator, e.message)
          result = false
        rescue Exception => ex
          puts err_msj_uielement_notfound(selector, locator, e.message)
          result = false
        end

        return result
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, Raise Exception si no se encuentra el elemento
      def assert_for_uielements_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          result = uielements_exists(selector, locator)
          if result
            break
          end
        end

        unless result
          mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, (timeout.nil? ? 5 : timeout.to_s.to_i), '')
                          : mjs_error
          raise(mjs_error)
        end
        return result
      end

      #Método que espera que exista un elemento. Si no existe el testcase es 'fail'
      # @params
      # * :selector Symbol parametro de busqueda del elemento, por :xpath/:id/:class
      # * :locator String query, expresión de busqueda del elemento
      # * :timeout Int tiempo máximo de espera
      # * :mjs_error String en caso de un error mensaje a mostrar en la consola
      # @return
      #   Capybara::UIElement si el elemento existe, False si no se encuentra el elemento
      def verify_for_uielements_exist(selector, locator, timeout=nil, mjs_error=nil)
        result = false
        #si no se manda timeout por default se envia 5
        to = timeout.nil? ? 5 : timeout.to_s.to_i
        start = Time.new

        while (start.to_i + to.to_i) > Time.new.to_i
          result = uielements_exists(selector, locator)
          if result
            break
          end
        end

        unless result
          mjs_error = mjs_error.nil? ? err_msj_timeout_uielement(selector, locator, (timeout.nil? ? 5 : timeout.to_s.to_i), '')
                          : mjs_error
          puts(mjs_error)
        end
        return result
      end


    end
  end
end
