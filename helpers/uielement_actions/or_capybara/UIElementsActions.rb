require 'fileutils'
require 'capybara/dsl'
require_relative '../ORBase'
require_relative '../ORErrorMessages'

module ORActions
  module ORCapybara
    class UIElementsActions
      include Capybara::DSL

      def initialize(or_base = ORActions::ORBase.new)
        @or_error_msj = ORActions::ORErrorMessages.new(or_base)
      end

      #**************************************************************************************
      # ACCIONES
      #**************************************************************************************

      def click_uielement(uielement)
        action = 'CLICK'
        begin
          uielement.click
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
        end
      end

      def hover_uielement(uielement)
        action = 'MOUSE OVER'
        begin
          uielement.hover
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
        end
      end

      def set_text_to_uielement(uielement, text)
        action = 'SET TEXT (PEGAR TEXTO)'
        begin
          uielement.set text
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
        end
      end

      def send_keys_to_uielement(uielement, keys)
        action = 'SEND KEYS (ESCRIBIR TEXTO)'
        begin
          uielement.send_keys keys
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
        end
      end

      def select_option_uielement(uielement, option)
        action = 'SELECT OPTION LIST (SELECCIONAR OPCION DE LISTA)'
        begin
          select(option, from: uielement, visible: false).select_option
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
        end
      end

      def mouse_over(uielement)
        action = 'MOUSE OVER'
        begin
          page.driver.browser.action.move_to(uielement.native).perform
        rescue Capybara::ElementNotFound => e
          raise @or_error_msj.ex_element_not_found
          #fail @or_error_msj.ex_element_not_found
        rescue Exception => ex
          raise @or_error_msj.ex_element_action(action, ex.message)
          #fail @or_error_msj.ex_element_action(action, ex.message)
        end
      end


    end
  end
end
