require 'fileutils'
require 'capybara/dsl'
require_relative '../../retryable'
require_relative '../ORBase'
require_relative '../ORErrorMessages'
require_relative 'UIBrowserActions'
require_relative 'UIElementsAsserts'
require_relative 'UIElementsActions'
require_relative 'UIElementsJSActions'
require_relative 'UIElementsAsserts'

module ORActions
  module ORCapybara
    class ORUIElementActions
      include Capybara::DSL
      include Retryable

      def initialize(or_base = ORActions::ORBase.new)
        @or_base = or_base
        @ui_asserts = ORActions::ORCapybara::UIElementsAsserts.new
        @ui_browser_action = ORActions::ORCapybara::UIBrowserActions.new
        @ui_elements_action = ORActions::ORCapybara::UIElementsActions.new(@or_base)
        @ui_elements_js_action = ORActions::ORCapybara::UIElementsJSActions.new(@or_base)
        @or_err_msj = ORActions::ORErrorMessages.new(@or_base)
        @string_market = "****************************************************************************"
      end

      #Metodo que evalua ASSERT/VERITY a un elemento
      # @params
      # @return
      #   - Capybara::UIElement Objeto UIElement - SI '@or_base.selector.to_s.to_sym != :get_array_uielements'
      #   - ARRAY Capybara::UIElement Ojetos UIElements - SI '@or_base.selector.to_s.to_sym = :get_array_uielements'
      def validar_uielement
        element = nil
        if @or_base.selector.to_s.to_sym == :url
          element = true
        else
          #SI 'ACCION_1' ES ':get_array_uielements', entonces se recuperar todos los elementos con page.all
          if @or_base.action_1.to_s.to_sym == :get_array_uielements
            element = assert_verify_array_uielement
          else
            element = assert_verify_uielement
          end
        end
        return element
      end

      def assert_verify_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielement_exist(@or_base.selector.to_s.to_sym,
                                                           @or_base.locator_new.to_s.strip,
                                                           @or_base.time_out.to_s.to_i,
                                                           @or_err_msj.ex_element_not_found
          )
        end
        return element
      end

      def assert_verify_array_uielement
        element = nil
        if @or_base.assert_verify_action.to_s.to_i == 1
          element = @ui_asserts.assert_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        else
          element = @ui_asserts.verify_for_uielements_exist(@or_base.selector.to_s,
                                                            @or_base.locator_new.to_s.strip,
                                                            @or_base.time_out.to_s.to_i,
                                                            @or_err_msj.ex_element_not_found
          )
        end
        return element
      end


      #Metodo que ejecuta acciones a un elemento, este metodo se debe implementar usando OR DATA POOL
      #   :action Accion a realizar
      def or_uielement_action(action)
        #OBJECT => find(CAPYBARA=>ELEMENT)
        uielement = @or_base.uielement
        #valor a ingresar en el uielement
        send_text = @or_base.send_text
        action = action.nil? ? :na : action.to_s.to_sym
        action = action.to_s.empty? ? :na : action.to_s.to_sym

        #REINTENTAR HACER LA ACCIÓN EN CASO DE ERROR
        retryable(:tries => 1, :on => Exception, :log =>:error) do
          #posibiona la pantalla en el elemento con js
          @ui_elements_js_action.js_scroll_to_uielement(uielement)

          case action.to_s.to_sym
          when :na
            puts "NA ACTION"
          when :get_array_uielements
            puts "NA ACTION - SE RECUEPARAN TODOS LOS UILEMENTS - PAGE.ALL"

          ##################################################################################
          ### CAPYBARA
          ##################################################################################
          when :click
            @ui_elements_action.click_uielement(uielement)

          when :hover
            @ui_elements_action.hover_uielement(uielement)

          when :set
            @ui_elements_action.set_text_to_uielement(uielement, send_text.to_s.strip)

          when :send_keys
            @ui_elements_action.send_keys_to_uielement(uielement, send_text)

          when :select_option
            @ui_elements_action.select_option_uielement(uielement, send_text.to_s.strip)

          when :visit
            @ui_browser_action.visit_url(@or_base.locator_new.to_s.strip)

          when :mouse_over_uielement
            @ui_elements_action.mouse_over(uielement)

          ##################################################################################
          ### JS
          ##################################################################################
          when :js_reload_page
            @ui_elements_js_action.js_reload_page

          when :js_back_page
            @ui_elements_js_action.js_back_browser_page

          when :js_get_current_url
            @ui_elements_js_action.js_get_current_url_page

          when :js_click
            @ui_elements_js_action.js_click_uielement(uielement)

          when :js_remove_attribute
            @ui_elements_js_action.js_remove_attribute_uielement(uielement)

          when :js_focus
            @ui_elements_js_action.js_focus_to_uielement(uielement)

          when :js_scroll_into_view
            @ui_elements_js_action.js_scroll_to_uielement(uielement)

          when :js_mouse_over_uielement
            @ui_elements_js_action.js_mouse_over_uielement(uielement)

          else
            raise "ERROR => or_uielement_action => ACCION NO VALIDA. => #{action}"
          end
        end
      end


    end
  end
end
