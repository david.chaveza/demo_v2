# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  OR TEST -- PRUEBAS DE CLASES DEL OR ACTIONS
#####################################################
require_relative 'ORBase'
require_relative 'ORTest'

def test
  oraction = ORActions::ORBase.new
  puts "OR_DEFAULT_OPTIONS"
  puts "#{ORActions::ORBase.OR_DEFAULT_OPTIONS}"
  dt = {:automation_driver=>"capybara", :assert_verify_action=>"0", :element_name=>"Text Email", :or_element_name=>"txt_email", :selector=>"id", :locator=>"email", :array_uielements=>"0", :locator_override=>nil, :action_1=>"click", :action_2=>"send_keys", :actions_extra=>nil, :sleep_between_actions=>nil, :time_out=>"1", :element_description=>"campo para capturar email"}
  oraction.set_or_csv(dt)
  puts oraction.to_s
  oraction.selector = 'VALOR SELECTOR'
  puts oraction.selector

  ortest = ORActions::ORTest.new(oraction)
  ortest.getselector

  oraction.selector = 'MOD SELECTOR'
  ortest.getselector
end

test

