# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper para datos de un elemento desde 1 OR
#####################################################

require 'fileutils'
require_relative '../data_driven/csv_data'

# Método para obtener datos de un elemento desde OR
# @params
#   :or_element_name String nombre del elemento a obtener, debe hacer match con la columna "or_element_name" del archivo "OR"
#   :or_data_file_name String nombre del archivo csv que contiene el OR
#   :path_or_data_file_name String directorio donde se encuentra el archivo 'or_data_file_name'
#   :driver Object Automation Driver. Ej. Selenium, Webdriver, Appium, Winium, Etc
# @return
#   HASH registro del OR CSV con datos del elemento
def get_or_data(or_element_name, or_data_file_name, path_or_data_file_name = nil, driver=nil)
  #se obtiene el CSV del OR (object repository)
  or_data = get_csv_data(or_data_file_name, path_or_data_file_name)
  #hash a retornar
  get_or = Hash.new

  #si no se envia element_name se recupera todo el data pool
  if or_element_name.nil?
    raise "Error el parametro 'or_element_name' es requerido"
  else
    or_data.each do |elem|
      if elem[:or_element_name].to_s.downcase.strip == or_element_name.to_s.downcase.strip
        get_or = elem
        break
      end
    end
  end

  raise "get_or_data => Error no se encontro el or_element_name => #{or_element_name}" if get_or.empty?
  get_or[:driver] = driver
  #puts "get_or => #{get_or}"
  return get_or
end

