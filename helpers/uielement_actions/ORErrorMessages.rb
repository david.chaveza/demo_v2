# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  OR MENSAJES DE ERROR Y EXCEPTION
#####################################################
require 'fileutils'
require_relative 'ORBase'

module ORActions
  class ORErrorMessages

    def initialize(or_base = ORActions::ORBase.new)
      @or_base = or_base
      @string_market = "****************************************************************************"
    end

    def ex_msj_error_automation_driver
      msj = StringIO.new
      msj << "\n"
      msj << "#{@string_market}\n"
      msj << "ERROR. AUTOMATION DRIVER INVALIDO.\n"
      msj << "Nombre del elemento = #{@or_base.element_name}.\n"
      msj << "Automation Driver = #{@or_base.automation_driver}.\n"
      msj << "Valores permitidos = #{@or_base.OR_AUTOMATION_DRIVERS.to_s}.\n"
      msj << "#{@string_market}\n"
      return msj.string.to_s
    end

    # MENSAJE DE EXCEPTION - ELEMENTO NO ENCONTRADO
    # @params
    # * :element_name String nombre del elemento
    # * :selector String selector, tecnica de identificación: 'id,css,xpath,etc'
    # * :locator String locator
    # * :automator_driver Automator driver 'capybara,selenium,appium,etc'
    # @return
    #   String MSJ
    #def ex_element_not_found(element_name, selector, locator, automator_driver)
    def ex_element_not_found
      msj = StringIO.new
      msj << "\n"
      msj << "#{@string_market}\n"
      msj << "ERROR. ELEMENTO NO ENCONTRADO\n"
      msj << "Nombre del Elemento: '#{@or_base.element_name}'\n"
      msj << "Automation Driver: '#{@or_base.automation_driver}'\n"
      msj << "Filtros de busqueda:\n"
      msj << "\t- Selector: '#{@or_base.selector}'\n"
      msj << "\t- Locator: '#{@or_base.locator_new}'\n"
      msj << "#{@string_market}\n"
      return msj.string.to_s
    end

    # MENSAJE DE EXCEPTION - ACTION
    # @params
    # * :element_name String nombre del elemento
    # * :selector String selector, tecnica de identificación: 'id,css,xpath,etc'
    # * :locator String locator
    # * :automator_driver Automator driver 'capybara,selenium,appium,etc'
    # * :action String accion a realizar sobre el uielement
    # * :exception String mensaje de la excepcion
    # @return
    #   String MSJ
    #def ex_element_action(element_name, selector, locator, automator_driver, action, ex)
    def ex_element_action(action, exception=nil)
      msj = StringIO.new
      msj << "\n"
      msj << "#{@string_market}\n"
      msj << "ERROR AL EJECUTAR ACCION AL ELEMENTO\n"
      msj << "Acción: '#{action}'\n"
      msj << "Nombre del Elemento: '#{@or_base.element_name}'\n"
      msj << "Automator Driver: '#{@or_base.automation_driver}'\n"
      msj << "Filtros de busqueda:\n"
      msj << "\t- Selector: '#{@or_base.selector}'\n"
      msj << "\t- Locator: '#{@or_base.locator_new}'\n"
      msj << "EXCEPTION:\n"
      msj << "#{exception}\n"
      msj << "#{@string_market}\n"
      return msj.string.to_s
    end

  end
end
