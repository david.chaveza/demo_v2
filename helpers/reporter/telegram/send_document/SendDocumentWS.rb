# encoding: utf-8
require "uri"
require 'net/http'
require 'openssl'
require 'json'
require_relative '../../../data_driven/yml_data'

module Telegram
  class SendDocumentWS

    #############################################################################
    # WEBSERVICE DATA
    #############################################################################
    def initialize
      @current_path = File.expand_path(File.dirname(__FILE__))
      @config = get_yml_data('../config.yml', @current_path)
      @url_servicio = "#{@config['url_telegram']}/bot#{@config['token_bot']}/sendDocument"
      @chat_id = @config['chat_id']
      @request_ssl = true
      @request_timeout = 120
      @request_headers = [
          {:header => 'User-Agent', :valor => 'testing/webservices'}
      ]
      @response = nil
      @response_codigo_esperado = 200
    end


    #############################################################################
    # ACCIONES
    #############################################################################

    #Método que ejecuta el request
    # @params
    #   :chat_id String id del chat o grupo de telegram, usar servicio getUpdates si no se conoce cual es el chat del grupo
    #   :message String mensaje a enviar desde el bot. Ej. '#*HOLA* _DESDE POSTMAN_ [text url](http://www.example.com/)\nHOLA'
    #   :parse_mode String Modo de stilo del texto. Ver https://core.telegram.org/bots/api#formatting-options. Ej. 'Markdown'
    # @return
    #   :String texto con los query params
    def request(file, caption_file)
      #obtener query params
      params = "?chat_id=#{@chat_id}&caption=#{caption_file}"
      url_servicio = "#{@url_servicio}#{params}"
      #encode para caracteres
      url_servicio = URI.escape(url_servicio)
      #encode para salto de linea
      url_servicio = url_servicio.to_s.gsub("%5Cn", '%0A')

      # 1 - URI, SSL, TimeOut
      url = URI(url_servicio)

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = @request_ssl
      https.read_timeout = @request_timeout

      # 2 - Métodos HTTP
      request = nil
      request = Net::HTTP::Post.new(url)
      #request.basic_auth(@auth_user, @auth_password)

      # 3 - Agregar Headers
      for i in 0..(@request_headers.size - 1)
        request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
      end

      # 4 REQUEST BODY > AGREGAR MULTIPART-FORM DATA
      form_data = [['document', File.open("#{file}")]] # or File.open() in case of local file
      request.set_form form_data, 'multipart/form-data'

      # 5 - Obtener HTTP Response
      @response = https.request(request)

      return @response

    rescue Exception => e
      raise("Error al ejecutar el Request a la URL => #{url}. \nException => #{e.message}")

    end


    #############################################################################
    # ASSERTS/VERIFYS/VALIDACIONES
    #############################################################################

    def assert_response_code
      unless @response.code.to_s.to_i == @response_codigo_esperado
        raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
      end
    end



  end

end


