# encoding: utf-8
require 'jsonpath'
require_relative 'SendDocumentWS'
require_relative '../eval_data'
require_relative '../../../data_driven/yml_data'

def send_telegram_document(file, caption_file)
  current_path = File.expand_path(File.dirname(__FILE__))
  config = get_yml_data('../config.yml', current_path)

  #Si la configuracion esta habilitada (1), se ejecuta los mensajes a telegram
  if config['enabled'].to_s.strip == '1'
    ws = Telegram::SendDocumentWS.new
    json_expr = "$.ok"

    begin
      puts "Ejecutando envio de Documento a Telegram: #{file}"
      response = ws.request(file, caption_file)
      ws.assert_response_code
      result = get_json_path_value(response.read_body, json_expr)
      raise "Error. No se envio a telegram, el Documento: #{file}" unless result[0].to_s.downcase.strip == 'true'
    rescue Exception => e
      puts "Error. Al enviar Documento: #{file}. Exception => #{e.message}"
    end
  end
end
