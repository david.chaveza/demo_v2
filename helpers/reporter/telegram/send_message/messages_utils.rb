# encoding: utf-8
require 'jsonpath'
require_relative 'SendMessageWS'
require_relative '../eval_data'
require_relative 'messages'
require_relative '../../../data_driven/yml_data'

def send_telegram_message(message)
  current_path = File.expand_path(File.dirname(__FILE__))
  config = get_yml_data('../config.yml', current_path)

  #Si la configuracion esta habilitada (1), se ejecuta los mensajes a telegram
  if config['enabled'].to_s.strip == '1'
    ws = Telegram::SendMessageWS.new
    parse_mode = 'Markdown'
    json_expr = "$.ok"

    begin
      puts "Ejecutando envio de mensaje a Telegram: #{message}"
      response = ws.request(message ,parse_mode)
      ws.assert_response_code
      result = get_json_path_value(response.read_body, json_expr)
      raise "Error. No se envio a telegram, el mensaje: #{message}" unless result[0].to_s.downcase.strip == 'true'
    rescue Exception => e
      puts "Error. Al enviar mensaje: .Exception => #{e.message}"
    end
  end
end

def send_telegram_message_inicial(nombre_proyecto)
  message = msj_initial(nombre_proyecto)
  send_telegram_message(message)
end

def send_telegram_message_custom(message)
  message = msj_custom(message)
  send_telegram_message(message)
end

def send_telegram_message_test_status(test, estatus, msj_error=nil)
  message = msj_test_status(test, estatus, msj_error)
  send_telegram_message(message)
end

def send_telegram_message_final(nombre_proyecto)
  message = msj_final(nombre_proyecto)
  send_telegram_message(message)
end
