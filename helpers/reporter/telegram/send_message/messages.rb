
#Permite crear un mensaje customisado
def msj_custom(message)
  return encoding_utf8(replace_chars(message))
end

#Permite crear un mensaje inicial o bienvenida
def msj_initial(nombre_proyecto)
  msj = StringIO.new
  msj << "*---------------------------------------*\n"
  msj << "*Hola!!!.*\n"
  msj << "Se comenzo la ejecución del proyecto: *#{nombre_proyecto}*"
  msj << "\n*---------------------------------------*\n"
  return encoding_utf8(msj.string.to_s)
end

#Permite crear un mensaje para dar el detalle del estatus de un test cases automatizado
def msj_test_status(test, estatus, msj_error=nil)
  msj = StringIO.new
  msj << "*---------------------------------------*\n"
  msj << "*Test Case:* #{replace_chars(test)}."
  msj << "\n*Estatus:* #{estatus}."
  msj << "\n*Mensaje Error:*: #{replace_chars(msj_error)}" unless msj_error.nil?
  msj << "\n*---------------------------------------*\n"
  return encoding_utf8(msj.string.to_s)
end

#Permite crear un mensaje final termino de ejecución
def msj_final(nombre_proyecto)
  msj = StringIO.new
  msj << "*---------------------------------------*\n"
  msj << "Termino la ejecución del proyecto: *#{nombre_proyecto}*"
  msj << "\n*---------------------------------------*\n"
  return encoding_utf8(msj.string.to_s)
end


def encoding_utf8(texto)
  string_value = String.new(texto)
  string_value = string_value.to_s.force_encoding(Encoding::UTF_8)
  return string_value
end

def replace_chars(texto)
  texto = texto.to_s.tr("_", " ")
  return texto
end