# Helper para conexión con: **"TELGRAM BOT-API"**

1- Crear un bot

2- Del bot guardar los datos:

`Token Bot. Ejemplo: 5086681050:AAFqssWuSb64-_ATgdTG5GDkCRQqY7Sgevs`

`URL Bot. Ejemplo: http://t.me/taftbot`

3- Agregar el Bot al chat grupal al cual se enviaran los mensajes

4- Obtener el ID CHAT del grupo. Usar API - https://api.telegram.org/bot{{TOKEN}}/getUpdates para encontrar el ID CHAT

`Consultar API https://core.telegram.org/bots/api`.

5- Configurar los datos en el archivo $PROYECTO/helpers/reporter/telegram/config.yml

`enabled: '1' #  1 => prendido, 0 => apagado`

`url_telegram: 'https://api.telegram.org'`

`token_bot: '5086681050:AAFqssWuSb64-_ATgdTG5GDkCRQqY7Sgevs'`

`chat_id: '-1001650818114'`

`bot_link: 'http://t.me/taftbot`

6- Agregar en el HOOKS `AfterConfiguration` (Antes de ejecutar la suite features) el codigo:

`send_telegram_message_inicial('NOMBRE DEL PROYECTO')`

7- Agregar en el HOOKS `After do |scenario|` el codigo:

`send_telegram_message_test_status(scenario.name,
scenario.failed? ? 'FAILED' : 'PASSED',
scenario.failed? ? scenario.exception.message : nil
)`

8- Agregar en el HOOKS `at_exit` (Despues de ejecutar la suite features) el codigo:

`send_telegram_message_final('NOMBRE DEL PROYECTO')`

