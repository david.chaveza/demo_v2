# encoding: utf-8
require 'jsonpath'


# Valida que el response body contenga un json path
# @return String valor de la expresion jsonpath
def get_json_path_value(response_body_current, json_path_expression)
  #CREAR JSON PATH
  json_path = JsonPath.new(json_path_expression)
  #OBTENER RESULTADOS DE EVALUAR JSON PATH
  results = json_path.on(response_body_current)
  if results.size == 0
    raise "Error. No se obtuvieron resultados al evaluar el JSON PATH: '#{json_path_expression}\n'
           Sobre el Response Body: \n#{response_body_current.to_s[0..200]} .............."
  end

  return results
end
