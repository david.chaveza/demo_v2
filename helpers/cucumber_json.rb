# encoding: utf-8
#####################################################
#Organización: TESTER ACADEMY
#Autor: David Chavez Avila
#Descripcion:  Helper hacer merge entre JSON resultados VS JSON rerun
#####################################################

require 'fileutils'
require 'json'

#arhcivo csv data pool para order_books
#$file_json_single = File.join(File.dirname(__FILE__), '../venture/live_desktop_chrome_.json')
#$file_json_rerun = File.join(File.dirname(__FILE__), '../venture/live_desktop_chrome__rerun.json')

#Path relativo y nombre del archivo de ejecucion single | ARGV[0] => nombre archivo json 'ejecucion single'
$file_json_single = File.join(File.dirname(__FILE__), "../venture/#{ARGV[0]}")
#Path relativo y nombre del archivo de ejecucion single | ARGV[1] => nombre archivo json 'ejecucion rerun'
$file_json_rerun = File.join(File.dirname(__FILE__), "../venture/#{ARGV[1]}")


#Mètodo hacer merge entre los archivos de ejecucion single y rerun
def merge_json_single_vs_rerun
  puts "file json ejecucion single => #{$file_json_single}"
  puts "file json ejecucion rerun  => #{$file_json_rerun}"

  json_single = File.read($file_json_single.to_s)
  json_rerun = File.read($file_json_rerun.to_s)

  data_hash_single = JSON.parse(json_single)
  data_hash_rerun = JSON.parse(json_rerun)

  #contar features
  #data_hash_rerun[0].size

  #contar scenarios x feature
  # data_hash_rerun[0]['elements'].size

  #contar steps x scenario
  # data_hash_rerun[0]['elements'][0]['steps'].size

  #obtener el estatus de un step
  #data_hash_rerun[0]['elements'][0]['steps'][0]['result']['status']

  #Leer 'Feature' de ejecucion inicial
  for i in 0..(data_hash_single.size - 1)
    #Leer 'Scenarios' de ejecucion inicial
    for j in 0..(data_hash_single[i]['elements'].size - 1)
      #Leer 'Steps' de ejecucion inicial
      for k in 0..(data_hash_single[i]['elements'][j]['steps'].size - 1)
        #Se evalua los steps donde hubo 'status = failed', si es asi entonces el scenario debio ser fail y ejecutarse en el rerun
        if data_hash_single[i]['elements'][j]['steps'][k]['result']['status'].to_s == 'failed'
          puts "ID EJECUCION SINGLE => #{data_hash_single[i]['elements'][j]['id']}"
          puts "STATUS EJECUCION SINGLE => #{data_hash_single[i]['elements'][j]['steps'][k]['result']['status']}"
          #Obtener data hash del scenario rerun, donde sea igual al scenario single
          scenario_rerun = get_data_scenario_rerun(data_hash_rerun, data_hash_single[i]['elements'][j]['id'].to_s)
          #Si la data 'scenario_rerun' es el mismo scenario single ejecutado, se reemplaza.
          #   esto debido a que si el archivo rerun esta vacio, aun asi se reemplazaria como vacio el scenario (sin datos).
          if scenario_rerun['id'].to_s == data_hash_single[i]['elements'][j]['id'].to_s
            #Remplaza el 'Scenario ejecucion single' => 'Scenario ejecucion rerun'
            data_hash_single[i]['elements'][j] = scenario_rerun
            puts "scenario_rerun => #{scenario_rerun['id']}"
          end

          #puts "NEW ID => #{data_hash_single[i]['elements'][j]['id']}"
          #puts "NEW STATUS => #{data_hash_single[i]['elements'][j]['steps'][k]['result']['status']}"
        end
      end
    end
  end

  #Parse Hash => Json
  #data_json = JSON.generate(data_hash_rerun)
  data_json = JSON.pretty_generate(data_hash_single)
  # fJson = File.open($file_json_single.to_s,'w')
  # fJson.write(data_json)
  # fJson.close

  #Escribir el json en el archivo 'ejecucion single *.json'
  File.open("#{$file_json_single.to_s}", 'w') do |f|
    f.write(data_json)
    f.close
  end

end

#Mètodo para obtener la data del scenario rerun cuando este sea igual a 1 scenario fail de ejecucion single
def get_data_scenario_rerun(data_hash_rerun, scenario_single)

  scenario_rerun = Hash.new

  #Leer 'Feature' de ejecucion rerun
  for x in 0..(data_hash_rerun.size - 1)
    #Leer 'Scenarios' de ejecucion rerun
    for z in 0..(data_hash_rerun[x]['elements'].size - 1)
      #Si el 'Scenario ejecucion single' = 'Scenario ejecucion rereun'
      if scenario_single.to_s == data_hash_rerun[x]['elements'][z]['id'].to_s
        puts "ID EJECUCION RERUN => #{data_hash_rerun[x]['elements'][z]['id']}"
        scenario_rerun = data_hash_rerun[x]['elements'][z]
        break
      end
    end
  end

  return scenario_rerun
end



#EJECUTAR EL MERGE DE ARCHIVOS JSON
merge_json_single_vs_rerun

