#####################################################
#Autor: TESTER ACADEMY                                   #
#Descripcion:  Helper para obtener los datos del csv#
#####################################################

#require 'unirest'
require_relative 'config'
require_relative 'generic'
require_relative 'data_driven/csv_data'
require 'jsonpath'


#Metodo que envia el request y obtiene el response
# @params
#   :ws_name nombre del ws a probar, el valor puede ser null
#   :params_override se pueden enviar los parametros. si se envian se sobreescribe el valor obtenido de los data pool.
#   :data_ws_filter filtros que se pueden aplicar a la data del webservices. 'csv_data.rb => get_data_by_filters(filters, csv)'
def get_reponse(ws_name = nil, params_override = nil, data_ws_filter = nil)

  ws = get_ws(ws_name)

  response = nil
  url = get_ws_url(ws)
  driver = ws[:driver]
  headers = get_ws_headers(ws[:headers])
  auth = get_ws_auth(ws)
  params = get_ws_parameters(ws, data_ws_filter) unless ws[:parameter_type].nil?
  param_type = ws[:parameter_type].to_s.downcase.strip
  method = ws[:method].to_s.downcase.strip


  #si params_override tiene algun valor, entonces se sobreescribe 'params',
  #   ignorando los valores obtneidos desde data pool
  if !params_override.nil? or !params_override.to_s.empty?
    params = params_override
  end

  #si "param_type = 'url'" entonces se sobreescribe la url
  #   ya que los parametros se envian desde la url
  if param_type == 'url'
    #url = "#{url}/#{params}"
    url = params.to_s.empty? ? url : "#{url}/#{params}"
    params = nil
  end

  #se crea el 'request' segun el tipo de
  #   parametro de la petion: put, get, post, etc
  response = request_net_http(url, method, ws[:headers], params)

  return response

end

def request_net_http(url_servicio, method, headers_string, params)
  require "uri"
  require "net/http"

  response = nil
  url = URI(url_servicio)
  https = Net::HTTP.new(url.host, url.port);
  https.use_ssl = true
  https.read_timeout = 180

  #se crea el tipo de request segun el metodo http
  request = get_method_request_net_http(url, method)
  #envian los headers
  result = headers_string.split("|")
  result = result.map{|x|
    x = x.split(": ");
    h_tmp = String.new
    h_tmp = get_vars_out(x.last.to_s)
    request[x.first.to_s] = h_tmp
  }
  #se envia el cuerpo de la respuesta
  request.body = params unless params.nil?

  #se envia el request
  response = https.request(request)

  return response
end

def get_method_request_net_http(url, method)
    request = nil

    #se crea el 'request' segun el tipo de
    #   parametro de la petion: put, get, post, etc
    case method.to_s.downcase.strip
      when 'get' then
        request = Net::HTTP::Get.new(url)

      when 'post' then
        request = Net::HTTP::Post.new(url)

      when 'put' then
        request = Net::HTTP::Put.new(url)

      when 'delete' then
        request = Net::HTTP::Delete.new(url)

      when 'patch' then
        request = Net::HTTP::Patch.new(url)

      else
        raise("method request => opción method invalida => #{method}")
    end

    return request
end

#Metodo que permite obtener los datos del webservice a testear
# @params
#   :ws_name nombre del ws a probar, el valor puede ser null
def get_ws(ws_name = nil)

  #LEER EL CSV CON LOS DATOS DE LOS WS
  dt = get_csv_data('ws')

  #hash a retornar
  ws = Hash.new

  #se evalua si se envio el nombre del ws a testear,
  #   si 'ws_name = nil', entonces se retorna la info de todos los ws

  if ws.nil? or ws.to_s.empty?
    ws = dt

  else
    #si 'ws_name != nil', entonces se retorna la info del registro correspondiente al ws
    dt.each_with_index do |record, index|
      #print "key: #{record}, index: #{index}\n"
      #se valida solo se recupera el primer registro! cuando "status_record <> 0"
      #		y el index mayor a '0' ya que este es las columnas
      if record[:ws_name].to_s.downcase.strip == ws_name.to_s.downcase.strip
        ws = record
        break
      end
    end

  end

  return ws

end



##########################################################################################################
##########################################################################################################
##########################################################################################################
# METODOS PARA OBTENER LOS DATOS DEL REQUEST
#   - URL
#   - HEADERS
#   - METHOD
#   - PARAMETERS
#   - AUTHENTICATION
##########################################################################################################
##########################################################################################################
##########################################################################################################

#Metodo recupera la url del ws a probar, el HOST se toma de '/csv_data/environment.csv',
#     si en el data pool '/csv_data/ws.csv' tiene algun valor en 'host_override' se sobrescribe el host del web service
# @params
#   :ws_data hash con los datos del ws a testear
def get_ws_url(ws_data)

  host_override = ws_data[:host_override].to_s.strip
  url = ws_data[:url].to_s.strip

  if host_override.nil? or host_override.to_s.empty?
    return "#{Config.url_visit}/#{url}"
  else
    return "#{host_override}/#{url}"
  end

end

#Metodo recupera la autenticacion del servidor
# @params
#   :ws_data hash con los datos del ws a testear
def get_ws_auth(ws_data)
  user = ws_data
  if ws_data[:auth_user].nil? or ws_data[:auth_user].to_s.empty?
    return nil
  else
    return  {:user=>"#{ws_data[:auth_user].to_s.strip}", :password=>"#{ws_data[:auth_password].to_s.strip}"}
  end
end


#Metodo recupera los headers para enviar en el request
# @params
#   :headers_string string que contiene los datos de los headres, separados por ';' cada header
def get_ws_headers(headers_string)
  #result = headers_string.split(";")
  result = headers_string.split("|")
  headers = Hash.new
  result = result.each { |header|
    h = header.split(":");
    h_tmp = String.new
    h_tmp = get_vars_out(h.last)
    headers[h.first.to_sym] = h_tmp
  }
  #result = result.reduce(:merge)
  return headers
end

#Metodo que recupera los parametros a ingresar, esto mediante el remplazo
#     de exprecion regular en el data pool 'ws.csv' y los datos del archivo data pool relacionado  '/csv_data/dt_ws_[nombre-archivo].csv'
# @params
#   :ws_data hash con los datos del ws a testear
#   :data_ws_filter filtros que se pueden aplicar a la data del webservices. 'csv_data.rb => get_data_by_filters(filters, csv)'
def get_ws_parameters(ws_data, data_ws_filter = nil)

  param_type = ws_data[:parameter_type].to_s.downcase.strip
  params = ws_data[:parameters].to_s.strip
  dt_file = ws_data[:data_file].to_s.downcase.strip
  parameters = nil
  #hash a retornar
  dt_ws = Hash.new

  #se obtiene el request del archivo csv
  if param_type.to_s.downcase.strip == 'body_file'
    file =  File.join(File.dirname(__FILE__), "../venture/config/csv_data/#{params.to_s.strip}")
    file_data = File.read(file.to_s)
    params = file_data
  end


  #obtiene el data pool con los datos a insertar en el web service.
  #   se reutiliza el metodo para obtener data de O.R.
  #dt_ws = get_dt_for_or_elements(dt_file)
  if dt_file.nil? == false && dt_file.to_s.empty? == false
    #OBTIENE EL CSV del datapool (completo)
    dt_ws = get_csv_data(dt_file)
    dt_ws = get_data_by_filters(data_ws_filter, dt_ws)
    dt_ws = dt_ws[1]
  end

  #obtiene las keys (nombres de columnas), del registro a usar del data pool
  keys = dt_ws.keys

  #SE CREA PARAMS TEMPORAL, PARA NO SOBREESCRIBIR EL DATA POOL DEL WEB SERVICE
  params_tmp = nil
  params_tmp = params

  #SE ITERA LAS KEYS, PARA EVALUAR CADA COLUMNA Y REMPLAZAR LA EXPRECION REGULAR
  #   POR EL VALOR DEL DATA POOL RELACIONADO AL WEB SERVICE
  keys.each_with_index do |key, index|

    if params.to_s.downcase.strip.include? "${dt_#{key}".to_s.downcase.strip
      #puts "key = #{key} value = #{dt_ws[key.to_s.to_sym]}"
      key_tmp = "${dt_#{key}}".to_s.upcase.strip
      #puts key_tmp
      params_tmp[key_tmp] = "#{dt_ws[key.to_s.to_sym]}"
    end

  end

  #OBTENER VARIABLES DE SALIDA Y REMPLAZAR LA EXPRESION REGULAR POR EL VALOR DE LA VARIABLE
  params_tmp = get_vars_out(params_tmp)

  case param_type.to_s.downcase.strip
    when 'na' then
      parameters = nil

    when 'url' then
      parameters = params_tmp

    when 'body' then
      parameters = params_tmp

    when 'body_file' then
      parameters = params_tmp

    when 'hash_params' then
      #PARSEA EL RESULTADO DE JSON.TO_S => HASH
      parameters = JSON.parse(params_tmp)

    else
      raise("get_ws_parameter => opción param_type invalida => #{param_type}")
  end

  #puts parameters
  return parameters

end


#Metodo recupera los headers para enviar en el request
# @params
#   :headers_string string que contiene los datos de los headres, separados por ';' cada header
def get_response_headers(ws_name, response)
  ws = get_ws(ws_name)
  driver_http = ws[:driver].to_s.downcase.strip
  headers = nil

  #obtener response body, dependiendo el driver http
  headers = response.read_body

  return headers
end


##########################################################################################################
##########################################################################################################
##########################################################################################################
# METODOS DE ASSERTS GENERICOS
##########################################################################################################
##########################################################################################################
##########################################################################################################

#Metodo que valida el response code de un WS
#     data pool 'ws.csv' y los datos del archivo data pool relacionado  '/csv_data/dt_ws_[nombre-archivo].csv'
# @params
#   :ws_name nombre del webservice a testear, este debe existir en el data pool 'ws.csv'
#   :response response del ws
def assert_ws_response_code(ws_name = nil, response)

  ws = get_ws(ws_name)

  expected_response_code = ws[:expected_response_code].to_s.strip
  response_code = response.code
  result = false

  if response_code.to_s == expected_response_code
    result = true
    puts "Codigo de respuesta => #{response_code}.
          \nCodigo de respuesta esperado => #{expected_response_code}.
          \n"
  else
    fail("assert_ws_response_success => Response code no esperado.
          \nCodigo de respuesta => #{response_code}.
          \nCodigo de respuesta esperado => #{expected_response_code}.
          \n"
    )
  end

  return result

end


#Metodo que valida el response body de un WS
#     data pool 'ws.csv' y los datos del archivo data pool relacionado  '/csv_data/dt_ws_[nombre-archivo].csv'
# @params
#   :ws_name nombre del webservice a testear, este debe existir en el data pool 'ws.csv'
#   :response response del ws
def assert_ws_response_body(ws_name = nil, response)

  ws = get_ws(ws_name)
  response_body_is_file = ws[:expected_response_body_is_file]
  driver_http = ws[:driver].to_s.downcase.strip
  resp_body = nil
  dt_ws_expected_response_body = nil

  #obtener response body, dependiendo el driver http
  resp_body = response.read_body

  #se obtiene el response del archivo csv
  if response_body_is_file.to_s.downcase.strip == '1'
    file =  File.join(File.dirname(__FILE__), "../venture/config/csv_data/#{ws[:expected_response_body].to_s.strip}")
    file_data = File.read(file)
    dt_ws_expected_response_body = file_data.to_s.strip
  else
    dt_ws_expected_response_body = ws[:expected_response_body].to_s.strip
  end


  if ws[:ws_type].to_s.include? 'soap'
    require 'json'
    require 'active_support/core_ext/hash'
    expected_response_body = Hash.from_xml(dt_ws_expected_response_body.to_s.strip).to_json
    expected_response_body = JSON.parse(expected_response_body)
    response_body = Hash.from_xml(resp_body).to_json
    response_body = JSON.parse(response_body)
  else
    expected_response_body = JSON.parse( dt_ws_expected_response_body.to_s.strip )
    response_body = JSON.parse( resp_body )
  end

  val_response(expected_response_body, response_body)

end




#Metodo que valida la estructura del response vs response_esperado
#   si los datos vienen en null, se envia log indicando que no se coincide
# @params
#   :expected_response_body response esperado
#   :response_body response obtenido
# @return Array con las incidencias.
#       array = [{ :nodo_padre => @el_padre, :nodo_hijo => el_hijo,
#                 :res_experado => values[0], :res_obtenido => values[1],
#                 :mensaje => 'Valores no hacen match. Valor NULL detectado', :result => false }]
def val_response(expected_response_body, response_body)
  @results = Array.new
  @hs = Array.new
  compare(expected_response_body, response_body)
  #puts @results
  @results
end


#Metodo que compara la estructura del response vs response_esperado
#   si los datos vienen en null, se envia log indicando que no se coincide
# @params
#   :hash1 hash de datos. Ej response esperado
#   :hash2 hash de datos. Ej response obtenido
# @return Boolean
def compare(hash1, hash2)

  args = [hash1, hash2]
  #p "args => #{args}"

  return true if args.all? {|h| h.nil?}
  return false if args.one? {|h| h.nil?}
  
  if hash1.is_a?(Array)
   return compare(hash1[0], hash2[0])
  end

  hash1.each_key do |k|
    values = [hash1[k], hash2[k]]
    #p "\nkey => #{k}"
    #p "values => #{values}"
    args[0].delete k
    args[1].delete k

    if values.all? {|h| h.is_a?(Hash)}
      @el_padre = nil
      @el_padre = k.to_s
      #p "ELEMENTO PADRE"
      @hs = args
      return compare(*values)
    else
      #SETEAR VARIABLES DE SALIDA DEL RESPONSE A VARS_OUT
      set_var_out_from_response(values[0], values[1])
      #p "values => #{values[0]}, #{values[1]}"
      #VALIDAR LOS VALUES QUE SON NULL, PUDIERA EXISTIR UNA DIFERENCIA
      #return false if values.one? {|value| value.nil? }
      if values.one? {|value| value.nil? }
        el_hijo = nil
        el_hijo = k.to_s
        result = Hash.new
        #p "ELEMENTO HIJOS"
        result = { :nodo_padre => @el_padre, :nodo_hijo => el_hijo,
                   :res_experado => values[0], :res_obtenido => values[1],
                    :mensaje => 'Valores no hacen match. Valor NULL detectado', :result => false }
        @results.push(result)
      end
    end
  end

  #Los Hash previos como los padres son evaluados,
  #   para evitar que se termine el ciclo en un elemento con los nodos hijos
  #   y no continue con los posibles hermanos previos que tuviera el padre
  unless @hs.empty?
    if @hs[0].size > 0
      #p "ARGS FINALES #{@hs}"
      compare(@hs[0], @hs[1])
    end
  end

  true
end

# Assertion, valida que el response body contenga un json path
# @return String valor de la expresion jsonpath
def assert_json_path(response_body_current, json_path_expression)
  #CREAR JSON PATH
  json_path = JsonPath.new(json_path_expression)
  #OBTENER RESULTADOS DE EVALUAR JSON PATH
  results = json_path.on(response_body_current)
  if results.size == 0
    raise "Error. No se obtuvieron resultados al evaluar el JSON PATH: '#{json_path_expression}\n'
           Sobre el Response Body: \n#{response_body_current}"
  end

  return results
end

# Valida que el response body contenga un json path
# @return String valor de la expresion jsonpath
def assert_json_path_value(response_body_current, json_path_expression)
  #CREAR JSON PATH
  json_path = JsonPath.new(json_path_expression)
  #OBTENER RESULTADOS DE EVALUAR JSON PATH
  results = json_path.on(response_body_current)
  if results.size == 0
    raise "Error. No se obtuvieron resultados al evaluar el JSON PATH: '#{json_path_expression}\n'
           Sobre el Response Body: \n#{response_body_current.to_s[0..200]} .............."
  end

  return results
end


##########################################################################################################
##########################################################################################################
##########################################################################################################
# GET & SET VARS OUT
#     se obtienen y crear las variables de salida usadas en los template de request y response
##########################################################################################################
##########################################################################################################
##########################################################################################################


#Metodo que permite crear variable de salida (paso) desde el response, usando expresión regular
# @params
#   :response_value_expected hash de datos. Ej response esperado
#   :response_value_current hash de datos. Ej response obtenido
def set_var_out_from_response(response_value_expected, response_value_current)
  vout_name = String.new
  vout_name = response_value_expected.to_s.strip.scan(/\${SET_VOUT_(.*)}/)
  if vout_name.size > 0
    #p "set_var_out_from_response => #{vout_name[0][0].to_s.strip}, #{response_value_current.to_s.strip}\n"
    Config.set_var_out(vout_name[0][0].to_s.strip, response_value_current.to_s.strip)
  end
end


#Metodo que permite obtener variables de salida (paso)
#   para sustituir la expresion regular de un template (string) con el valor de la variable
# @params
#   :data_string datos que contienen la exprecion regular para sustituirla por el valor de una variable out
def get_vars_out(data_string)
  vars = Array.new
  vars = data_string.to_s.strip.scan(/\${GET_VOUT_(.*)}/)

  for i in 0..(vars.size - 1)
    var = String.new
    var = Config.get_var_out( vars[i][0].to_s.strip )
    data_string["${GET_VOUT_#{vars[i][0].to_s.strip}}"] = var
  end
  return data_string
end
