@ECHO OFF

REM --------------------------------------------------
REM SET VARS PATH PROYECT
REM --------------------------------------------------
set unidad_disco=C:
set selenium_drivers="C:/ambiente/selenium_drivers"
set cucumber_proyect="C:/ambiente/automation/demo_v2"
set jenkins_home="C:/ambiente/Jenkins"
set jenkins_workspace="/workspace/demo_web_capybara"
set environment="stage"
set driver=chrome_capybara
REM set driver=firefox_capybara
set device_name="web_desktop"
set cucumber_profile="web_desktop"
set feature="07_dummy_features/07_qa_touch_dummy.feature"
set cucumber_tags="--tags @regresion"

REM PATH EXTERNAL DATA, PARA TOMER LOS DATA POOL CSV DESDE UN DIRECTORIO EXTERNO AL PROYECTO
set path_external_data="C:/ambiente/automation_external_data/demo_v2"

REM --------------------------------------------------
REM SET VARS QA-TOUCH
REM --------------------------------------------------
REM 1- HABILITAR CONEXION  2- DESHABILITAR CONEXION
SET TAF_QATOCUH_ENABLED=1
SET TAF_QATOCUH_URL_ADD_RESULTS=https://api.qatouch.com/api/v1/testRunResults/add/results
SET TAF_QATOCUH_API_TOKEN=855f5c86c185551487ef2e6b768e8465c77d9a813082e083ac614fdc5528cf4b
SET TAF_QATOCUH_DOMAIN=TESTER ACADEMY
SET TAF_QATOCUH_PROJECT_KEY=09wl
SET TAF_QATOCUH_TEST_RUN_KEY=lb9r

REM --------------------------------------------------
REM IR AL PROYECTO
REM --------------------------------------------------
%unidad_disco%
cd %cucumber_proyect%/bin

REM --------------------------------------------------
REM BAJAR CAMBIOS GIT
REM --------------------------------------------------
REM git pull https://git.client.user:Test1234@gitlab.com/david.chaveza/demo_v2.git

sh run_tests.sh --cucumber_proyect=%cucumber_proyect% --jenkins_home=%jenkins_home% --jenkins_workspace=%jenkins_workspace% --environment=%environment% --driver=%driver% --device_name=%device_name% --app=%app% --device_id_calabash=%device_id_calabash% --cucumber_profile=%cucumber_profile% --feature=%feature% --cucumber_tags=%cucumber_tags% --selenium_drivers=%selenium_drivers% --path_external_data=%path_external_data%

pause

exit