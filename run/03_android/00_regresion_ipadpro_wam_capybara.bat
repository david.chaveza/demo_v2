@ECHO OFF

REM --------------------------------------------------
REM SET VARS PATH PROYECT
REM --------------------------------------------------
set unidad_disco=D:
set cucumber_proyect="D:/ambiente/automation/demo_v2"
set jenkins_home="D:/ambiente/Jenkins"
set jenkins_workspace="/workspace/demo_wam_capybara"
set environment="stage"
set driver=wam_capybara
set device_name="ipad_pro"
set app=
set device_id_calabash=
set cucumber_profile="web_desktop"
set feature="01_web/"
set cucumber_tags="--tags @regresion"

REM --------------------------------------------------
REM IR AL PROYECTO
REM --------------------------------------------------
%unidad_disco%
cd %cucumber_proyect%/bin

REM --------------------------------------------------
REM BAJAR CAMBIOS GIT
REM --------------------------------------------------
REM git pull https://git.client.user:Test1234@gitlab.com/david.chaveza/demo_v2.git

sh run_tests.sh --cucumber_proyect=%cucumber_proyect% --jenkins_home=%jenkins_home% --jenkins_workspace=%jenkins_workspace% --environment=%environment% --driver=%driver% --device_name=%device_name% --app=%app% --device_id_calabash=%device_id_calabash% --cucumber_profile=%cucumber_profile% --feature=%feature% --cucumber_tags=%cucumber_tags%

pause

exit