#!/bin/bash

#******************************************************************************
# SH PARAMETROS
#******************************************************************************
# - cucumber_proyect   =>   Directorio donde se encuentra el proyecto
# - jenkins_home   =>   Directorio donde de Jenkins:
#		- Si es la PC master: directorio donde esta instalado
# 		- Si es un Nodo Jenkins: directorio remoto que se configura en el nodo de Jenkins"
# - jenkins_workspace   =>   Directorio del workspace del proyecto de Jenkins:
# 		- Si es la PC master: directorio donde esta instalado + el workspace del proyecto
# 		- Si es un Nodo Jenkins: directorio remoto que se configura en el nodo de Jenkins + el workspace del proyecto"
# - environment   =>   Ambiente de pruebas, puede tomar diversos valores, tantos como ambientes de pruebas existan y estos debenran hacer
#		match con el archivo: "$cucumber_proyect/venture/config/csv_data/00_config/dt_environmet.csv"
# - driver   =>   Driver de automatización a usar.
#		Valores premitidos: "firefox_capybara", "chrome_capybara", "chrome_headless_capybara", "ie_capybara", "winium", "appium", "appium_wam", "appium_desktop", "winappdriver", "ws (webservices)", "calabash"
# - device_name   =>   Dispositivo de pruebas a utilizar, puede tomar diversos valores, tantos como dispositivos de pruebas se requieran y estos debenran hacer match con el archivo: "$cucumber_proyect/venture/config/csv_data/00_config/dt_devices.csv"
# - app   =>   Sirve para saber que app se tiene que firmar. SOLO CALABASH
#				La app debera estar en la ruta: "$cucumber_proyect/venture/app.apk"
# - device_id_calabash   =>   UUID del dispositivo. SOLO CALABASH
# - cucumber_profile   =>   Cucumber profile. El perfil a usar debera hacer match con el archivo: "$cucumber_proyect/venture/config/cucumber.yml"
# - feature   =>   Cucumber FEATURE. Indica el archivo .feature a ejecutar; si el valor esta vacio, se ejecutaran todos los archivos .feature existentes. EJ. "$cucumber_proyect/venture/features/**/*.feature"
# - cucumber_tags   =>   Define las tags de cucumber a ejecutar. Las tags se definen de diferente forma en WEB/APPIUM/ETC VS CALABASH"


#******************************************************************************
# OBTIENE LOS PARAMETROS SETEADOS AL SH
#******************************************************************************
echo "*************************************************************************"
echo "OBTENER PARAMETROS EN EL SH"
echo "*************************************************************************"
while [ $# -gt 0 ]; do
  case "$1" in
    --selenium_drivers=*)
      selenium_drivers="${1#*=}"
      ;;
    --cucumber_proyect=*)
      cucumber_proyect="${1#*=}"
      #cucumber_proyect="${cucumber_proyect,,}"
      ;;
    --jenkins_home=*)
      jenkins_home="${1#*=}"
      ;;
    --jenkins_workspace=*)
      jenkins_workspace="${1#*=}"
      ;;
    --environment=*)
      environment="${1#*=}"
      ;;
    --driver=*)
      driver="${1#*=}"
      ;;
    --device_name=*)
      device_name="${1#*=}"
      ;;
    --app=*)
      app="${1#*=}"
      ;;
    --device_id_calabash=*)
      device_id_calabash="${1#*=}"
      ;;
    --cucumber_profile=*)
      cucumber_profile="${1#*=}"
      ;;
    --feature=*)
      feature="${1#*=}"
      ;;
    --cucumber_tags=*)
      cucumber_tags="${1#*=}"
      ;;
    --path_external_data=*)
      path_external_data="${1#*=}"
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument.*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done
printf "Argument cucumber_proyect   =>   %s\n" "$cucumber_proyect"
printf "Argument selenium_drivers   =>   %s\n" "$selenium_drivers"
printf "Argument jenkins_home   =>   %s\n" "$jenkins_home"
printf "Argument jenkins_workspace   =>   %s\n" "$jenkins_workspace"
printf "Argument environment   =>   %s\n" "$environment"
printf "Argument driver   =>   %s\n" "$driver"
printf "Argument device_name   =>   %s\n" "$device_name"
printf "Argument app   =>   %s\n" "$app"
printf "Argument device_id_calabash   =>   %s\n" "$device_id_calabash"
printf "Argument cucumber_profile   =>   %s\n" "$cucumber_profile"
printf "Argument feature   =>   %s\n" "$feature"
printf "Argument cucumber_tags   =>   %s\n" "$cucumber_tags"
printf "Argument path_external_data   =>   %s\n" "$path_external_data"
echo "********************************************************"

#******************************************************************************
# MONTAR UNIDAD DE DISCO DONDE ESTA EL PROYECTO
# (DESHABILITADO)
#******************************************************************************
# MOONT DISK
# net use X: %JENKINS%

#******************************************************************************
# EXPORTAR VARIABLES PARA JENKINS
#******************************************************************************
export JENKINS=$jenkins_home
export JENKINS_WORKSPACE=$jenkins_workspace

#******************************************************************************
# VARIABLES
#******************************************************************************
export SELENIUM_DRIVERS=$selenium_drivers
export CUCUMBER_PROYECT=$cucumber_proyect
export ENVIRONMENT=$environment
export DRIVER=$driver
export DEVICE_NAME=$device_name
export APP=$app
# VARIABLE PARA INPUT/OUTPUTS EXTERNOS
export PATH_EXTERNAL_DATA=$path_external_data
# EXPORT VARS PARA EVICENCIA/REPORTES
export EXEC_DATE=$(date +"%y%m%d")
export EXEC_TIME=$(date +"%H%M")
export EXEC_TYPE=single
export PATH_RESULTS=evidence/"$EXEC_DATE"/"$EXEC_TIME"/"$ENVIRONMENT"_"$DEVICE_NAME"_"$DRIVER"
export PATH_RESULTS_SINGLE="$PATH_RESULTS"/single
export PATH_RESULTS_RERUN="$PATH_RESULTS"/rerun
export REPORT_NAME="results"


#******************************************************************************
# CREAR DIRECTORIOS EVIDENCIAS / REPORTES...
#******************************************************************************
echo "*************************************************************************"
echo "CREAR DIRECTORIOS EVIDENCIAS / REPORTES..."
echo "*************************************************************************"
cd ../venture
echo mkdir -p "$PATH_RESULTS"
mkdir -p "$PATH_RESULTS"
echo mkdir -p "$PATH_RESULTS_SINGLE"
mkdir -p "$PATH_RESULTS_SINGLE"
echo mkdir -p "$PATH_RESULTS_RERUN"
mkdir -p "$PATH_RESULTS_RERUN"
echo "*************************************************************************"

#******************************************************************************
# BUNDLE INSTALL...
#******************************************************************************
echo "*************************************************************************"
echo "BUNDLE INSTALL..."
echo "*************************************************************************"
echo "DRIVER => "$DRIVER""
##SE ELIMINAN CARPETAS DEL BUNDLE Y GEMFILE
cd ../
rm -r -f .bundle
echo "Se elimino "$CUCUMBER_PROYECT"/.bundle ..."
rm -r -f bundle
echo "Se elimino "$CUCUMBER_PROYECT"/bundle ..."
rm -r -f GemFile.lock
echo "Se elimino "$CUCUMBER_PROYECT"/GemFile.lock ..."
rm -r -f GemFile.lock
echo "Se elimino "$CUCUMBER_PROYECT"/GemFile.lock ..."

##SE COPIA EL TEMPLATE DE GEMFILE ADECUADO AL DRIVER
#cp -f bin/Gemfile_generic Gemfile
bundle install --path=bundle

echo "BUNDLE INSTALL SUCCESS..."
echo "*************************************************************************"

#******************************************************************************
# EJECUCION DE TEST
#******************************************************************************
echo "*************************************************************************"
echo "EJECUCION TEST SIGLE"
echo "*************************************************************************"
cd venture

echo bundle exec cucumber $cucumber_tags -p $cucumber_profile features/$feature -f pretty -f html --out "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html -f pretty -f rerun --out "$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f json -o "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json
bundle exec cucumber $cucumber_tags -p $cucumber_profile features/$feature -f pretty -f html --out "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html -f pretty -f rerun --out "$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f json -o "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json <<RUNTEST
#bundle exec parallel_cucumber --type cucumber -n 2 features/$feature -o '-f pretty -f html --out "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html' --group-by scenarios <<RUNTEST
RUNTEST

echo "FIN EJECUCION SIGLE"
echo "*************************************************************************"

echo "*************************************************************************"
echo "CUSTOM REPORT"
echo "*************************************************************************"
echo ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html"
cd ..
cd helpers
echo ruby custom_report.rb ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html"
ruby custom_report.rb ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html"
echo "*************************************************************************"

echo "*************************************************************************"
echo "EJECUCION TEST RERUN"
echo "*************************************************************************"
cd ..
cd venture

export EXEC_TYPE=rerun
echo bundle exec cucumber @"$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f html --out "$PATH_RESULTS_RERUN"/"$REPORT_NAME".html -f pretty -f json -o "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json
bundle exec cucumber @"$PATH_RESULTS_SINGLE"/rerun_"$REPORT_NAME".txt -f pretty -f html --out "$PATH_RESULTS_RERUN"/"$REPORT_NAME".html -f pretty -f json -o "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json <<RERUNTEST
RERUNTEST

echo "FIN EJECUCION RERUN"
echo "*************************************************************************"

echo "*************************************************************************"
echo "CUSTOM REPORT"
echo "*************************************************************************"
echo ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_SINGLE"/"$REPORT_NAME".html"
cd "$CUCUMBER_PROYECT"
cd helpers
echo ruby custom_report.rb ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_RERUN"/"$REPORT_NAME".html"
ruby custom_report.rb ""$CUCUMBER_PROYECT"/venture/"$PATH_RESULTS_RERUN"/"$REPORT_NAME".html"
echo "********************************************************"

echo "*************************************************************************"
echo "BUILD REPORTS"
echo "*************************************************************************"
cd "$CUCUMBER_PROYECT"
cd helpers
echo ruby cucumber_report_builder.rb "$PATH_RESULTS"
ruby cucumber_report_builder.rb "$PATH_RESULTS" <<cucumber_report_builder
cucumber_report_builder
echo "FIN BUILD REPORTS"
echo "*************************************************************************"

echo "*************************************************************************"
echo "COPIANDO CUCUMBER JSON => WORKSPACE"
echo "*************************************************************************"
cd ../venture
echo cp -f "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json \""$JENKINS"/"$JENKINS_WORKSPACE"/"$REPORT_NAME".json\"
cp -f "$PATH_RESULTS_SINGLE"/"$REPORT_NAME".json "$JENKINS"/"$JENKINS_WORKSPACE"/"$REPORT_NAME".json
echo cp -f "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json "$JENKINS"/"$JENKINS_WORKSPACE"/"$REPORT_NAME"_rerun.json
cp -f "$PATH_RESULTS_RERUN"/"$REPORT_NAME"_rerun.json "$JENKINS"/"$JENKINS_WORKSPACE"/"$REPORT_NAME"_rerun.json
echo "FIN COPIANDO CUCUMBER JSON => WORKSPACE"
echo "*************************************************************************"

echo "*************************************************************************"
echo "FIN EJECUCION..............."
echo "............................"
echo "*************************************************************************"

exit