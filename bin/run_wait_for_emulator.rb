#####################################################
#Autor: BICHO
#Descripcion: RUN PARA ESPERAR A QUE UN DISPOSITIVO EXISTA
#####################################################

require 'fileutils'
require 'csv'


#####################################################
# VARIABLES DE ENTORNO
#####################################################

@is_emulator = ENV['is_emulator'].nil? ? '' : ENV['is_emulator']
@avd_name = ENV['avd_name'].nil? ? '' : ENV['avd_name']
@port = ENV['port'].nil? ? '' : ENV['port']


#####################################################
# ESPERAR AL DISPOSITIVO
#####################################################

#METODO PRINCIPAL (EJECUTOR)
def run
	if @is_emulator == '1'
		wait_for_device
	end
end

#METODO PARA EJECUTAR CMD QUE ESPERA QUE EL BOOT DEL DISPOSITIVO INICIE
# :params
def wait_for_device

  begin

    require 'open3'
	
	result = false	

	#ESPERA 180 SEG POR EL DISPOSITIVO
	for i in 0..180
		
		sleep 1
		
		#VARIABLES PARA RECUPERAR OUT DE CONSOLA
		console_out = nil
		console_err = nil
		
		#EJECUTA CONSOLA CMD#espera por boot del dispositivo
		#	cuando inicia debe retornar:
		#		dev.bootcomplete
		#		1
		stdin, stdout, stderr = Open3.popen3("adb -s emulator-#{@port} shell getprop dev.bootcomplete")

		#RECUPERA MENSAJES SALIDA DEL OUT Y ERR PARA LA CONSOLA
		console_out = stdout.readlines
		console_err = stderr.readlines
		#puts "console_out #{console_out}"
		#puts "console_err #{console_err}"
	
		#OBTIENE EL RESULTADO DEL TEST CASE DESDE EL OUT DE CONSOLA
		for i in 0..(console_out.size - 1)
		  if console_out[i].to_s.strip.include? '1'
			puts "DEVICE STARTED..."
			result = true
			break
		  end
		end
		
		#SI YA SE INCIO EL DISPOSITIVO SE ROMPE EL CICLO DE ESPERA
		if result
			break
		end
		
	end 

	#SI NO SE INICIA DISPOSITIVO
	unless result
		#FAIL WAIT FOR DEVICE
		puts "wait_for_device => FAIL WAIT FOR DEVICE => DEVICE NOT FOUND"
		raise "wait_for_device => FAIL WAIT FOR DEVICE => DEVICE NOT FOUND"
	end	


  rescue Exception => e

    #EXPEPTION WAIT FOR DEVICE
	puts "wait_for_device => EXPEPTION WAIT FOR DEVICE => '#{e.message}'"
	raise "wait_for_device => EXPEPTION WAIT FOR DEVICE => '#{e.message}'"
    
  end

end



#####################################################
# OBTENER DATA
#####################################################

#Metodo obtener la data de un csv en hash map
# @params:
#   :csv_data nobre del archivo csv que se va cargar
def get_csv_data(csv_data)
  csv_file = nil

  #obtiene el path y nombre del archivo csv
  if csv_data.to_s.include? '.csv'
    csv_file =  File.join(File.dirname(__FILE__), "ts_data/#{csv_data}")
  elsif (
    csv_file = File.join(File.dirname(__FILE__), "ts_data/#{csv_data}.csv")
  )
  end

  #csv_arr = CSV.read( csv_file,  {:headers => true, :header_converters => :symbol, :encoding => 'ISO-8859-1'} )
  csv_arr = CSV.read( csv_file,  {:headers => true, :header_converters => :symbol, :encoding => 'UTF-8'} )
  keys = csv_arr.headers.to_a
  # read attribute example => csv[index][:column1]
  return csv_arr.to_a.map {|row| Hash[ keys.zip(row) ] }
end



#####################################################
# WRITE CSV DATA
#####################################################

def exportHashToCsv(data, path_file, file)

  csv_file = nil
  #obtiene el path y nombre del archivo csv
  if file.to_s.include? '.csv'
    csv_file = "#{path_file}/#{file}"
  elsif (
    csv_file = "#{path_file}/#{file}.csv"
  )
  end

  CSV.open(csv_file, "w", headers: data.first.keys) do |csv|
    data.each do |h|
      csv << h.values
    end
  end

end




#####################################################
# RUN
#####################################################

run
